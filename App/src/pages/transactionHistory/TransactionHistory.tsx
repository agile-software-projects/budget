/* 
 * As a user I want to be able to see the 
 * list of my transactions in a selected period of time.
 * AC
 * filter by month
 * show the description
 * show the amount 
*/

import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButtons, IonIcon, IonCard, IonCardHeader, IonItem, IonLabel, IonChip } from '@ionic/react';
import { trashOutline } from 'ionicons/icons';
import './TransactionHistory.css';

import Menu from '../menu/Menu';
import moment from 'moment'
import { Account, Transaction } from '../../store/store-types';
import { useStore } from '../../store/store-actions';
import { priceFormat } from '../../helpers/price';

const TransactionHistory: React.FC = () => {
    const {allTransactions, deleteTransaction, accounts} = useStore()
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'];

    const getAccount = (transaction: Transaction) => {
        const find = accounts.filter(a => a.accountId === transaction.accountId)
        return (find.length ? find[0] : null) as Account
    }

	function isMonthActive(month: number) {
		let dt = new Date();
		let currentMonth = dt.getMonth();

		return (month === currentMonth ? "active-chip-dark" : "dark");

	}

    const directionIndicator = (direction: string, amount: number) => {
        if(direction === 'income'){
            return `+ ${priceFormat(amount)}`;
        }
        return `- ${priceFormat(amount)}`;
    }

    const listTransactions = allTransactions.map((trans: Transaction, i) => {
        return (
            <IonCard key={i}>
                <IonCardHeader className="transaction-top">
                    <IonItem lines="none">
                        <div className="transacion-name">{trans.transactionDescription}</div>
                        <IonIcon className="corner" icon={trashOutline} onClick={() => deleteTransaction(trans, getAccount(trans))}></IonIcon>
                    </IonItem>
                </IonCardHeader>
                <IonItem lines="none">
                    <IonLabel>Amount:</IonLabel>
                    <IonLabel slot="end" color="body-light">{directionIndicator(trans.transactionDirection, trans.amount)}</IonLabel>
                </IonItem>
                <IonItem lines="none">
                    <IonLabel>Date:</IonLabel>
                    <IonLabel slot="end" color="body-light">{moment(trans.transactionDate).format('MMM DD, YYYY')}</IonLabel>
                </IonItem>
                {/* {categoryPart(trans.category)} */}
            </IonCard>
        );
    });

    return (
        <IonPage className="transactions">
            <IonContent fullscreen color="light">
                <IonHeader className="header">
					<IonToolbar color="light" className="top-bar">
						<IonTitle size="large" className="title">History</IonTitle>
						<IonButtons slot="end">
							<Menu/>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
                <div className="months-filter">
                    {months.map((value, index) => 
                        <IonChip className={isMonthActive(index)} key={index}>
                            <IonLabel>{value}</IonLabel>
                        </IonChip>
                    )}
                </div>

                {listTransactions}
            </IonContent>
        </IonPage>
    );
};

export default TransactionHistory;
