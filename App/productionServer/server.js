//requires
const express = require('express');
var path = require('path');
const port = 8080;

//set the app
const app = express();
const http = require('http').createServer(app);
//------------------------------------------------------------------------------------------------
//define routes
app.use("/", express.static("build"));
app.use("/", express.static("public"));
app.use("/public", express.static("public"));

app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname, "build", "index.html"));
});
//------------------------------------------------------------------------------------------------
http.listen(port, () => {
    console.log(`Node app is listening on port: ${port}`);
});
