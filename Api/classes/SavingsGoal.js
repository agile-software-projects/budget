const { rows } = require('mssql');
const { v4: uuidv4 } = require('uuid');
const databaseFunctions = require('../core/databaseFunctions');
const { winston } = require('../scripts/env');
const Transaction = require("./Transaction");

class SavingsGoal {
    constructor(savingsGoal) {
        this.goalId = savingsGoal.goalId;
        this.goalName = savingsGoal.goalName;
        this.goalAmount = savingsGoal.goalAmount;
        this.creationDate = savingsGoal.creationDate;
        this.targetDate = savingsGoal.targetDate;
        this.active = Boolean(savingsGoal.active);
        this.currentAmount = savingsGoal.currentAmount;
        this.accountId = savingsGoal.accountId;
    }

    wholeSavingsGoal() {
        return {
            goalId: this.goalId,
            goalName: this.goalName,
            goalAmount: this.goalAmount,
            creationDate: this.creationDate,
            targetDate: this.targetDate,
            active: Boolean(this.active),
            currentAmount: this.currentAmount,
            accountId: this.accountId
        }
    }

    assignSavingsGoalId() {
        this.goalId = uuidv4();
        return this;
    }

    validate() {
        if (this.goalId.length !== 36) { throw new Error("The goalId is not valid"); }
        if (this.accountId.length !== 36) { throw new Error("The accountId is not valid"); }

        const stringRe = /^(?=[a-zA-Z0-9._\ \-]{1,32}$)/;
        if (!(stringRe.test(this.goalName))) { throw new Error("The goal name is not valid"); }

        if (typeof this.active !== "boolean" && this.active !== 1 && this.active !== 0) { throw new Error("The active flag is not valid"); }

        if (!(Number.isInteger(this.goalAmount))) { throw new Error("total goal amount must be of type Int"); }
        if (!(Number.isInteger(this.currentAmount))) { throw new Error("current goal amount must be of type Int"); }

        const isDate = (date) => {
            return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
        }
        if (!(isDate(this.creationDate))) { throw new Error("creation date must be of Date type"); }
        if (!(isDate(this.targetDate))) { throw new Error("target date must be of Date type"); }

        return this;
    }

    activate() {
        this.active = true;
        return this;
    }

    deactivate() {
        this.active = false;
        return this;
    }

    verifyAccount() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("account", {accountId: this.accountId}).then((accounts) => {
                if (accounts.length === 1) {
                    resolve(accounts[0]);
                } else {
                    reject({error: "The account does not exist"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    insertSavingsGoal() {
        //check it doesnt exist. insert if not
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("goal", {goalId: this.goalId}).then((goals) => {
                if (goals.length === 0) {
                    this.creationDate = new Date();
                    databaseFunctions.insertTableRow("goal", this.wholeSavingsGoal()).then((rowsAffected) => {
                        if (rowsAffected[0] === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "could not insert savings goal"});
                        }
                    }).catch((err) => {
                        reject({error: err});
                    });
                } else {
                    reject({error: "the savings goal already exists"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    addSavingsLine(data, transactionId) {
        return new Promise((resolve, reject) => {
            const transaction = new Transaction({
                transactionId: transactionId,
                accountId: data.accountId,
                transactionDescription: data.transactionDescription,
                transactionDate: data.transactionDate,
                transactionDirection: data.transactionDirection,
                amount: data.amount,
                recurring: false,
                recurrencePeriod: data.recurrencePeriod,
            });

            try {
                transaction.validate().insertTransaction().then(() => {
                    databaseFunctions.insertTableRow("transactionGoalLink", {linkId: uuidv4(), transactionId: transaction.transactionId, goalId: this.goalId}).then((rowsAffected) => {
                        if (rowsAffected[0] === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "could not link goal to transaction"})
                        }
                    }).catch((err) => {
                        winston.error(err.message)
                        reject({error: err});
                    });
                }).catch((err) => {
                    winston.error(err.message)
                    reject({error: err})
                });
            } catch(err) {
                winston.error(err.message)
                reject({error: err.message});
            }
        });
    }

    populate() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("goal", {goalId: this.goalId}).then((goals) => {
                if (goals.length === 1) {
                    const savingsGoal = goals[0];
                    this.goalId = savingsGoal.goalId;
                    this.goalName = savingsGoal.goalName;
                    this.goalAmount = savingsGoal.goalAmount;
                    this.creationDate = savingsGoal.creationDate;
                    this.targetDate = savingsGoal.targetDate;
                    this.active = Boolean(savingsGoal.active);
                    this.currentAmount = savingsGoal.currentAmount;
                    this.accountId = savingsGoal.accountId;
                    resolve({success: true});
                } else {
                    resolve({success: false});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    update() {
        //check goal exists. update if it does
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("goal", {goalId: this.goalId}).then((goals) => {
                if (goals.length === 1) {
                    databaseFunctions.updateTableRow("goal", {goalId: this.goalId}, this.wholeSavingsGoal()).then((rowsAffected) => {
                        if (rowsAffected === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "Savings goal not updated"})
                        }
                    }).catch((err) => {
                        reject({error: err});
                    });
                } else {
                    reject({error: "The savings goal does not exist"});
                }
            }).catch((err) => {
                reject({error: err});
            })
        });
    }

    deleteSavingsGoal() {
        return new Promise((resolve, reject) => {
            databaseFunctions.deleteTableRows("goal", {goalId: this.goalId}).then((rowsAffected) => {
                if (rowsAffected.rowsAffected[0] === 1) {
                    resolve({success: true});
                } else {
                    reject({error: "Savings goal not deleted"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }
}

module.exports = SavingsGoal