export default function validateBudgetForm(values: any) {
    
    let errors: any = {};

    const inFuture = (date: Date) => {
        let targetDate = new Date(date);
        return targetDate.setHours(0,0,0,0) >= new Date().setHours(0,0,0,0)
    };

    if (!values.budgetName.trim()) {
        errors.budgetName = "Description required";
    } else if (!/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/.test(values.budgetName)) {
        errors.budgetName = "Only letters and numbers allowed";
    }

    if (!values.totalLimit.toString().trim()) {
        errors.totalLimit = "Target amount required";
    } else if (!/^[1-9]\d*(\.\d+)?$/.test(values.totalLimit)) {
        errors.totalLimit = "Amount should be numbers only";
    }

    if (!values.currentAmount.toString().trim()) {
        errors.currentAmount = "Current amount required";
    } else if (!/^[0-9]\d*(\.\d+)?$/.test(values.currentAmount)) {
        errors.currentAmount = "Amount should be numbers only";
    }

    if (!values.endDate.trim()) {
        errors.endDate = "Target date required";
    } else if (!inFuture(values.endDate)) {
        errors.endDate = "Target date cannot be before current date";
    }
    
    return errors;
}