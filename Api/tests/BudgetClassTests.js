const assert = require('assert');
const Budget = require('../classes/Budget');

const goodData = {
    budgetId: "00000000-0000-0000-0000-000000000000",
    budgetName: "some budget",
    totalLimit: 45000,
    accountId: "00000000-0000-0000-0000-000000000000",
    active: true,
    resetPeriod: "30d",
    currentPeriod: "00000000-0000-0000-0000-000000000000",
    startDate: "2021-01-01 23:07:51.523",
    endDate: "2021-01-31 23:07:51.523"
}

const badGuid = {
    budgetId: "00000000-0000-0000-000000000000",
    budgetName: "some budget",
    totalLimit: 45000,
    accountId: "00000000-0000-0000-0000",
    active: true,
    resetPeriod: "30d",
    currentPeriod: "00000000-0000-0000-0000-000000000000",
    startDate: "2021-01-01 23:07:51.523",
    endDate: "2021-01-31 23:07:51.523"
}

const badName = {
    budgetId: "00000000-0000-0000-0000-000000000000",
    budgetName: "some b%$£^£$%%£udget",
    totalLimit: 45000,
    accountId: "00000000-0000-0000-0000-000000000000",
    active: true,
    resetPeriod: "30d",
    currentPeriod: "00000000-0000-0000-0000-000000000000",
    startDate: "2021-01-01 23:07:51.523",
    endDate: "2021-01-31 23:07:51.523"
}

const badInt = {
    budgetId: "00000000-0000-0000-0000-000000000000",
    budgetName: "some budget",
    totalLimit: 45000.345,
    accountId: "00000000-0000-0000-0000-000000000000",
    active: true,
    resetPeriod: "30d",
    currentPeriod: "00000000-0000-0000-0000-000000000000",
    startDate: "2021-01-01 23:07:51.523",
    endDate: "2021-01-31 23:07:51.523"
}

const badReset = {
    budgetId: "00000000-0000-0000-0000-000000000000",
    budgetName: "some budget",
    totalLimit: 45000,
    accountId: "00000000-0000-0000-0000-000000000000",
    active: true,
    resetPeriod: "30days",
    currentPeriod: "00000000-0000-0000-0000-000000000000",
    startDate: "2021-01-01 23:07:51.523",
    endDate: "2021-01-31 23:07:51.523"
}

const badBool = {
    budgetId: "00000000-0000-0000-0000-000000000000",
    budgetName: "some budget",
    totalLimit: 45000,
    accountId: "00000000-0000-0000-0000-000000000000",
    active: 123,
    resetPeriod: "30d",
    currentPeriod: "00000000-0000-0000-0000-000000000000",
    startDate: "2021-01-01 23:07:51.523",
    endDate: "2021-01-31 23:07:51.523"
}

const badDate = {
    budgetId: "00000000-0000-0000-0000-000000000000",
    budgetName: "some budget",
    totalLimit: 45000,
    accountId: "00000000-0000-0000-0000-000000000000",
    active: true,
    resetPeriod: "30d",
    currentPeriod: "00000000-0000-0000-0000-000000000000",
    startDate: "2021-01-l",
    endDate: "2021-01-31 23:07:51.523"
}

describe("Test the budget class", () => {
    it("Should retain a bugdet", (done) => {
        const budget = new Budget(goodData);

        const b = budget.wholeBudget();

        assert.strictEqual(budget.budgetId, b.budgetId);
        assert.strictEqual(budget.budgetName, b.budgetName);
        assert.strictEqual(budget.totalLimit, b.totalLimit);
        assert.strictEqual(budget.accountId, b.accountId);
        assert.strictEqual(budget.active, b.active);
        assert.strictEqual(budget.resetPeriod, b.resetPeriod);
        assert.strictEqual(budget.currentPeriod, b.currentPeriod);
        assert.strictEqual(budget.startDate, b.startDate);
        assert.strictEqual(budget.endDate, b.endDate);
        done();
    });

    it("assign a budgetId", (done) => {
        const budget = new Budget({budgetId: ""});
        budget.assignBudgetId();
        assert.strictEqual(budget.budgetId.length, 36);
        done();
    });

    it("allow a valid budget to pass validation", (done) => {
        const budget = new Budget(goodData);
        assert.strictEqual(budget.validate(), budget);
        done();
    });

    it("fail validation on Guid", (done) => {
        const budget = new Budget(badGuid);
        //assert.strictEqual(budget.validate(), false);
        let validate = function () { budget.validate() };
        assert.throws(validate, Error, "The budgetId is not valid");
        done();
    });

    it("fail validation on Name", (done) => {
        const budget = new Budget(badName);
        //assert.strictEqual(budget.validate(), false);
        let validate = function () { budget.validate() };
        assert.throws(validate, Error, "The budget name is not valid");
        done();
    });

    it("fail validation on Int", (done) => {
        const budget = new Budget(badInt);
        //assert.strictEqual(budget.validate(), false);
        let validate = function () { budget.validate() };
        assert.throws(validate, Error, "total limit must be of type Int");
        done();
    });

    it("fail validation on Reset", (done) => {
        const budget = new Budget(badReset);
        //assert.strictEqual(budget.validate(), false);
        let validate = function () { budget.validate() };
        assert.throws(validate, Error, "total limit must be of type Int");
        done();
    });

    it("fail validation on Bool", (done) => {
        const budget = new Budget(badBool);
        //assert.strictEqual(budget.validate(), false);
        let validate = function () { budget.validate() };
        assert.throws(validate, Error, "The active flag is not valid");
        done();
    });

    it("fail validation on Date", (done) => {
        const budget = new Budget(badDate);
        let validate = function () { budget.validate() };
        assert.throws(validate, Error, "start date must be of Date type");
        done();
    });

    it("activate the budget", (done) => {
        const budget = new Budget(goodData);
        budget.activate();
        assert.strictEqual(budget.active, true);
        done();
    });

    it("deactivate the budget", (done) => {
        const budget = new Budget(goodData);
        budget.deactivate();
        assert.strictEqual(budget.active, false);
        done();
    });

    it("verify the account", (done) => {
        const budget = new Budget(goodData);

        setTimeout(() => {
            budget.validate().verifyAccount().then((account) => {
                assert.strictEqual(account.accountId, budget.accountId);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("insert the budget", (done) => {
        const budget = new Budget(goodData);

        setTimeout(() => {
            budget.assignBudgetId().assignCurrentPeriodId().validate().insertBudget().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("insert the account", (done) => {
        const budget = new Budget(goodData);

        setTimeout(() => {
            budget.assignBudgetId().validate().insertBudget().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    const budgetId = "226794d2-d16a-4da5-9b53-910b7dfa664e";

    it.skip("populate the account", (done) => {
        const budget = new Budget({budgetId: budgetId});

        setTimeout(() => {
            budget.populate().then((result) => {
                assert.strictEqual(result.success, true);
                assert.strictEqual(budget.budgetName, goodData.budgetName);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("update the account", (done) => {
        const budget = new Budget(goodData);
        budget.budgetId = budgetId;

        setTimeout(() => {
            budget.deactivate().validate().update().then((result) => {
                assert.strictEqual(result.success, true);
                assert.strictEqual(budget.active, false);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("populate the account", (done) => {
        const budget = new Budget({budgetId: budgetId});

        setTimeout(() => {
            budget.deleteBudget().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });
});
