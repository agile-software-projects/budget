/*Create the users table*/
BEGIN TRANSACTION
CREATE TABLE dbo.users
	(
	userId nvarchar(38) NOT NULL,
	username nvarchar(64) NOT NULL,
	password nvarchar(4000) NOT NULL,
	email nvarchar(256) NOT NULL
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.users ADD CONSTRAINT
	PK_userId_users PRIMARY KEY CLUSTERED 
	(
	userId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_username_users ON dbo.users
	(
	username
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_email_users ON dbo.users
	(
	email
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.users SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the account table*/
BEGIN TRANSACTION
CREATE TABLE dbo.account
	(
	accountId nvarchar(38) NOT NULL,
	userId nvarchar(38) NOT NULL,
	accountType nvarchar(32) NOT NULL,
	accountName nvarchar(32) NOT NULL,
	creationDate datetime NOT NULL DEFAULT getdate(),
	accountEnabled tinyint NOT NULL DEFAULT 1,
	currentBalance int,
	lastUpdated datetime
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
BEGIN TRANSACTION
ALTER TABLE dbo.account ADD CONSTRAINT
	PK_accountId_account PRIMARY KEY CLUSTERED 
	(
	accountId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_userId_account ON dbo.account
	(
	userid
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.account SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the transactions table*/
BEGIN TRANSACTION
CREATE TABLE dbo.transactions
	(
	transactionId nvarchar(38) NOT NULL,
	accountId nvarchar(38) NOT NULL,
	transactionDate datetime NOT NULL DEFAULT getdate(),
	transactionDirection nvarchar(32) NOT NULL,
	amount int NOT NULL
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.transactions ADD CONSTRAINT
	PK_transactionId_transactions PRIMARY KEY CLUSTERED 
	(
	transactionId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_accountId_transactions ON dbo.transactions
	(
	accountId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.transactions SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the budget table*/
BEGIN TRANSACTION
CREATE TABLE dbo.budget
	(
	budgetId nvarchar(38) NOT NULL,
	budgetName nvarchar(64) NOT NULL,
	totalLimit int NOT NULL,
	accountId nvarchar(38) NOT NULL,
	active tinyint NOT NULL DEFAULT 1,
	resetPeriod nvarchar(32) NOT NULL DEFAULT '1month'
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.budget ADD CONSTRAINT
	PK_budgetId_budget PRIMARY KEY CLUSTERED 
	(
	budgetId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_accountId_budget ON dbo.budget
	(
	accountId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.budget SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the users table*/
BEGIN TRANSACTION
CREATE TABLE dbo.goal
	(
	goalId nvarchar(38) NOT NULL,
	goalName nvarchar(32) NOT NULL,
	goalAmount int NOT NULL,
	creationDate datetime NOT NULL DEFAULT getdate(),
	targetDate datetime,
	active tinyint NOT NULL DEFAULT 1,
	currentAmount int NOT NULL DEFAULT 0,
	accountId nvarchar(38) NOT NULL
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.goal ADD CONSTRAINT
	PK_goalId_goal PRIMARY KEY CLUSTERED 
	(
	goalId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.goal SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the category table*/
BEGIN TRANSACTION
CREATE TABLE dbo.category
	(
	categoryId nvarchar(38) NOT NULL,
	categoryName nvarchar(64) NOT NULL,
	parentCategoryId nvarchar(38),
	isDefault tinyint NOT NULL DEFAULT 0
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.category ADD CONSTRAINT
	PK_categoryId_category PRIMARY KEY CLUSTERED 
	(
	categoryId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.category SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the transactionGoalLink table*/
BEGIN TRANSACTION
CREATE TABLE dbo.transactionGoalLink
	(
	linkId nvarchar(38) NOT NULL,
	transactionId nvarchar(38) NOT NULL,
	goalId nvarchar(38) NOT NULL
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.transactionGoalLink ADD CONSTRAINT
	PK_linkId_transactionGoalLink PRIMARY KEY CLUSTERED 
	(
	linkId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_transactionId_transactionGoalLink ON dbo.transactionGoalLink
	(
	transactionId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_goalId_transactionGoalLink ON dbo.transactionGoalLink
	(
	goalId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.transactionGoalLink SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the transactionGoalLink table*/
BEGIN TRANSACTION
CREATE TABLE dbo.transactionBudgetLink
	(
	linkId nvarchar(38) NOT NULL,
	transactionId nvarchar(38) NOT NULL,
	budgetId nvarchar(38) NOT NULL
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.transactionBudgetLink ADD CONSTRAINT
	PK_linkId_transactionBudgetLink PRIMARY KEY CLUSTERED 
	(
	linkId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_transactionId_transactionBudgetLink ON dbo.transactionBudgetLink
	(
	transactionId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_budgetId_transactionBudgetLink ON dbo.transactionBudgetLink
	(
	budgetId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.transactionBudgetLink SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the transactionGoalLink table*/
BEGIN TRANSACTION
CREATE TABLE dbo.budgetCategoryLink
	(
	linkId nvarchar(38) NOT NULL,
	budgetId nvarchar(38) NOT NULL,
	categoryId nvarchar(38) NOT NULL
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.budgetCategoryLink ADD CONSTRAINT
	PK_linkId_budgetCategoryLink PRIMARY KEY CLUSTERED 
	(
	linkId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_budgetId_budgetCategoryLink ON dbo.budgetCategoryLink
	(
	budgetId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_categoryId_budgetCategoryLink ON dbo.budgetCategoryLink
	(
	categoryId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.budgetCategoryLink SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


/*Create the transactionGoalLink table*/
BEGIN TRANSACTION
CREATE TABLE dbo.transactionCategoryLink
	(
	linkId nvarchar(38) NOT NULL,
	transactionId nvarchar(38) NOT NULL,
	categoryId nvarchar(38) NOT NULL
	)  ON [PRIMARY]
GO
/*Set the users table keys*/
ALTER TABLE dbo.transactionCategoryLink ADD CONSTRAINT
	PK_linkId_transactionCategoryLink PRIMARY KEY CLUSTERED 
	(
	linkId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IX_transactionId_transactionCategoryLink ON dbo.transactionCategoryLink
	(
	transactionId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX IX_categoryId_transactionCategoryLink ON dbo.transactionCategoryLink
	(
	categoryId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.transactionCategoryLink SET (LOCK_ESCALATION = TABLE)
GO
COMMIT