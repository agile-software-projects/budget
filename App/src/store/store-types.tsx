export interface Store {
    loadingApp: boolean
    loggedIn: boolean
    showNotification: Notification
    user: User
    selectedAccount: Account
    accounts: Account[]
    allTransactions: Transaction[]
    categories: Category[]
    goals: Goal[]
    budgets: Budget[]
}

export interface Notification {
    show: boolean
    message: string
    color: string
}

export interface User {
    userId: string
    username: string
    email: string
}

export interface Account {
    accountId: string
    userId: string
    accountName: string
    accountType: string
    creationDate: Date
    lastUpdated: Date
    accountEnabled: number
    currentBalance: number
}

export interface Category {
    categoryId: string
    categoryName: string
    parentCategoryId: string
    isDefault: number
}

export interface Transaction {
    transactionId: string
	accountId: string
	transactionDate: Date
	transactionDirection: string
	amount: number
    transactionDescription: string
    recurring: string
    recurrencePeriod: string
}

export interface Goal {
    goalId: string
    currentAmount: number
	goalName: string
	goalAmount: number
    targetDate: Date
    accountId: String
}

export interface Budget {
    budgetId: string
    currentAmount: number
	budgetName: string
	totalLimit: number
    endDate: Date
    categoryName: string
    accountId: string
    resetPeriod: string
}

export const actions = {
    app: {
        LOADING: 'LOADING',
        LOADED: 'LOADED',
        SHOW_NOTIFICATION: 'SHOW_NOTIFICATION',
        HIDE_NOTIFICATION: 'HIDE_NOTIFICATION'
    },
    auth: {
        LOGIN: 'LOGIN',
        LOGOUT: 'LOGOUT',
    },
    user: {
        SET_USER: 'SET_USER',
        UPDATE_USER: 'UPDATE_USER',
        SET_CURRENCY: 'SET_CURRENCY'
    },
    accounts: {
        SET_ACCOUNTS: 'SET_ACCOUNTS',
        ADD_ACCOUNT: 'ADD_ACCOUNT',
        SAVE_ACCOUNT: 'SAVE_ACCOUNT',
        SELECT_ACCOUNT: 'SELECT_ACCOUNT',
        DELETE_ACCOUNT: 'DELETE_ACCOUNT',
    },
    transactions: {
        SET_ALL_TRANSACTIONS: 'SET_ALL_TRANSACTIONS',
        SET_ACCOUNT_TRANSACTIONS: 'SET_ACCOUNT_TRANSACTIONS',
        ADD_TRANSACTION: 'ADD_TRANSACTION',
        SAVE_TRANSACTION: 'SAVE_TRANSACTION',
        DELETE_TRANSACTION: 'DELETE_TRANSACTION',
        SET_SINGLE_TRANSACTION: 'SET_SINGLE_TRANSACTION'
    },
    categories: {
        SET_CATEGORIES: 'SET_CATEGORIES',
    },
    goals: {
        SET_GOALS: 'SET_GOALS',
        ADD_GOAL: 'ADD_GOAL',
        SAVE_GOAL: 'SAVE_GOAL',
        DELETE_GOAL: 'DELETE_GOAL',
        ADD_SAVINGS: 'ADD_SAVINGS',
        SET_ACCOUNT_GOALS: 'SET_ACCOUNT_GOALS',
        SELECT_GOAL: 'SELECT_GOAL'
    },
    budget: {
        SET_BUDGETS: 'SET_BUDGETS',
        ADD_BUDGET: 'ADD_BUDGET',
        SAVE_BUDGET: 'SAVE_BUDGET',
        DELETE_BUDGET: 'DELETE_BUDGET',
        SELECT_BUDGET: 'SELECT_BUDGET'
    }
}