const assert = require('assert');
const Transaction = require('../classes/Transaction');

//any other consts (like auth)

const goodData = {
    transactionId: "00000000-0000-0000-0000-000000000000",
    accountId: "00000000-0000-0000-0000-000000000000",
    transactionDescription: "pizza",
    transactionDate: "2021-01-27 23:07:51.523",
    transactionDirection: "expense",
    amount: 3599
}

const badGuid = {
    transactionId: "0-000000000000",
    accountId: "00000-000000000000",
    transactionDescription: "pizza",
    transactionDate: "2021-01-27 23:07:51.523",
    transactionDirection: "expense",
    amount: 3599
}

const badName = {
    transactionId: "0-000000000000",
    accountId: "00000-000000000000",
    transactionDescription: "piz$%^£$a",
    transactionDate: "2021-01-27 23:07:51.523",
    transactionDirection: "expense",
    amount: 3599
}


const badDate = {
    transactionId: "00000000-0000-0000-0000-000000000000",
    accountId: "00000000-0000-0000-0000-000000000000",
    transactionDescription: "pizza",
    transactionDate: "2wersfsadfaweaw-27 23:07:51.523",
    transactionDirection: "expense",
    amount: 3599
}

const badDirection = {
    transactionId: "00000000-0000-0000-0000-000000000000",
    accountId: "00000000-0000-0000-0000-000000000000",
    transactionDescription: "pizza",
    transactionDate: "2021-01-27 23:07:51.523",
    transactionDirection: "£$%£$%2£se",
    amount: 3599
}

const badAmount = {
    transactionId: "00000000-0000-0000-0000-000000000000",
    accountId: "00000000-0000-0000-0000-000000000000",
    transactionDescription: "pizza",
    transactionDate: "2021-01-27 23:07:51.523",
    transactionDirection: "expense",
    amount: 3599.125
}

describe("Tests the transaction class", () => {
    it.skip("returns the whole transaction", (done) => {
        const transaction = new Transaction(goodData);
        assert.strictEqual(transaction.transactionId, goodData.transactionId);
        assert.strictEqual(transaction.accountId, goodData.accountId);
        assert.strictEqual(transaction.transactionDescription, goodData.transactionDescription);
        assert.strictEqual(transaction.transactionDate, goodData.transactionDate);
        assert.strictEqual(transaction.transactionDirection, goodData.transactionDirection);
        assert.strictEqual(transaction.amount, goodData.amount);
        done();
    });

    it.skip("successfully validates a good transaction object", (done) => {
        const transaction = new Transaction(goodData);
        assert.strictEqual(transaction.validate(), transaction);
        done();
    });

    it.skip("fails validation on bad ID", (done) => {
        const transation = new Transaction(badGuid);
        assert.strictEqual(transation.validate(), false);
        done();
    });

    it.skip("fails validation on bad Description", (done) => {
        const transation = new Transaction(badName);
        assert.strictEqual(transation.validate(), false);
        done();
    });

    it.skip("fails validation on bad date", (done) => {
        const transation = new Transaction(badDate);
        assert.strictEqual(transation.validate(), false);
        done();
    });

    it.skip("fails validation on bad Direction", (done) => {
        const transation = new Transaction(badDirection);
        assert.strictEqual(transation.validate(), false);
        done();
    });

    it.skip("fails validation on bad Amount", (done) => {
        const transation = new Transaction(badAmount);
        assert.strictEqual(transation.validate(), false);
        done();
    });

    it.skip("verifies the account exists", (done) => {
        const transaction = new Transaction(goodData);

        setTimeout(() => {
            transaction.verifyAccount().then((account) => {
                assert.strictEqual(account.accountId, goodData.accountId);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("inserts a transaction", (done) => {
        const transaction = new Transaction(goodData);

        setTimeout(() => {
            transaction.validate().assignTransactionId().insertTransaction().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("populates a transaction from the id", (done) => {
        const transactionId = "8cba8fb1-7277-44ea-ab40-192935f45297";
        const transaction = new Transaction({transactionId: transactionId});
    
        setTimeout(() => {
            transaction.populate().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });

    it.skip("updates a transaction", (done) => {
        const transactionId = "8cba8fb1-7277-44ea-ab40-192935f45297";
        
        const transaction = new Transaction(goodData);
        transaction.transactionDirection = "income";
        transaction.transactionId = transactionId;
        setTimeout(() => {
            transaction.validate().update().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    }); 

    it.skip("deletes a transaction", (done) => {
        const transactionId = "8cba8fb1-7277-44ea-ab40-192935f45297";
        
        const transaction = new Transaction(goodData);
        transaction.transactionId = transactionId;

        setTimeout(() => {
            transaction.deleteTransaction().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    }); 
});
