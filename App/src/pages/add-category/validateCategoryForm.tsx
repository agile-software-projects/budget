export default function validateCategoryForm(values: any) {

    let errors: any = {};

    if (!values.catName.trim()) {
        errors.description = "Name required";
    } else if (!/^[A-Za-z0-9]*$/.test(values.catName)) {
        errors.description = "Only letters and numbers allowed";
    }

    return errors;
}