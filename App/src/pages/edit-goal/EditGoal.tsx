import React, {useState, useEffect} from 'react';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonGrid, IonItem, IonLabel, IonInput, IonDatetime, IonProgressBar, IonSelect, IonSelectOption, IonPage } from '@ionic/react';
import { closeOutline } from 'ionicons/icons';
import './EditGoal.css';
import { useStore } from '../../store/store-actions';
import validate from './validateGoalForm';
import { convertPriceFormat, priceFormat } from '../../helpers/price';

const EditGoal = (props: any) => {

	interface IErrors {
        goalName?: string,
        goalAmount?: string,
        targetDate?: string,
        currentAmount?: string
    }

	const { hide } = props
	
	const [errors, setErrors] = useState<IErrors>({});

    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success
	const {selectedGoal, saveGoal, accounts} = useStore();

	const [values, setValues] = useState({
		goalId: selectedGoal.goalId,
        goalName: selectedGoal.goalName,
        goalAmount: priceFormat(selectedGoal.goalAmount),
        targetDate: selectedGoal.targetDate,
        active: 1,
        currentAmount: priceFormat(selectedGoal.currentAmount),
		accountId: selectedGoal.accountId
    })
	
	const handleChange = (e: any) => {

        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
	}

    const handleSubmit = (e: any) => {
        e.preventDefault();
        setErrors(validate(values));
        setIsSubmitted(true);
	}
	
	useEffect(() => {
        if (isSubmitted && Object.keys(errors).length === 0) {

			setSubmission(1);

            const data: any = {
				goalId: values.goalId,
                goalName: values.goalName,
                goalAmount: convertPriceFormat(values.goalAmount),
                active: values.active,
                targetDate: values.targetDate,
                currentAmount: convertPriceFormat(values.currentAmount),
                accountId: values.accountId
            }

            try{
				setSubmission(2);
				saveGoal(data);
                hide(false);
            }catch(e){
                console.error(e)
                setSubmission(-1);
            }
            
        } else if (Object.keys(errors).length !== 0) {
            setSubmission(-1);
        }
    },
        [isSubmitted, errors, values.goalId, values.goalName, values.goalAmount, values.active, values.targetDate, values.currentAmount, values.accountId, saveGoal, hide]
    );

    const messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error</IonLabel>;
        } else if(submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Saved</IonLabel>;
        } else {
            return null;
        }
    }

	return (
		<IonPage className='edit-goal'>
            <IonContent fullscreen>
				<IonHeader className="modal-header">
					<IonToolbar className="top-bar">
						<IonTitle className="edit-goal-title">Edit Saving Goal</IonTitle>
						<IonButtons slot="end">
							<IonButton onClick={() => {hide(false)}} className="btn-menu">
								<IonIcon slot="icon-only" icon={closeOutline} className="menu-icon"/>
							</IonButton>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
				<div className="modal-content">
					<form className="goal-budget-form" onSubmit={handleSubmit}>
						<IonGrid>
							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="text" 
									name="goalName"
									className="form-input"
									placeholder="Description"
									value={values.goalName}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.goalName}</IonLabel>
							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="number"
									step=".01"
									name="goalAmount"
									className="form-input"
									placeholder="Target Amount"
									value={values.goalAmount}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.goalAmount}</IonLabel>
							<IonItem className="ion-no-padding ion-margin">
								<IonDatetime 
									displayFormat="DD-MMM-YYYY" 
									placeholder="Target Date" 
									className="form-input"
									name="targetDate"
									max="2099-12-31"
									value={values.targetDate.toString()}
									onIonChange={handleChange}
									>
								</IonDatetime> 
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.targetDate}</IonLabel>

							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="number"
									step=".01"
									name="currentAmount"
									className="form-input"
									placeholder="Current Amount"
									value={values.currentAmount}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.currentAmount}</IonLabel>

							<IonItem className="ion-no-padding ion-margin">
								<IonSelect onIonChange={handleChange} name="accountId" placeholder="Select an account" value={values.accountId}>
									{accounts.map((acc:any, i) => (
										<IonSelectOption value={acc.accountId} key={i}>
											{acc.accountName}
										</IonSelectOption>
									))}
								</IonSelect>
							</IonItem>

							<IonButton expand="block" type="submit" color="primary" id="btn-save" className="ion-margin">Save</IonButton>
							{messages()}
						</IonGrid>
					</form>
				</div>
            </IonContent>
        </IonPage>
	);
};

export default EditGoal;
