import React, { useState, useEffect } from 'react';
import { IonButton, IonButtons, IonIcon, IonLabel, IonToggle, IonInput, IonDatetime, IonSelect, IonSelectOption, IonGrid, IonRow, IonCol, IonProgressBar, IonPage, IonContent, IonHeader, IonToolbar, IonTitle } from '@ionic/react';
import AddCategory from '../add-category/AddCategory';
import Toggle from '../../ui-compontent/toggle/Toggle';
import CustomSelect from '../../ui-compontent/custom-select/CustomSelect';
import { closeOutline } from 'ionicons/icons';
import { useStore } from '../../store/store-actions';
import { convertPriceFormat } from '../../helpers/price';
import {apiUrl} from "../../helpers/env";

const EditTransaction = (props: any) => {    
    const units = ["Days", "Weeks", "Months", "Years"]

    const { hide } = props

    const {selectedTransaction, saveTransaction, categories, accounts} = useStore();
    const [errors, setErrors] = useState<IErrors>({});
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success
    const [categoriesList, setCategoriesList] = useState([]);

    const freqNr = selectedTransaction.recurrencePeriod.split(/D|W|M|Y/)[0]

    let recPeriod = selectedTransaction.recurrencePeriod;
    const charArr = [];
    for (let i = 0; i < recPeriod.length; i++) {
        if (isNaN(recPeriod[i] as any)){
            charArr.push(recPeriod[i]);
        }
    }

    const freqUnit = charArr.join("");

    const [values, setValues] = useState({
        descr: selectedTransaction.transactionDescription,
        transDate: selectedTransaction.transactionDate,
        amount: (selectedTransaction.amount/100).toFixed(2) || null,
        category: "",
        freqNr: freqNr,
        freqUnit: freqUnit,
    });
    const handleChange = (e: any) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e: any) => {
        e.preventDefault();
        // setErrors(validate(values));
        setIsSubmitted(true);
    }

    useEffect(() => {
        const catArr = [] as any;
        categories.forEach((cat: any) => {
            catArr.push(cat.categoryName);
        })
        setCategoriesList(catArr);

        fetch(`${apiUrl}/transaction/${selectedTransaction.transactionId}/getTransactionCategory`, {
            method: "get",
            credentials: "include"
        }).then((response) => {
            return response.json();
        }).then((transCategory) => {
            setValues({
                ...values,
                category: transCategory.categoryName
           })
        });
    }, [!categories]);

    useEffect(() => {
        // const userContext = [];
        // userContext.push(idb.getRecordFromId("clientUser", 1));
        // userContext.push(idb.loadFromDb("accounts"));
        // userContext.push(idb.loadFromDb("categories"));
        // Promise.all(userContext).then((results) => {
            // const userId = results[0].userId;
            // const accounts = results[1]
            const category = categories.filter((cat: any) => values.category === cat.categoryName)
            const account = accounts.filter(acc => acc.accountId === selectedTransaction.accountId)[0]
            if (isSubmitted && Object.keys(errors).length === 0) {
                setSubmission(1);
                
                const transaction = {
                    transactionId: selectedTransaction.transactionId,
                    transactionDescription: values.descr,
                    transactionDate: values.transDate,
                    accountId: selectedTransaction.accountId,
                    transactionDirection: expense ? "expense" : "income",
                    amount: convertPriceFormat(values.amount), 
                    recurring: selectedTransaction.recurring,
                    recurrencePeriod: selectedTransaction.recurrencePeriod,
                }
                
                saveTransaction(transaction as any, category[0].categoryId, account, selectedTransaction.amount).then(() => {
                    setSubmission(2);
                    setIsSubmitted(false);
                    hide(false);
                }).catch((err) => {
                    setSubmission(-1)
                });
                
            } else if (Object.keys(errors).length !== 0) {
                setSubmission(-1);
            }
        // }).catch((err) => {
        //     console.log(err);
        //     setSubmission(-1);
        // });
    },
        [isSubmitted, errors]
    );

    interface IErrors {
        descr?: string,
        transDate?: string
        amount?: number,
        category?: string,
        freqNr?: number,
        freqUnit?: string,
    }

    const [expense, setExpense] = useState<boolean>(selectedTransaction.transactionDirection === "expense")
    const [recurring, setRecurring] = useState<boolean>(false)

    const messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error saving the transaction</IonLabel>;
        } else if(submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Transaction saved</IonLabel>;
        } else {
            return null;
        }
    }

    const [showAddCategory, setShowAddCategory] = useState(false)

    const toggleAdd = (e:any) => {
        setShowAddCategory(!showAddCategory);
    }

    const handleExpense = (value: any) => {
        setExpense(value);
    } 
    return (
        <IonPage className="single-transaction">
            <IonHeader className="modal-header">
                <IonToolbar className="top-bar">
                    <IonTitle className="title">Edit</IonTitle>
                    <IonButtons slot="end">
                        <IonButton  onClick={() => {hide(false)}} className="btn-menu">
                            <IonIcon slot="icon-only" icon={closeOutline} className="menu-icon"/>
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen color="light">
                <IonInput value={values.descr} placeholder="Description" name="descr" onIonChange={handleChange}></IonInput>
                <IonGrid>
                    <Toggle expense={expense} first="Income" second="Expense" handle={(value: boolean) => handleExpense(value)} />
                    <IonRow>
                        <IonCol><IonDatetime displayFormat="DD MM YY" placeholder="Date" name="transDate" value={values.transDate as any} onIonChange={handleChange}></IonDatetime></IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol><IonInput type="number" step="0.01" value={values.amount} min="0.01" placeholder="Amount" name="amount" className={expense ? "negative" : "positive"} onIonChange={handleChange}></IonInput></IonCol>
                    </IonRow>
                    <CustomSelect id="category" value={values.category} placeholder="Category" name="category" status={!showAddCategory} handle={handleChange} handleAdd={(e: any) => toggleAdd(e)} list={categoriesList} />
                    {showAddCategory ? <AddCategory /> : null}
                    <IonRow>
                        <IonCol id="recurring" size="11">
                            <IonLabel>Recurring</IonLabel>
                        </IonCol>
                        <IonCol size="1">
                            <IonToggle checked={recurring} onIonChange={e => setRecurring(e.detail.checked)} />
                        </IonCol>
                    </IonRow>
                    {recurring ?
                        <IonRow>
                            <IonCol><IonInput min="1" type="number" name="freqNr" value={values.freqNr} onIonChange={handleChange}></IonInput></IonCol>
                            <IonCol>
                                <IonSelect value={values.freqUnit} name="freqUnit" onIonChange={handleChange}>
                                    {units.map((unit, index) => <IonSelectOption value={unit} key={index} >{unit}</IonSelectOption>)}
                                </IonSelect>
                            </IonCol>
                        </IonRow>
                        : null
                    }
                    <IonRow>
                        <IonCol>
                            <IonButton expand="block" type="button" color="primary" id="btn-save-goal" className="ion-margin" onClick={handleSubmit}>Save</IonButton>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        <IonCol>
                            {messages()}
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonContent>
        </IonPage>
    );
};

export default EditTransaction;
