import React from 'react';
import { IonLabel, IonRow, IonCol } from '@ionic/react';
import './Toggle.css';

const Toggle = (props: any) => {

    return (
        <IonRow className="group">
            <IonCol className="no-padding"><IonLabel className={props.expense ? "choice-item left" : "choice-item left selected"} onClick={e => props.handle(false)}>{props.first}</IonLabel></IonCol>
            <IonCol className="no-padding"><IonLabel className={props.expense ? "choice-item right selected" : "choice-item right"} onClick={e => props.handle(true)}>{props.second}</IonLabel></IonCol>
        </IonRow>
    );
};

export default Toggle;
