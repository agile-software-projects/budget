const express = require('express');
const router = express.Router();
const winston = require('../core/winston');
const env = require('../scripts/env');
const jwt = require('jsonwebtoken');
const User = require('../classes/User');

router.all('*', (req, res, next) => {
    if(env.devDatabase.enabled) {
        next();
    } else {
        res.status(500).json({error: "No database detected"});
    }
});

router.post('/signup', (req, res) => {
    const user = new User(req.body);
    user.username = req.sanitize(user.username);
    user.email = req.sanitize(user.email);
    try {
        user.assignUserId().validate();
        user.hashPassowrd().then((result) => {
            if (result.success) {
                user.active = 1;
                user.insertUser().then((results) => {
                    if (results.success === true) {
                        res.status(200);
                        return res.json({success: true});
                    } else {
                        res.status(400);
                        return res.json({error: "New user could not be inserted"});
                    }
                }).catch((err) => {
                    res.status(400);
                    winston.error(JSON.stringify(err));
                    return res.json(err);    
                });
            } else {
                res.status(400);
                return res.json({error: "There was a problem saving the user"});
            }
        });
    } catch(err) {
        winston.error(err.toString());
        res.status(400);
        return res.json({error: "New user could not be validated"});
    }
});

//testing. username something pwd somePa55w0rd!
router.post('/login', (req, res) => {
    const user = new User(req.body);
    user.username = req.sanitize(user.username);
    user.populate().then((result) => {
        if (result) {
            user.validate().comparePassword(req.body.password).then((result) => {
                console.log(result);
                if (result.success) {
                    //we have a match. load the auth token into the cookie
                    const payload = {username: user.username};
                    const token = jwt.sign(payload, env.jwt.key, {
                        expiresIn: '24h'
                    });
                    user.markActive(1).then(() => {
                        res.cookie('token', token, env.cookie);
                        res.status(200);
                        return res.json({success: true});
                    }).catch((err) => {
                        res.status(500);
                        return res.json(err);
                    })
                } else {
                    res.status(401);
                    return res.json({error: "could not authenticate"});
                }
            }).catch((err) => {
                console.log(err);
                res.status(401);
                return res.json(err);
            });
        } else {
            res.status(401);
            return res.json({error: "The user could not be loaded"});
        }
    }).catch((err) => {
        res.status(401);
        return res.json(err);
    });
});

router.post("/signout", (req, res) => {
    if (req.cookies && req.cookies.token) {
        jwt.verify(req.cookies.token, env.jwt.key, (err, decode) => {
            if (err) {
                res.status(401)
                return res.json({error: "Unauthorised: Invalid token"});
            } else {
                const user = new User({username: decode.username});
                user.populate().then(() => {
                    user.markActive(0).then(() => {
                        res.status(200);
                        return res.json({auth: false});
                    }).catch((err) => {
                        res.status(500);
                        return res.json(err);                        
                    });
                }).catch((err) => {
                    res.status(500);
                    return res.json(err);
                });
            }    
        });
    } else {
        res.status(200);
        return res.json({auth: false});
    }
})


module.exports = router;