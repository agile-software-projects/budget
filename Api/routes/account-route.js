const express = require('express');
const router = express.Router();
const winston = require('../core/winston');
const env = require('../scripts/env');
var databaseFunctions = null 
const Account = require('../classes/Account');

if(env.devDatabase.enabled) {
    databaseFunctions = require('../core/databaseFunctions');
}

router.all('*', (req, res, next) => {
    if(env.devDatabase.enabled) {
        next();
    } else {
        res.status(500).json({error: "No database detected"});
    }
});

// get all finance accounts
router.get('/getAccounts', async (req, res) => {
    try{
        res.status(400)

        const user = req.session.user

        //get accounts for given user
        const accounts = await databaseFunctions.selectTable("account", {userId: user.userId})
        
        res.status(200)
        res.json(accounts)
    }catch(e){
        console.error(e)
        res.status(res.statusCode)
        res.json({error: e})
    }
})

// get single finance account
router.get('/getAccount', async (req, res) => {
    try{
        res.status(400)

        const accountId = req.sanitize(req.query.accountId)
        
        const accounts = await databaseFunctions.selectTable("account", {accountId})
        if(!accounts.length){
            res.status(404)
            throw 'No acount found'
        }

        const account = accounts[0]

        res.status(200)
        res.json(account)
    }catch(e){
        console.error(e)
        res.status(res.statusCode)
        res.json({error: e})
    }
})

// insert finance account
router.post('/insertAccount', async (req, res) => {
    try{
        res.status(400)
        const user = req.session.user
        const accountData = {...req.body, userId: user.userId, creationDate: new Date(), lastUpdated: new Date(), accountEnabled: 1}
        const account = new Account(accountData)

        const validate = account.validate()
        if(!validate){
            res.status(403)
            throw 'Error validating account'
        }

        await account.verifyUser()

        await account.insertAccount()

        res.status(200)
        res.json({message: 'account inserted'})
    }catch(e){
        console.error(e)
        res.status(res.statusCode)
        res.json({error: e})
    }
})

// update finance account
router.post('/:accountid/updateAccount', async (req, res) => {
    try{
        res.status(400)

        const accountId = req.sanitize(req.params.accountid)
        const accounts = await databaseFunctions.selectTable("account", {accountId})
        if(!accounts.length){
            res.status(404)
            throw 'No account found'
        }

        const accountData = {...accounts[0], ...req.body}

        const account = new Account(accountData)

        const validate = account.validate()
        if(!validate){
            res.status(403)
            throw 'Error validating account'
        }

        await account.update()

        res.status(200)
        res.json({message: 'Account updated'})
    }catch(e){
        console.error(e)
        res.status(res.statusCode)
        res.json({error: e})
    }
})

// update finance account
router.post('/:accountid/deleteAccount', async (req, res) => {
    try{
        res.status(400)
        const account = new Account({accountId: req.sanitize(req.params.accountid)});

        await account.deleteAccount()

        res.status(200)
        res.json({message: 'Account deleted'})
    }catch(e){
        console.error(e)
        res.status(res.statusCode)
        res.json({error: e})
    }
})


module.exports = router;