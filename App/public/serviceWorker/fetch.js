self.addEventListener('fetch', (event) => {
    event.respondWith((async () => {
        try {
            const networkResponse = await fetch(event.request);
            return networkResponse;
        } catch (error) {
            const cache = await caches.open(cacheName);
            const cachedResponse = await cache.match(event.request);
            return cachedResponse;
        }
    })());
});