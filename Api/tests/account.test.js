const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { v4: uuidv4 } = require('uuid');

chai.use(chaiHttp);

const baseUrl = "http://localhost:3000/account" //can be localhost
//any other consts (like auth)

describe("Testing the get accounts endpoint", () => {
    it("Get Accounts List", (done) => {
        chai.request(baseUrl)
            .get('/getAccounts')
            .query({username: "richardsefton"})
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Insert Account", (done) => {
        chai.request(baseUrl)
            .post('/insertAccount')
            .send({
                accountId: uuidv4(),
                userId: '245784f3-3fb5-47f6-8263-f6ce7933bd13',
                accountType: 'bank-account',
                accountName: 'My Account',
                accountEnabled: 1,
                currentBalance: 4500,
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Update Account", (done) => {
        chai.request(baseUrl)
            .post('/updateAccount')
            .send({
                accountId: '5449d372-46bc-4d7d-8903-7c356ea4ed4b',
                userId: '245784f3-3fb5-47f6-8263-f6ce7933bd13',
                accountType: 'bank-account',
                accountName: 'My Account updated',
                accountEnabled: 1,
                currentBalance: 5700,
                creationDate: '2021-02-06T17:10:40.001Z',
                lastUpdated: '2021-02-06T17:10:40.001Z'
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Get Single Account", (done) => {
        chai.request(baseUrl)
            .get('/getAccount')
            .query({
                accountId: '5449d372-46bc-4d7d-8903-7c356ea4ed4b',
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });
});