export interface SignupFormValues {
    username: string
    email: string
    password: string
}