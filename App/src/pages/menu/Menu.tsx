import React, {useState} from 'react';
import { IonModal, IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonIcon, IonButton, IonGrid, IonRow, IonCol } from '@ionic/react';
import { albumsOutline, calendarOutline, cardOutline, cashOutline, closeOutline, gridOutline, logOutOutline, personOutline, pricetagOutline, walletOutline } from 'ionicons/icons';
import './Menu.css';
import { useHistory } from 'react-router';
import Auth from '../../helpers/Auth';
import { useStore } from '../../store/store-actions';

const Menu = (props: any) => {
    const [showModal, setShowModal] = useState(false)
    const {setLoggedIn, setLoadingApp} = useStore()

    const history = useHistory()
    const handleClick = (route: string) => {
        setShowModal(false)
        setTimeout(() => {
            history.push(route)
        }, 300)
    }

    const handleSignout = () => {
        setLoadingApp(true)
		const auth = new Auth()
		auth.signout().then(() => {
            setLoggedIn(false)
            setLoadingApp(false)
		}).then((err) => {
			console.log(err)
            setLoadingApp(false)
		});
	}

	return (
        <div>
            <IonButton onClick={() => setShowModal(true)} className="btn-menu">
                <IonIcon slot="icon-only" icon={gridOutline}className="menu-icon"/>
            </IonButton>
            <IonModal isOpen={showModal} cssClass='menu'>
                <IonContent fullscreen>
                    <IonHeader className="modal-header">
                        <IonToolbar className="top-bar">
                            <IonTitle size="large" className="title">Menu</IonTitle>
                            <IonButtons slot="end">
                                <IonButton onClick={() => setShowModal(false)} className="btn-menu">
                                    <IonIcon slot="icon-only" icon={closeOutline} className="menu-icon"/>
                                </IonButton>
                            </IonButtons>
                        </IonToolbar>
                    </IonHeader>
                    <IonGrid class="menu-items">
                        <IonRow>
                            <IonCol size="6" size-sm>
                                <div className="menu-item-box" onClick={() => handleClick('/home')}>
                                    <div className="menu-item-icon"><IonIcon icon={albumsOutline}/></div>
                                    <div className="menu-item-name">Accounts</div>
                                </div>
                            </IonCol>
                            <IonCol size="6" size-sm>
                                <div className="menu-item-box" onClick={() => handleClick('/goals')}>
                                    <div className="menu-item-icon"><IonIcon icon={calendarOutline}/></div>
                                    <div className="menu-item-name">Goals</div>
                                </div>
                            </IonCol>
                            <IonCol size="6" size-sm>
                                <div className="menu-item-box" onClick={() => handleClick('/budget')}>
                                    <div className="menu-item-icon"><IonIcon icon={cashOutline}/></div>
                                    <div className="menu-item-name">Budget</div>
                                </div>
                            </IonCol>
                            <IonCol size="6" size-sm>
                                <div className="menu-item-box" onClick={() => handleClick('/categories')}>
                                    <div className="menu-item-icon"><IonIcon icon={pricetagOutline}/></div>
                                    <div className="menu-item-name">Categories</div>
                                </div>
                            </IonCol>
                            <IonCol size="6" size-sm>
                                <div className="menu-item-box" onClick={() => handleClick('/transaction-history')}>
                                    <div className="menu-item-icon"><IonIcon icon={cardOutline}/></div>
                                    <div className="menu-item-name">Transactions</div>
                                </div>
                            </IonCol>
                            <IonCol size="6" size-sm>
                                <div className="menu-item-box" onClick={() => handleClick('/recurring-expenses')}>
                                    <div className="menu-item-icon"><IonIcon icon={walletOutline}/></div>
                                    <div className="menu-item-name">Recurring</div>
                                </div>
                            </IonCol>
                            {/* <IonCol size="6" size-sm>
                                <div className="menu-item-box" onClick={() => handleClick('/profile')}>
                                    <div className="menu-item-icon"><IonIcon icon={personOutline}/></div>
                                    <div className="menu-item-name">Profile</div>
                                </div>
                            </IonCol> */}
                            <IonCol size="6" size-sm>
                                <div className="menu-item-box" onClick={() => handleSignout()}>
                                    <div className="menu-item-icon"><IonIcon icon={logOutOutline}/></div>
                                    <div className="menu-item-name">Log Out</div>
                                </div>
                            </IonCol>
                        </IonRow>
                    </IonGrid>
                </IonContent>
            </IonModal>
        </div>
	)
}

export default Menu
