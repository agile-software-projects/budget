# Explaination of contents

These folder contains a library of assets that are normally in the core container and mapped to the respective core folders in the App and Api directories.

These are served in this way as they are a part of a library of generic solutions that can be expanded and used for other projects in the same you you would perhaps include an npm module. The folder also includes files necessary for setup, for example if we had individual development databases, it would start an 'example' database. It also houses the configuration files for the nginx container so a project can be hosted over https and a resolvable url rather than localhost. The reason for this is for testing Progressive Web Apps in a development environment. 

# Express Core Components

This respository houses core components that are shared across multiple apps. There are a few parts to this, but this document will aim to explain them all

---

# Assets

First and foremost the assets folder is the one to watch. This folder contains scripts that can be called in projects to aid in development. A library of common functions and components if you like. This currently includes:

* Winston
* Database Functions

## Winston

Winston is a logging script that has different levels of debugging. These are:

1. error
2. warn
3. info
4. http
5. websocket
6. sql
7. debug

You can require winston in any script by running ```const winston = require('./core/winston')```. The format for any console logs will be:

 ```
 2020-01-01 00:12:59[debug_level]: console log message
 ```

The logging levels are colour coded and can be changed in the env.js file you created in the scripts folder on each app.

Winston will also keep a history of your logs in the logs folders in the express-base framework.

To use winston simply call ```winston.{desired log level}("Your log message here")```

You can use console.logs in your development but winston is designed for persitent logs in a production environment. 


## Database Functions

# databaseFunctions

This contains some generic database functions for mssql using the tedious driver. If you use a different database to mssql, you should include your own functions in the scripts folder. You can require this in any script with the follwoing ```const databaseFunctions = require('./core/databaseFunctions');```

**If you find you have to do complicated join operations it is reccomended you do these as sql views and use the *selectTable* function to call them.**

The functions you can access in the scripts folder include are as follows:

**SELECT TABLE**
```
//The operand value can be AND or OR. Its an optional value that defaults to AND
databaseFunctions.selectTable("tablename", {tableField: "filterValue"}, operand).then((results) => {
    /*this will return the results in an array. */
}).catch((err) => {
    /*handle any errors */
});
```

**INSERT TABLE ROW**
```
databaseFunctions.insertTableRow("tablename", {tableField: "filterValue"}).then((rowsAffected) => {
    /*this will return the rowsAffected for your confirmation. */
}).catch((err) => {
    /*handle any errors */
});
```

**UPDATE TABLE ROW**
```
/*The first object in the parameters is to set filters, the second is the object to be updated. 
!!IMPORTANT!! The "DESTROY" paramer is optional and required only if you are running an update changing multiple rows without a filter. If there is no filter object and no "DESTROY" command the update will fail*/ 
databaseFunctions.updateTableRow("tablename", {oldTableField: "filterValue"}, {tableField: "newValue", "DESTROY"}).then((rowsAffected) => {
    /*this will return the rowsAffected for your confirmation. */
}).catch((err) => {
    /*handle any errors */
});
```

**EXECUTE STORED PROCEDURE**
```
databaseFunctions.executeStoredProcedure("storedProcedureName", {paramName: "paramValue"}).then((results) => {
    /*this will return the results in an array. */
}).catch((err) => {
    /*handle any errors */
});
```

**DELETE TABLE ROW**
```
//The operand is optional and defaults to AND when not set
databaseFunctions.deleteTableRows("tablename", {tableField: "filterValue"}, operand).then((rowsAffected) => {
    /*this will return the rowsAffected for your confirmation */
}).catch((err) => {
    /*handle any errors */
});
```

---

# nginx

This part of core is used in some instances of dev stacks only. It contains the cofiguration files for an nginx container that will allow you to test your development locally using a url over an ssl connection. The url is configured to *webprojects-dev.co.uk* and when you spin up a base development stack you can specify the http and https ports nginx is listening for. 

**In the current nginx configuration all traffic that hits the url through port 80 will be automatically redirected to 443**

You will need to add the following to your machines host file:

* ```127.0.0.1 webprojects-dev.co.uk```

---

# Dev Database

This part of core contains initialisation scripts that will spin up the development database container with an example db, table and data. Its literally not that exiting. Its a sql script, and some csv files along with a linux shell script file. 

---
