import React, {useState, useEffect} from 'react';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonCard, IonCardHeader, IonCardTitle, IonProgressBar, IonCardSubtitle, IonChip, IonLabel, IonItem, IonInput} from '@ionic/react';
import '../../theme/custom-colors.css'
import '../../theme/custom.css'
import './BaseList.css';
import AddGoal from '../add-goal/AddGoal';
import AddBudget from '../add-budget/AddBudget';
import useModal from '../../helpers/useModal';
import Menu from '../menu/Menu';
import { closeOutline, checkmarkOutline, createOutline } from 'ionicons/icons';
import { apiUrl } from "../../helpers/env";

interface item {
	id: String,
	description: String,
	targetAmount: number,
	currentAmount: number,
	category?: String,
	percent: number
}

interface Props {
	title: String,
	data: item[],
	type: String,
	isBudgetList: boolean
}

const List: React.FC<Props> = (props) => {

	const [isFormOpen, toggleForm] = useModal();

	const modalSection = props.isBudgetList ? <AddBudget isOpen={isFormOpen} hide={toggleForm}/> : <AddGoal isOpen={isFormOpen} hide={toggleForm}/>;

	const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'];

	function isMonthActive(month: number) {
		let currentMonth = new Date().getMonth();
		return (month === currentMonth ? "active-chip-dark" : "dark");
	}

	const [showConfirmDelete, setShowConfirmDelete] = useState(false);
	const [indexToDelete, setIndexToDelete] = useState(-1);

	const [showEdit, setShowEdit] = useState(false);
	const [indexToEdit, setIndexToEdit] = useState(-1);

	const [propsData, updateData] = useState(props.data);
	const [tempValue, updateTempValue] = useState(0);

	const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success


	useEffect(() => {
		updateData(props.data);
	}, [props.data])


	// toggle showing delete confirmation div
	const toggleDeleteDiv = (index: number) => {
		setShowConfirmDelete(!showConfirmDelete);
		setIndexToDelete(index);
	}

	// submit delete
	const submitDelete = (index: number) => {
		// call API to delete from database

		let url = props.isBudgetList ? `${apiUrl}/budget/${propsData[index].id}/delete` : `${apiUrl}/goal/goalId/${propsData[index].id}/deleteGoal`;

		setSubmission(1);
		fetch(url, {
			method: 'POST',
			credentials: 'include'
		})
		.then(data => setSubmission(2))
		.catch((error) => setSubmission(-1));

		// if successful then delete from state data
		let newData = [...propsData];
		newData.splice(index, 1);
		updateData(newData);
		// reset index to delete
		setIndexToDelete(-1);
		setShowConfirmDelete(false);
	}

	// edit handler
	const editHandler = (e: any) => {
		if (e.target.value > 0) {
			updateTempValue(parseFloat(e.target.value));
		}  
	}

	// confirm edit
	const confirmEdit = (index: number) => {
		// call API to edit database
		// TO DO
		if (tempValue <= 0) setSubmission(-1)
		else {
			setSubmission(1);

			fetch('https://jsonplaceholder.typicode.com/posts', {
				method: 'POST',
				mode: 'no-cors',
			})
			.then(data => {
				setSubmission(2)
				let newData = [...propsData];
				newData[index].targetAmount = tempValue;
				updateData(newData);
				setShowEdit(false);
				updatePercent(index);
			})
			.catch((error) => setSubmission(-1));
		}
	}

	const messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error</IonLabel>;
        } else if(submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Saved</IonLabel>;
        } else {
            return null;
        }
    }

	// toggle showing delete confirmation div
	const toggleEditDiv = (index: number) => {
		setShowEdit(!showEdit);
		setIndexToEdit(index);
	}

	// function to update percent bar
	const updatePercent = (index: number) => {
		let newData = [...propsData];
		newData[index].percent = (newData[index].currentAmount / newData[index].targetAmount);
		updateData(newData);
	}

	// create list from state data
	const list = propsData.map((item, index) => 
		{	
			let confirmDeleteDiv;
			if (index === indexToDelete) {
				confirmDeleteDiv = (
					<IonItem key={index}>
						<IonLabel className="card-label">Confirm delete</IonLabel>
						<IonButtons slot="end">
							<IonButton onClick={() => toggleDeleteDiv(-1)}>
								<IonIcon  slot="icon-only" icon={closeOutline} className="menu-icon" />
							</IonButton>
							<IonButton onClick={() => submitDelete(index)}>
								<IonIcon slot="icon-only" icon={checkmarkOutline} className="menu-icon" />
							</IonButton>
						</IonButtons>
					</IonItem>
				);	
			} else {
				confirmDeleteDiv = null;
			}
			
			const deleteBtn = (
				<IonButtons slot="end">
					<IonButton onClick={() => toggleDeleteDiv(index)}>
						<IonIcon slot="icon-only" icon={closeOutline} className="menu-icon" />
					</IonButton>
				</IonButtons>
			);

			const editBtn = (
				<IonButtons slot="end">
					<IonButton onClick={() => toggleEditDiv(index)}>
						<IonIcon slot="icon-only" icon={createOutline} className="menu-icon" />
					</IonButton>
				</IonButtons>
			);
			
			let editDiv;
			if (index === indexToEdit) {
				editDiv = (
					<IonItem>
						<IonInput type="number" placeholder="Enter amount" onInput={editHandler}></IonInput>
						<IonButtons slot="end">
							<IonButton className="delete-budget-btn" onClick={() => toggleEditDiv(index)}>
								<IonIcon  slot="icon-only" icon={closeOutline} className="menu-icon" />
							</IonButton>
							<IonButton className="delete-budget-btn" onClick={() => confirmEdit(index)} >
								<IonIcon slot="icon-only" icon={checkmarkOutline} className="menu-icon" />
							</IonButton>
						</IonButtons>
					</IonItem>
				)
			} else {
				editDiv = null;
			}
			
			let message = null;
			if (index === indexToEdit || index === indexToDelete) message = messages()

			return (
				<IonCard>
				<IonCardHeader>
					<IonItem className="ion-no-padding" lines="none">
						<div className="card-top">
							<IonLabel className="card-label">{capitalizeFirstLetter(item.description)}</IonLabel>
							<IonLabel className="card-label" color="gray">{item.category}</IonLabel>
						</div>
						{showConfirmDelete ? null : deleteBtn}
						{showEdit ? null : editBtn}
					</IonItem>
					{showConfirmDelete ? confirmDeleteDiv : null}
					<div className="card-bottom">
						<IonCardTitle>{numberWithCommas(item.targetAmount)}</IonCardTitle>
						{showEdit ? editDiv : null}
						<div className="progress-bar-container">
								<IonProgressBar value={item.currentAmount/item.targetAmount} color="green"></IonProgressBar>
								<IonCardSubtitle>{numberWithCommas(item.currentAmount)}</IonCardSubtitle>
						</div>
					</div>
					{message}
				</IonCardHeader>
			</IonCard>
			);
		}
	)
	
	function capitalizeFirstLetter(string: String){
		return string
		// return string[0].toUpperCase() + string.slice(1).toLowerCase();
	}

	function numberWithCommas(num: Number) {
		// return num.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
		return num
	}

	return (
		<IonContent fullscreen color="light">

			{modalSection}

			<IonHeader collapse="condense">
				<IonToolbar color="light" className="top-bar">
					<IonTitle size="large" className="title">{props.title}</IonTitle>
					<IonButtons slot="end">
						<Menu/>
					</IonButtons>
				</IonToolbar>
			</IonHeader>
			<div className="months-filter">
				{months.map((value, index) => 
					<IonChip className={isMonthActive(index)} key={index}>
						<IonLabel>{value}</IonLabel>
					</IonChip>
				)}
			</div>
			<div className="body-actions">
				<span className="body-action" onClick={toggleForm as any}>+ Add {props.type}</span>
			</div>
			<div className="list">
				{list}
			</div>

			{propsData.length < 1 && <IonLabel>No saved items</IonLabel>}

		</IonContent>
	);
};

export default List;
