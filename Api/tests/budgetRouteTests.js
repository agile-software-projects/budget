const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { totalmem } = require('os');

chai.use(chaiHttp);

const baseUrl = "http://localhost:3000/budget"; //can be localhost
//any other consts (like auth)

const badAccountData = {
    budgetName: "awf2345£$%£",
    totalLimit: 12000,
    accountId: 1,
    active: true,
    resetPeriod: "1m"
}

const goodAccountData = {
    budgetName: "monthlyExpenses",
    totalLimit: 12000,
    accountId: 1,
    active: true,
    resetPeriod: "1m"
}
describe.skip("Testing /:accountid/new", () => {
    it.skip("Return 400 if account does not exist", (done) => {
        chai.request(baseUrl)
            .post('/1/new') //can be post etc
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "account doesn't exist");
            done();
        });
    });

    it.skip("return 400 if new account data doesn't validate", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/new')
            .send(badAccountData)
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "The budget name is not valid");
            done();
        });
    });

    it("return 200 if all ok", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/new')
            .send(goodAccountData)
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    });
});

describe.skip("Testing /:budgetid/update", () => {
    it.skip("Return 400 if budget does not exist", (done) => {
        chai.request(baseUrl)
            .post('/1/update') //can be post etc
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "Budget not found");
            done();
        });
    });

    it.skip("return 400 if budget data doesn't validate", (done) => {
        chai.request(baseUrl)
            .post('/4d88431a-c494-4d6f-9633-151b07dbe499/update')
            .send(badAccountData)
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "The budget name is not valid");
            done();
        });
    });

    it.skip("return 200 if all ok", (done) => {
        chai.request(baseUrl)
            .post('/4d88431a-c494-4d6f-9633-151b07dbe499/update')
            .send(goodAccountData)
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    });
});

describe.skip("Testing /:budgetid/delete", () => {
    it("Return 400 if budget does not exist", (done) => {
        chai.request(baseUrl)
            .post('/1/delete') //can be post etc
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "Budget not found");
            done();
        });
    });

    it("return 200 if all ok", (done) => {
        chai.request(baseUrl)
            .post('/4d88431a-c494-4d6f-9633-151b07dbe499/delete')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    });
});

describe.skip("Testing /:budgetid/get", () => {
    it("Return 400 if budget does not exist", (done) => {
        chai.request(baseUrl)
            .get('/1/get') //can be post etc
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "Budget not found");
            done();
        });
    });

    it("return 200 if all ok", (done) => {
        chai.request(baseUrl)
            .get('/4fa4a4f0-9c0f-4f82-8b94-40a1aadeaaf8/get')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });
});

describe.skip("Testing /:budgetid/categores/getAll", () => {
    it.skip("Return 400 if budget does not exist", (done) => {
        chai.request(baseUrl)
            .get('/1/categories/getAll') //can be post etc
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "Budget not found");
            done();
        });
    });

    it.skip("return 400 if budget has no categories", (done) => {
        chai.request(baseUrl)
            .get('/4fa4a4f0-9c0f-4f82-8b94-40a1aadeaaf8/categories/getAll')
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "No Categories found");
            done();
        });
    });

    it.skip("return 200 if all ok", (done) => {
        chai.request(baseUrl)
            .get('/245784f3-3fb5-47f6-8263-f6ce7933bd13/categories/getAll')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });
});