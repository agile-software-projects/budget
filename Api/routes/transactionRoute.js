const express = require('express');
const router = express.Router();
const winston = require('../core/winston');
const env = require('../scripts/env');
const User = require('../classes/User');
const Transaction = require('../classes/Transaction');
const Account = require('../classes/Account');

router.all('*', (req, res, next) => {
    if(env.devDatabase.enabled) {
        next();
    } else {
        res.status(500).json({error: "No database detected"});
    }
});

/*
*/

//Must have an end to create new transactions
router.post("/:accountid/:category/new", (req, res) => {
    if (req.session && req.session.user) {
        req.body.userId = req.session.user.userId;
    } else {
        res.status(401);
        return res.json({error: "you are not authenticated to post a transaction"})
    }
    const transaction = new Transaction(req.body);
    transaction.accountId = req.params.accountid;
    try {
        transaction.validate().verifyAccount().then((account) => {
            //need to get the category and link it to the transaction. 
            const category = req.params.category;
            transaction.categoryName = category;
            //get the categories assosciated (through budgets) to the account. 
            transaction.getAccountCategories().then((categories) => {
                const filteredCategories = categories.filter(cat => cat.categoryName === category);
                console.debug({filteredCategories});
                if (filteredCategories.length > 0) {
                    const insertPromises = [];
                    insertPromises.push(transaction.insertTransaction())
                    insertPromises.push(transaction.insertCategoryLink(filteredCategories[0].categoryId));
                    Promise.all(insertPromises).then(() => {
                        res.status(200);
                        return res.json({success: true});
                    }).catch((err) => {
                        winston.error(err);
                        res.status(400);
                        return res.json(err);
                    })
                } else {
                    res.status(400);
                    return res.json({error: "No matching categories found"});
                }
            }).catch((err) => {
                winston.error(err);
                res.status(400);
                return res.json(err);
            });
        }).catch((err) => {
            winston.error(err);
            res.status(400);
            return res.json(err)
        })
    } catch (err) {
        winston.error(JSON.stringify(err));
        res.status(500);
        return res.json(err);
    }
});

//Must have an endpoint to edit existing transactions
router.post("/:transactionid/edit", (req, res) => {
    const transaction = new Transaction({transactionId: req.sanitize(req.params.transactionid)})
    transaction.populate().then(() => {
        if (Object.keys(req.body).length > 0) {
            if (req.body.transactionDescription) { transaction.transactionDescription = req.sanitize(req.body.transactionDescription) }
            if (req.body.transactionDate) { transaction.transactionDate = req.sanitize(req.body.transactionDate) }
            if (req.body.transactionDirection) { transaction.transactionDirection = req.sanitize(req.body.transactionDirection) }
            if (req.body.amount) { transaction.amount = Number.parseInt(req.sanitize(req.body.amount)) }
            let categoryId = "";
            if (req.body.categoryId) { categoryId = req.body.categoryId; } //not easy to sanitize as could be either string or int. 
            try {
                transaction.validate().update().then(() => {
                    transaction.updateCategoryLink(categoryId).then(() => {
                        res.status(200);
                        return res.json({success: true});
                    }).catch((err) => {
                        winston.error(err);
                        res.status(400);
                        return res.json(err);
                    });
                }).catch((err) => {
                    winston.error(err);
                    res.status(400);
                    return res.json(err);
                });
            } catch(err) {
                winston.error(JSON.stringify(err));
                res.status(500);
                return res.json(err);
            }
        } else {
            winston.error("no body detected");
            res.status(400);
            return res.json({error: "no body detected"})
        }
    }).catch((err) => {
        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);
    }) 
});

//Must have an endpoint for deleting a transaction
router.post("/:transactionid/delete", (req, res) => {
    const transaction = new Transaction({transactionId: req.sanitize(req.params.transactionid)});
    //check we're deleting an actual transaction
    transaction.populate().then(() => {
        transaction.deleteTransaction().then(() => {
            res.status(200);
            return res.json({success: true});
        }).catch((err) => {
            winston.error(err);
            res.status(500);
            return res.json(err);
        })
    }).catch((err) => {
        winston.error(err);
        res.status(400);
        return res.json(err);
    });
});

//Must have an endpoint for deleting all of an accounts transactions
router.post("/:accountid/deleteAll", (req, res) => {
    const account = new Account({accountId: req.sanitize(req.params.accountid)});
    //just check its a legit account
    account.populate().then(() => {
        account.getAllAccountTransactions().then((transactions) => {
            const deletePromises = [];
            transactions.forEach((trans) => {
                const transaction = new Transaction({transactionId: trans.transactionId});
                //we just got these from the db so we know they're legit. just straight up delete them
                deletePromises.push(transaction.deleteTransaction());
            });

            Promise.all(deletePromises).then(() => {
                res.status(200);
                return res.json({success: true});
            }).catch((err) => {
                winston.error(JSON.stringify(err));
                res.status(500);
                return res.json(err);
            });
        }).catch((err => {
            winston.error(JSON.stringify(err));
            res.status(400);
            res.json(err);
        }));
    }).catch((err) => {
        winston.error(JSON.stringify(err));
        res.status(400);
        res.json(err);
    });
});

//Must have an endpoint for retrieving all transactions for a specific account
router.get("/:accountid/accountTransaction/getAll", (req, res) => {
    const account = new Account({accountId: req.sanitize(req.params.accountid)});
    //check its legit
    account.populate().then(() => {
        account.getAllAccountTransactions().then((transactions) => {
            res.status(200);
            return res.json(transactions);
        }).catch((err) => {
            winston.error(err);
            res.status(400);
            return res.json(err);
        });
    }).catch((err) => {
        winston.error(err);
        res.status(400);
        return res.json(err);
    });
});

//Must have an endpoint for retrieving all transactions for a specific user
router.get("/userTransaction/getAll", (req, res) => {
    const user = new User({userId: req.session.user.userId});
    user.populate().then(() => {
        //get all the user accounts
        user.getAllAccounts().then((accounts) => {
            const transactionsPromises = [];
            accounts.forEach((acc) => { 
                const account = new Account({accountId: acc.accountId});
                transactionsPromises.push(account.getAllAccountTransactions());
            });
            Promise.allSettled(transactionsPromises).then((transactionSets) => {
                const transactions = [];
                transactionSets.forEach((set) => {
                    if (set.value) {
                        set.value.forEach((trans) => {
                            transactions.push(trans);
                        });
                    }
                });
                res.status(200);
                return res.json(transactions);
            }).catch((err) => {
                winston.error(err);
                res.status(400);
                return res.json(err);
            });
        }).catch((err) => {
            winston.error(err);
            res.status(400);
            return res.json(err);
        });
    }).catch((err) => {
        winston.error(err);
        res.status(400);
        return res.json(err);
    });
});

//Endpoint to return a single transaction
router. get("/:transactionid/get", (req, res) => {
    const transaction = new Transaction({transactionId: req.sanitize(req.params.transactionid)});
    transaction.populate().then(() => {
        res.status(200);
        return res.json({transaction: transaction.wholeTransaction()});
    }).catch((err) => {
        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);
    });
});

//Must have an endpoint to convert some expenses into recurring expenses
router.post("/:transactionid/setRecurring", (req, res) => {
    const transaction = new Transaction({transactionId: req.sanitize(req.params.transactionid)});
    transaction.populate().then(() => {
        transaction.recurring = req.sanitize(req.body.recurring===true);
        transaction.recurring = transaction.recurring==="true"?true:false;
        transaction.recurrencePeriod = req.sanitize(req.body.recurrencePeriod);
        try {
            transaction.validate().update().then(() => {
                res.status(200);
                return res.json({success: true});
            }).catch((err) => {
                winston.error(err)
                res.status(400);
                return res.json(err);
            })
        } catch(err) {
            winston.error(err)
            res.status(500);
            return res.json({error: err});
        }
    }).catch((err) => {
        winston.error(err)
        res.status(400);
        return res.json(err);
    });
});

//Must have an endpoint to unmark a transaction as a recurring expense
//Mildly modified. This will set the recurring flag to !recurring. SO its a toggle
router.post("/:transactionid/recurring/toggle", (req, res) => {
    const transaction = new Transaction({transactionId: req.sanitize(req.params.transactionid)});
    transaction.populate().then(() => {
        transaction.recurring = transaction.recurring===1?false:true;
        transaction.update().then(() => {
            res.status(200);
            return res.json({success: true});
        }).catch((err) => {
            winston.error(err)
            res.status(500);
            return res.json(err);
        });
    }).catch((err) => {
        winston.error(err)
        res.status(400);
        return res.json(err);
    });
});

router.get("/:transactionid/getTransactionCategory", (req, res) => {
    const transaction = new Transaction({transactionId: req.sanitize(req.params.transactionid)});
    transaction.populate().then(() => {
        transaction.getTransactionCategory().then((category) => {
            res.status(200);
            return res.json(category);
        }).catch((err) => {
            res.status(500);
            return res.json({error: err});
        })
    }).catch((err) => {
        winston.error(err)
        res.status(400);
        return res.json(err);
    });
});

module.exports = router;