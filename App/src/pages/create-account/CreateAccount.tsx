import React, {useState, useEffect} from 'react';
import { IonModal, IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonItem, IonInput, IonList, IonSelect, IonSelectOption, IonLabel, IonProgressBar} from '@ionic/react';
import { closeOutline } from 'ionicons/icons';
import './CreateAccount.css';
import { useStore } from '../../store/store-actions';

const CreateAccount = (props: any) => {
    const { isOpen, hide } = props
    const {addAccount} = useStore()

    // set initial state for the form data
    const [formData, setValues] = useState({
        accountName: "",
        accountType: "",
        currentBalance: 0
    });

    // errors state
    interface IErrors {
        accountName?: string,
        accountType?: string,
        currentBalance?: string
    }

    const [errors, setErrors] = useState<IErrors>({});

    // submitted state
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success

    // update form input handler
    const handleChange = (e: any) => {
        setValues({
            ...formData,
            [e.target.name]: e.target.value
        });
    }

    const handlePriceChange = (e: any) => {
        let price = e.target.value;
        if (price) {
            price = Math.round(price*100)/100;
        }
        setValues({
            ...formData,
            currentBalance: price
        }) 
    }

    // hide component need to reset the state
    const hidePage = () => {
        setValues({ 
            accountName: "",
            accountType: "",
            currentBalance: 0.00
        });
        hide(false);
    }

    // Validation
    const validateInputs = () => {

        let errors: any = {};

        // account name
        if (!formData.accountName) {
            errors.accountName = "Account name required";
        } else if (!/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/.test(formData.accountName)) {
            errors.accountName = "Only letters and numbers allowed";
        } else if (formData.accountName.length > 32) {
            errors.accountName = "Account name too long. Maximum 32 characters."
        }
        // accountType
        if (!formData.accountType) {
            errors.accountType = "Account Type required";
        } else if (formData.accountType !== "bank_account" && formData.accountType !== "credit_card") {
            errors.accountType = "Currency must be Bank Account or Credit Card";
        }
        // starting balance
        if (formData.currentBalance === null) { //becuase zero might equate to false
            errors.currentBalance = "Starting balance required";
        }

        return errors;
    }

    // Submit Form Handler
    const submitForm = (e: any) => {
        e.preventDefault();
        setErrors(validateInputs());
        setIsSubmitted(true);
    }

    useEffect(() => {
        if (isSubmitted && Object.keys(errors).length === 0) {
            setSubmission(1)
            try{
                addAccount(formData as any)
                setSubmission(2)
                hidePage();
            }catch(e){
                console.error(e)
                setSubmission(-1);
            }
        } else if (Object.keys(errors).length !== 0) {
            setSubmission(-1);
        }
    },
        [isSubmitted, errors]
    );

    const messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error</IonLabel>;
        } else if(submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Saved</IonLabel>;
        } else {
            return null;
        }
    }

	return (
		<IonModal isOpen={isOpen} >
            <IonContent fullscreen>
				<IonHeader className="modal-header">
					<IonToolbar className="top-bar">
						<IonTitle>Create Account</IonTitle>
						<IonButtons slot="end">
							<IonButton onClick={hidePage} className="btn-menu">
								<IonIcon slot="icon-only" icon={closeOutline} className="menu-icon"/>
							</IonButton>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
                <div className="modal-content">
                    <IonList>
                        <IonItem className="ion-no-padding ion-margin">
                            <IonLabel position="floating">Account Name</IonLabel>
                            <IonInput 
                                type="text" 
                                name="accountName" 
                                className="form-input"
                                value={formData.accountName}
                                onIonChange={handleChange}>
                            </IonInput>
                        </IonItem>
                        <IonLabel color="danger" className="ion-padding-start">{errors.accountName}</IonLabel>
                        
                        <IonItem className="ion-no-padding ion-margin">
                            <IonLabel position="floating">Account Type</IonLabel>
                            <IonSelect 
                                        name="accountType"
                                        className="form-input"
                                        value={formData.accountType} 
                                        onIonChange={handleChange}>
                                <IonSelectOption value="bank_account">Bank Account</IonSelectOption>
                                <IonSelectOption value="credit_card">Credit Card</IonSelectOption>
                            </IonSelect>
                        </IonItem>
                        <IonLabel color="danger" className="ion-padding-start">{errors.accountType}</IonLabel>

                        <IonItem className="ion-no-padding ion-margin">
                            <IonLabel position="floating">Starting Balance</IonLabel>
                            <IonInput 
                                type="number" 
                                step="0.01"
                                name="currentBalance"
                                className="form-input"
                                value={formData.currentBalance}
                                onIonChange={handlePriceChange}>
                            </IonInput>
                        </IonItem>
                        <IonLabel color="danger" className="ion-padding-start">{errors.currentBalance}</IonLabel>
                    </IonList>
                    <IonButton expand="block" className="ion-margin" onClick={submitForm}>Create Account</IonButton>
                    {messages()}
                </div>
            </IonContent>
        </IonModal>
	);
};

export default CreateAccount;
