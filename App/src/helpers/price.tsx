export const priceFormat = (amount: number): number => {
    const val = amount
    return val/100
}

export const convertPriceFormat = (amount: any): number => {
    const val = amount
    return parseInt((val * 100).toFixed(2))
}