const assert = require('assert');
const Account = require("../classes/Account");

const goodData = {
    accountId: "00000000-0000-0000-0000-000000000000",
    userId: "d30cc1c0-7932-4f38-8421-ea56cd3220eb",
    accountType: "current",
    accountName: "main account",
    creationDate: "2021-01-27 23:07:51.523",
    accountEnabled: true,
    currentBalance: 166765,
    lastUpdated: "2021-01-27 23:07:51.523"
}

const badDataUuid = {
    accountId: "00000000-0000",
    userId: "d30cc1c0-7932-4f38-8421-ea56cd3220eb",
    accountType: "current",
    accountName: "main account",
    creationDate: "2021-01-27 23:07:51.523",
    accountEnabled: true,
    currentBalance: 166765,
    lastUpdated: "2021-01-27 23:07:51.523"
}

const badDataDates = {
    accountId: "00000000-0000-0000-0000-000000000000",
    userId: "d30cc1c0-7932-4f38-8421-ea56cd3220eb",
    accountType: "current",
    accountName: "main account",
    creationDate: "2021-waefasdfaeaweda.523",
    accountEnabled: true,
    currentBalance: 166765,
    lastUpdated: "2021-01-27 23:07:51.523"
}

const badDataBool = {
    accountId: "00000000-0000-0000-0000-000000000000",
    userId: "d30cc1c0-7932-4f38-8421-ea56cd3220eb",
    accountType: "current",
    accountName: "main account",
    creationDate: "2021-01-27 23:07:51.523",
    accountEnabled: "awdf",
    currentBalance: 166765,
    lastUpdated: "2021-01-27 23:07:51.523"
}

const badDataType = {
    accountId: "00000000-0000-0000-0000-000000000000",
    userId: "d30cc1c0-7932-4f38-8421-ea56cd3220eb",
    accountType: "cuedawerrent;@",
    accountName: "main account",
    creationDate: "2021-01-27 23:07:51.523",
    accountEnabled: true,
    currentBalance: 166765,
    lastUpdated: "2021-01-27 23:07:51.523"
}

const badDataName = {
    accountId: "00000000-0000-0000-0000-000000000000",
    userId: "d30cc1c0-7932-4f38-8421-ea56cd3220eb",
    accountType: "current",
    accountName: "main£%!$ account;",
    creationDate: "2021-01-27 23:07:51.523",
    accountEnabled: true,
    currentBalance: 166765,
    lastUpdated: "2021-01-27 23:07:51.523"
}

describe("Test the Account Class", () => {
    it.skip("Test the class returns a whole Object", (done) => {
        const account = new Account(goodData);
        assert.strictEqual(Object.keys(account.wholeAccount()).length, 8);
        done();
    });

    it.skip("assignAccountId should assign an accountId", (done) => {
        const account = new Account(goodData);
        account.accountId = "";
        account.assignAccountId();
        assert.strictEqual(account.accountId.length, 36);
        done();
    });

    it("Pass the validation", (done) => {
        const account = new Account(goodData)
        assert.strictEqual(account.validate(), account);
        done();
    });

    it("fail uuid", (done) => {
        const badUuid = new Account(badDataUuid);
        assert.strictEqual(badUuid.validate(), false);
        done();
    });

    it("fail type", (done) => {
        const badType = new Account(badDataType);
        assert.strictEqual(badType.validate(), false);
        done();
    });

    it("fail name", (done) => {
        const badName = new Account(badDataName);
        assert.strictEqual(badName.validate(), false);
        done();
    });

    it("fail date", (done) => {
        const badDate = new Account(badDataDates);
        assert.strictEqual(badDate.validate(), false);
        done();
    });

    it("fail bool", (done) => {
        const badBool = new Account(badDataBool);
        assert.strictEqual(badBool.validate(), false);
        done();
    });

    it.skip("should verify the user exists", (done) => {
        const account = new Account(goodData);

        setTimeout(() => {
            account.verifyUser().then((user) => {
                assert.strictEqual(user.email, "test@test.com");
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });

    it.skip("should insert the account record", (done) => {
        const account = new Account(goodData);

        setTimeout(() => {
            account.validate().insertAccount().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("should retrieve a record", (done) => {
        const account = new Account({accountId: goodData.accountId})

        setTimeout(() => {
            account.populate().then((acc) => {
                assert.strictEqual(acc.accountType, "current");
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });

    it.skip("should update a record", (done) => {
        const account = new Account(goodData);

        setTimeout(() => {
            account.validate().update().then((success) => {
                assert.strictEqual((success.success), true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });
});
