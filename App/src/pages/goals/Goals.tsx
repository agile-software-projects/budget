import React, {useState} from 'react';
import { IonAlert, IonModal, IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonCard, IonCardHeader, IonCardTitle, IonProgressBar, IonCardSubtitle, IonChip, IonLabel, IonItem, IonPage} from '@ionic/react';
import './Goals.css';
import '../../theme/custom-colors.css'
import '../../theme/custom.css'
import { useStore } from '../../store/store-actions';
import AddGoal from '../add-goal/AddGoal';
import EditGoal from '../edit-goal/EditGoal';
import useModal from '../../helpers/useModal';
import Menu from '../menu/Menu';
import { createOutline, trashOutline } from 'ionicons/icons';
import { priceFormat } from '../../helpers/price';


const Goals: React.FC = () => {

	const {goals, deleteGoal, currency, selectGoal, selectedGoal} = useStore();

	const [isFormOpen, toggleForm] = useModal();

	const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'];

	function isMonthActive(month: number) {
		let currentMonth = new Date().getMonth();
		return (month === currentMonth ? "active-chip-dark" : "dark");
	}

	const [isEditGoalOpen, setIsEditGoalOpen] = useState(false);
	const [showDeleteAlert, setShowDeleteAlert] = useState(false);

	const handleOpenEditGoal = (goal: any) => {

		const data: any = {
			goalId: goal.goalId,
			goalName: goal.goalName,
			goalAmount: goal.goalAmount,
			active: goal.active,
			targetDate: goal.targetDate,
			currentAmount: goal.currentAmount,
			accountId: goal.accountId
		}

		selectGoal(data);
		setIsEditGoalOpen(true);
		
	}

	const handleConfirmDelete = (goal: any) => {
		selectGoal(goal);
		setShowDeleteAlert(true);
	}

	const handleDeleteSelectedGoal = () => {
		try{
			deleteGoal(selectedGoal);
		}catch(e){
			console.error(e)
		}		
	}
	
		// create list from state data
		const list = goals.map((item, index) => 
		{	
			return (
				<IonCard key={index}>
					<IonCardHeader>
						<IonItem className="ion-no-padding" lines="none">
							<div className="card-top">
								<IonLabel className="card-label">{capitalizeFirstLetter(item.goalName)}</IonLabel>
								<IonButton className="trashIcon" fill="clear" onClick={() => {handleConfirmDelete(item)}}>
                                    <IonIcon slot="icon-only" icon={trashOutline} color="dark" className="trashBackground"/>
                                </IonButton>
								<IonButton className="editIcon" fill="clear" onClick={() => handleOpenEditGoal(item)}>
                                    <IonIcon slot="icon-only" icon={createOutline} color="dark"  className="trashBackground"/>
                                </IonButton>
							</div>
						</IonItem>
						<div className="card-bottom">
							<IonCardTitle>{currency}{priceFormat(item.currentAmount)}</IonCardTitle>
							<div className="progress-bar-container">
									<IonProgressBar value={item.goalAmount>0?Number((item.currentAmount/item.goalAmount).toFixed(2)):0} color="green"></IonProgressBar>
									<IonCardSubtitle>{currency}{priceFormat(item.goalAmount)}</IonCardSubtitle>
							</div>
						</div>
					</IonCardHeader>
				</IonCard>
			);
		}
	)
	
	function capitalizeFirstLetter(string: String){
		return string
	}

	return (
		<IonPage className="goals">
			<IonContent fullscreen color="light">

				<AddGoal isOpen={isFormOpen} hide={toggleForm}/>

				<IonModal isOpen={isEditGoalOpen}>
                    <EditGoal
                        hide={setIsEditGoalOpen}
                    />
                </IonModal>

				<IonHeader className="header">
					<IonToolbar color="light" className="top-bar">
						<IonTitle size="large" className="title">Saving Goals</IonTitle>
						<IonButtons slot="end">
							<Menu/>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
				<div className="months-filter">
					{months.map((value, index) => 
						<IonChip className={isMonthActive(index)} key={index}>
							<IonLabel>{value}</IonLabel>
						</IonChip>
					)}
				</div>
				<div className="body-actions">
					<span className="body-action" onClick={toggleForm as any}>+ Add Saving Goal</span>
				</div>
				<div className="list">
					{list}
				</div>
				<IonAlert
					isOpen={showDeleteAlert}
					onDidDismiss={() => setShowDeleteAlert(false)}
					cssClass='my-custom-class'
					header={'Delete Saving Goal?'}
					buttons={[
						{
							text: 'Cancel',
							role: 'cancel',
							cssClass: 'secondary',
							handler: () => {
								console.log('Confirm Cancel');
							}
						},
						{
							text: 'Delete',
							cssClass: 'warning',
							handler: () => {
								handleDeleteSelectedGoal();
							}
						}
					]}
				/>

				{goals.length < 1 && <IonLabel>No saved items</IonLabel>}

			</IonContent>
		</IonPage>
	);
};

export default Goals;
