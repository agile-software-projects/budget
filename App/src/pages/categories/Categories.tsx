import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButtons, IonIcon, IonGrid, IonRow, IonCol } from '@ionic/react';
import { pricetagOutline } from 'ionicons/icons';
import './Categories.css';
import Menu from '../menu/Menu';
import { useStore } from '../../store/store-actions';

const Categories: React.FC = () => {
	const {categories} = useStore()

	return (
		<IonPage className="home">
			<IonContent fullscreen color="light">
				<IonHeader className="header">
					<IonToolbar color="light" className="top-bar">
						<IonTitle size="large" className="title">Categories</IonTitle>
						<IonButtons slot="end">
							<Menu/>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
				{/* <div className="body-actions">
					<span className="body-action">+ Add category</span>
				</div> */}
				<IonGrid class="categories-list">
					<IonRow>
						{categories.map((cat, i) => 
						<IonCol size="6" size-sm key={i}>
							<div className="category-box">
								<div className="category-icon"><IonIcon icon={pricetagOutline}/></div>
								<div className="category-name">{cat.categoryName}</div>
							</div>
						</IonCol>)}
					</IonRow>
				</IonGrid>
			</IonContent>
		</IonPage>
	);
};

export default Categories;
