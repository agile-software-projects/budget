import React, {useState, useEffect, useContext} from 'react';
import { IonButton, IonItem, IonLabel, IonInput, IonDatetime, IonGrid, IonProgressBar, IonSelect, IonSelectOption} from '@ionic/react';
import CustomSelect from '../../ui-compontent/custom-select/CustomSelect';
import './AddForm.css';
import validate from './validateGoalBudgetForm';
import { useStore } from '../../store/store-actions';

interface FormProps {
    isOpen: any,
    hide: any,
    isBudgetForm: boolean
}

const AddForm: React.FC<FormProps> = (props) => {

    interface IErrors {
        description?: string,
        targetAmount?: string,
        targetDate?: string,
        currentAmount?: string
    }

    const [values, setValues] = useState({
        description: "",
        targetAmount: "",
        targetDate: "",
        active: 1,
        currentAmount: "",
        accountId: ""
    })

    const [errors, setErrors] = useState<IErrors>({});

    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success
    const {addGoal, accounts, addBudget} = useStore()

    const handleChange = (e: any) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e: any) => {
        e.preventDefault();
        setErrors(validate(values, props.isBudgetForm));
        setIsSubmitted(true);

    }

    useEffect(() => {
        if (isSubmitted && Object.keys(errors).length === 0) {

            setSubmission(1);

            const route = props.isBudgetForm ? 'budget' : 'goal'
            if(route === 'goal'){
                addGoal(values as any)
            }else if(route === 'budget'){
                addBudget(values as any)
            }
            
            setSubmission(2);
            props.hide()
            
        } else if (Object.keys(errors).length !== 0) {
            setSubmission(-1);
        }
    },
        [errors, isSubmitted, props.isBudgetForm, values.accountId, values.active, values.currentAmount, values.description, values.targetAmount, values.targetDate]
    );

    const messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error</IonLabel>;
        } else if(submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Saved</IonLabel>;
        } else {
            return null;
        }
    }

	return (
        <form className="goal-budget-form" onSubmit={handleSubmit}>
            <IonGrid>
                <IonItem className="ion-no-padding ion-margin">
                    <IonInput 
                        type="text" 
                        name="description"
                        className="form-input"
                        placeholder="Description"
                        value={values.description}
                        onIonChange={handleChange}
                        />
                </IonItem>
                <IonLabel color="danger" className="ion-padding-start">{errors.description}</IonLabel>
                <IonItem className="ion-no-padding ion-margin">
                    <IonInput 
                        type="number"
                        step=".01"
                        name="targetAmount"
                        className="form-input"
                        placeholder="Target Amount"
                        value={values.targetAmount}
                        onIonChange={handleChange}
                        />
                </IonItem>
                <IonLabel color="danger" className="ion-padding-start">{errors.targetAmount}</IonLabel>
                <IonItem className="ion-no-padding ion-margin">
                    <IonDatetime 
                        displayFormat="DD-MMM-YYYY" 
                        placeholder="Target Date" 
                        className="form-input"
                        name="targetDate"
                        value={values.targetDate}
                        onIonChange={handleChange}
                        >
                    </IonDatetime> 
                </IonItem>
                <IonLabel color="danger" className="ion-padding-start">{errors.targetDate}</IonLabel>

                <IonItem className="ion-no-padding ion-margin">
                    <IonInput 
                        type="number"
                        step=".01"
                        name="currentAmount"
                        className="form-input"
                        placeholder="Current Amount"
                        value={values.currentAmount}
                        onIonChange={handleChange}
                        />
                </IonItem>
                <IonLabel color="danger" className="ion-padding-start">{errors.currentAmount}</IonLabel>

                <IonItem className="ion-no-padding ion-margin">
                    <IonSelect onIonChange={handleChange} name="accountId" placeholder="Select an account" value={values.accountId}>
                        {accounts.map((acc:any, i) => (
                            <IonSelectOption value={acc.accountId} key={i}>
                                {acc.accountName}
                            </IonSelectOption>
                        ))}
                    </IonSelect>
                </IonItem>

                <IonButton expand="block" type="submit" color="primary" id="btn-save" className="ion-margin">Save</IonButton>
                {messages()}
            </IonGrid>
        </form>
	);
};

export default AddForm;
