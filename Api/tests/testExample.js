const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const baseUrl = "https://..."; //can be localhost
//any other consts (like auth)

describe("This is an example unit test", () => {
    it("Example test case", (done) => {
        chai.request(baseUrl)
            .get('/endpoint') //can be post etc
            .set('key', 'value') //headers
            .send('body') //body
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });
});
