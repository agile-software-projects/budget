module.exports.mssqlConnect = mssqlConnect;
module.exports.selectTable = selectTable;
module.exports.insertTableRow = insertTableRow;
module.exports.updateTableRow = updateTableRow;
module.exports.executeStoredProcedure = executeStoredProcedure;
module.exports.deleteTableRows = deleteTableRows;

/*----------------------------------------------------------------------------------------------------*/

const asql = require('mssql');
const dbConnect = require('../scripts/db');
const winston = require('./winston');
const { v4: uuidv4 } = require('uuid');

var pool;
mssqlConnect().then((con) => {
    winston.info(`inside then of mssqlConnect`);
    pool = con;
});

/*----------------------------------------------------------------------------------------------------*/

function mssqlConnect() {
    winston.debug(`inside function mssqlConnect`)
    return dbConnect.pool();
}

function getFieldType(tableName,fieldName,data,inputAppend){
    if(!inputAppend){ var inputAppend=""}
    return new Promise (function (resolve,reject){
        var param = {}
        param.fieldName = inputAppend + fieldName
        param.data = data
        var sql = "SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME= @tableName AND COLUMN_NAME=@fieldName"
        pool.request()
            .input ('tableName', asql.NVarChar, tableName)
            .input ('fieldName', asql.NVarChar, fieldName)
            .query(sql)
        .then(function (results){
            if (results.recordset.length==1){
                switch (results.recordset[0].DATA_TYPE) {
                    case "nvarchar":
                        param.fieldType = asql.NVarChar(results.recordset[0].CHARACTER_MAXIMUM_LENGTH)
                        resolve(param)
                        break;
                    case "tinyint":
                        param.fieldType = asql.TinyInt
                        resolve(param)
                        break;
                    case "decimal":
                        param.fieldType = asql.Decimal(12, 2);
                        resolve(param);
                        break;
                    case "int":
                        param.fieldType = asql.Int;
                        resolve(param);
                        break;
                    case "datetime":
                        param.fieldType = asql.DateTime();
                        resolve(param);
                        break;
                    default:
                        reject("Undefined Field Type");
                }
            } else{
                reject("FIELD NOT FOUND: " + fieldName )
            }
        });
    });
}

function selectTable(tableName,filterObject,operand){
    return new Promise (function (resolve,reject){
        var filter = ""
        if(!operand){var operand=" AND "}
    //      thisreq = pool.request()
        var params = []
        var fieldTypePromise = []
        for (var i=0;i<Object.keys(filterObject).length;i++){
                if (i==0){ filter += " WHERE " }else{ filter+=operand}
                var fieldName = Object.keys(filterObject)[i]
                filter += fieldName
                filter += "=@" + fieldName
                fieldTypePromise.push(getFieldType(tableName,fieldName,filterObject[fieldName]));
        }

        var sql ="SELECT * FROM " + tableName + filter
        Promise.all (fieldTypePromise).then (function (params){
            thisReq = pool.request()
            params.forEach(function (param){
                thisReq.input (param.fieldName,param.fieldType,param.data)
            })
            thisReq.query(sql).then(function (results){
                resolve(results.recordset)
            }).catch(function (err){
                console.log(err)
                reject(err)
            })
        }).catch (function (err){
            console.log(err)
            reject(err)
        })
    })
}


function insertTableRow(tableName,tableObject){
    return new Promise (function (resolve,reject){
        var databaseFields = "("
        var inputFields = "("
    //      thisreq = pool.request()
        var params = []
        var fieldTypePromise = []
        for (var i=0;i<Object.keys(tableObject).length;i++){
            var fieldName = Object.keys(tableObject)[i]
            databaseFields += fieldName
            inputFields += "@" + fieldName
            if (i+1 != Object.keys(tableObject).length ){
                databaseFields += ","
                inputFields += ","
            }
            fieldTypePromise.push(getFieldType(tableName,fieldName,tableObject[fieldName]))
        }

        inputFields += ")"
        databaseFields +=")"
        var sql ="INSERT INTO " + tableName + " " + databaseFields + " VALUES " + inputFields

        Promise.all (fieldTypePromise).then (function (params){
            thisReq = pool.request()
            params.forEach(function (param){
                thisReq.input (param.fieldName,param.fieldType,param.data)
            })
            thisReq.query(sql).then(function (results){
                resolve(results.rowsAffected)
            }).catch(function (err){
                console.log(err)
                reject(err)
            })
        }).catch (function (err){
            console.log(err)
            reject(err)
        });
    });
}


function updateTableRow(tableName,oldTableObject,tableObject,destroy){
    return new Promise (function (resolve,reject){
        var databaseFields = ""
    //      thisreq = pool.request()
        var params = []
        var fieldTypePromise = []
        var whereClause = " WHERE 1=2 "
        var userDestroy = (destroy=="DESTROY") || false
        if (Object.keys(oldTableObject).length>0) {
            for (var j=0;j<Object.keys(oldTableObject).length;j++){
                var queryField = Object.keys(oldTableObject)[j]
                if (j==0) {whereClause = " WHERE "}
                whereClause += queryField + "= @old_" + queryField + ""
                fieldTypePromise.push (getFieldType(tableName,queryField,oldTableObject[queryField],'old_'))
                if (j+1 != Object.keys(oldTableObject).length){whereClause +=' AND '}
            }
        }else{
            if (userDestroy==true){
                whereClause = ""
            }
        }

        for (var i=0;i<Object.keys(tableObject).length;i++){
                var fieldName = Object.keys(tableObject)[i]
                databaseFields += fieldName + " = " + "@" + fieldName
                if (i+1 != Object.keys(tableObject).length ){
                    databaseFields += ","
                }
            fieldTypePromise.push (getFieldType(tableName,fieldName,tableObject[fieldName]))
        }

        var sql ="UPDATE " + tableName + " SET " + databaseFields  + whereClause
        Promise.all (fieldTypePromise).then (function (params){
            thisReq = pool.request()
            params.forEach(function (param){
                thisReq.input (param.fieldName,param.fieldType,param.data)
            })
            thisReq.query(sql).then(function (results){
                resolve(results.rowsAffected.length)
            }).catch(function (err){
                console.log(err)
                reject(err)
            })
        }).catch (function (err){
            console.log(err)
            reject(err)
        })
    })
}

function executeStoredProcedure(procedureName, params) {
    return new Promise((resolve, reject) => {
        var sql = `EXEC ${procedureName} `;
        console.log(Object.keys(params).length);
        for(var i = 0; i < Object.keys(params).length; i++) {
            sql += `@${Object.keys(params)[i]} = '${params[Object.keys(params)[i]]}'`;
            if (i+1 < Object.keys(params).length) {
                sql += ', ';
            }
        }
        
        pool.request()
            .query(sql)
        .then((rows) => {
            resolve(rows.recordset)
        }).catch((err) => {
            reject(err);
        });
    });
}

function deleteTableRows(tableName, filterObject, operand) {
    return new Promise (function (resolve,reject){
        var filter = ""
        if(!operand){var operand=" AND "}
  //      thisreq = pool.request()
        var params = []
        var fieldTypePromise = []
        for (var i=0;i<Object.keys(filterObject).length;i++){
                if (i==0){ filter += " WHERE " }else{ filter+=operand}
                var fieldName = Object.keys(filterObject)[i]
                filter += fieldName
                filter += "=@" + fieldName
                fieldTypePromise.push (getFieldType(tableName,fieldName,filterObject[fieldName]))
        }

        var sql ="DELETE FROM " + tableName + filter

        Promise.all (fieldTypePromise).then (function (params){
          thisReq = pool.request()
          params.forEach(function (param){
            thisReq.input (param.fieldName,param.fieldType,param.data)
          })
          thisReq.query(sql).then(function (results){
            resolve(results)
          }).catch(function (err){
            console.log(err)
            reject(err)
          })
        }).catch (function (err){
            console.log(err)
            reject(err)
        })
    })
}
