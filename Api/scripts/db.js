const asql = require('mssql');
const winston = require('../core/winston');
const env = require('./env');

function pool() {
    return new Promise(async (resolve, reject) => {
        // winston.promise(`in promise for function pool`);
        const config = {
            //need to be env variables
            user: env.devDatabase.username,
            password: env.devDatabase.password,
            server: env.devDatabase.server, // You can use 'localhost\\instance' to connect to named instance
            database: env.devDatabase.database,
            port: env.devDatabase.port
            // "options": { //options needed for azureDB
            //     "encrypt": true,
            //     "enableArithAbort": true
            // }
        }

        var pool = await asql.connect(config);
        if (pool) {
            resolve(pool);
        } else {
            reject(false);
        }
    });
}

module.exports.pool = pool
