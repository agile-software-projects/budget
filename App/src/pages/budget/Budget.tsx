import React, {useState} from 'react';
import { IonModal, IonAlert, IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonCard, IonCardHeader, IonCardTitle, IonProgressBar, IonCardSubtitle, IonChip, IonLabel, IonItem, IonPage} from '@ionic/react';
import './Budget.css';
import '../../theme/custom-colors.css'
import '../../theme/custom.css'
import { useStore } from '../../store/store-actions';
import useModal from '../../helpers/useModal';
import Menu from '../menu/Menu';
import { trashOutline, createOutline } from 'ionicons/icons';
import { priceFormat } from '../../helpers/price';
import AddBudget from '../add-budget/AddBudget';
import EditBudget from '../edit-budget/EditBudget';

const Budget: React.FC = () => {

	const {budgets, deleteBudget, selectBudget, selectedBudget} = useStore();

	const [isFormOpen, toggleForm] = useModal();

	const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'November', 'December'];

	function isMonthActive(month: number) {
		let currentMonth = new Date().getMonth();
		return (month === currentMonth ? "active-chip-dark" : "dark");
	}

	const [isEditBudgetOpen, setIsEditBudgetOpen] = useState(false);
	const [showDeleteAlert, setShowDeleteAlert] = useState(false);

	const handleOpenEditBudget = (budget: any) => {
		selectBudget(budget);
		setIsEditBudgetOpen(true);
		
	}

	const handleConfirmDelete = (budget: any) => {

		selectBudget(budget);
		setShowDeleteAlert(true);
	}

	const handleDeleteSelectedBudget = () => {
		try{
			deleteBudget(selectedBudget);
		}catch(e){
			console.error(e)
		}		
	}


		// create list from state data
		const list = budgets.map((item, index) => 
		{	
			return (
				<IonCard key={index}>
				<IonCardHeader>
					<IonItem className="ion-no-padding" lines="none">
						<div className="card-top">
							<IonLabel className="card-label">{capitalizeFirstLetter(item.budgetName)}</IonLabel>
							<IonLabel className="card-label" color="gray">{item.categoryName}</IonLabel>
								<IonButton className="trashIcon" fill="clear" onClick={() => {handleConfirmDelete(item)}}>
                                    <IonIcon slot="icon-only" icon={trashOutline} color="dark" className="trashBackground"/>
                                </IonButton>
								<IonButton className="editIcon" fill="clear" onClick={() => handleOpenEditBudget(item)}>
                                    <IonIcon slot="icon-only" icon={createOutline} color="dark"  className="trashBackground"/>
                                </IonButton>
						</div>
					</IonItem>
					<div className="card-bottom">
						<IonCardTitle>{priceFormat(item.totalLimit)}</IonCardTitle>
						<div className="progress-bar-container">
						<IonProgressBar value={item.totalLimit>0?Number((item.currentAmount/item.totalLimit).toFixed(2)):0} color="green"></IonProgressBar>
								<IonCardSubtitle>{priceFormat(item.currentAmount)}</IonCardSubtitle>
						</div>
					</div>
				</IonCardHeader>
			</IonCard>
			);
		}
	)
	
	function capitalizeFirstLetter(string: String){
		return string
		// return string[0].toUpperCase() + string.slice(1).toLowerCase();
	}


	return (
		<IonPage className="budget">
			<IonContent fullscreen color="light">

				<AddBudget isOpen={isFormOpen} hide={toggleForm}/>

				<IonModal isOpen={isEditBudgetOpen}>
                    <EditBudget
                        hide={setIsEditBudgetOpen}
                    />
                </IonModal>

				<IonHeader className="header">
					<IonToolbar color="light" className="top-bar">
						<IonTitle size="large" className="title">Budget</IonTitle>
						<IonButtons slot="end">
							<Menu/>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
				<div className="months-filter">
					{months.map((value, index) => 
						<IonChip className={isMonthActive(index)} key={index}>
							<IonLabel>{value}</IonLabel>
						</IonChip>
					)}
				</div>
				<div className="body-actions">
					<span className="body-action" onClick={toggleForm as any}>+ Add Budget</span>
				</div>
				<div className="list">
					{list}
				</div>
				<IonAlert
					isOpen={showDeleteAlert}
					onDidDismiss={() => setShowDeleteAlert(false)}
					cssClass='my-custom-class'
					header={'Delete Budget?'}
					buttons={[
						{
							text: 'Cancel',
							role: 'cancel',
							cssClass: 'secondary',
							handler: () => {
								console.log('Confirm Cancel');
							}
						},
						{
							text: 'Delete',
							cssClass: 'warning',
							handler: () => {
								handleDeleteSelectedBudget();
							}
						}
					]}
				/>

				{budgets.length < 1 && <IonLabel>No saved items</IonLabel>}

			</IonContent>
		</IonPage>
	);
};

export default Budget;
