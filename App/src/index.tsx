import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { StoreProvider } from './store/store';
import IDB from "./helpers/IDB";

const idb = new IDB("budget", 1);
idb.createKeyStore([
    "clientUser",
    "accounts",
    "categories"
])

ReactDOM.render(<StoreProvider><App /></StoreProvider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
