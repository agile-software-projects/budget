const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const baseUrl = "http://localhost:3000/category"; 

describe("Testing inserting category", () => {

    it.skip("Insert valid category", (done) => {
        chai.request(baseUrl)
            .post('/new')
            .send({
                categoryName: "Groceries",
                parentCategoryId: null
            }) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            assert.strictEqual(typeof res.body.error, 'undefined')
            done();
        });
    });

    it("Return status 400 if category name is bad", (done) => {
        chai.request(baseUrl)
            .post('/new') 
            .send({
                categoryName: "Ch0c0l@te!<>sd",
                parentCategoryId: null
            }) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "New category could not be validated");
            done();
        });
    });
});

describe("Testing /:field/:code/getCategory", () => {

    it("Get category from categoryId", (done) => {
        chai.request(baseUrl)
            .get('/categoryId/25003e8d-2dfc-46d5-8fba-eee148ae2e1e/getCategory')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Errors on a bad field", (done) => {
        chai.request(baseUrl)
            .get('/awefawef/25003e8d-2dfc-46d5-8fba-eee148ae2e1e/getCategory')
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "unknown field identifier");
            done();
        });
    });

    it("Errors on a bad category id", (done) => {
        chai.request(baseUrl)
            .get('/categoryId/fasdcawe/getCategory')
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            done();
        });
    });
});

describe("Testing /:field/:code/updateCategory", () => {

    it.skip("Updates category name from categoryId", (done) => {
        chai.request(baseUrl)
            .post('/categoryId/25003e8d-2dfc-46d5-8fba-eee148ae2e1e/updateCategory')
            .send({
                categoryName: 'Category 2'
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Updates category parent id (valid) from categoryId", (done) => {
        chai.request(baseUrl)
            .post('/categoryId/25003e8d-2dfc-46d5-8fba-eee148ae2e1e/updateCategory')
            .send({
                categoryName: 'Category 2',
                parentCategoryId: '394475af-1126-4c88-9394-a3bea139bc9f'
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Errors on a bad category id", (done) => {
        chai.request(baseUrl)
            .post('/categoryId/fasdcawe/updateCategory')
            .send({
                categoryName: 'Category 2'
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            assert.strictEqual(res.body.error, "category does not exist")
            done();
        });
    });

    it("Errors on update with bad parent category id", (done) => {
        chai.request(baseUrl)
            .post('/categoryId/25003e8d-2dfc-46d5-8fba-eee148ae2e1e/updateCategory')
            .send({
                categoryName: 'Category 2',
                parentCategoryId: '394475af-1126-4c88-9394-a3bea139b100'
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "parent id does not exist")
            done();
        });
    });

    it.skip("Errors on no body", (done) => {
        chai.request(baseUrl)
            .post('/categoryId/25003e8d-2dfc-46d5-8fba-eee148ae2e1e/updateCategory')
            .send({})
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "no category name detected in body");
            done();
        });
    });

});

describe("Testing /:field/:code/deleteCategory", () => {

    it.skip("Deletes an existing category from categoryId", (done) => {
        chai.request(baseUrl)
            .post('/categoryId/5b2535e0-f207-45fd-8045-392d3ccc4377/deleteCategory')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Returns error if wants to delete a parent category", (done) => {
        chai.request(baseUrl)
            .post('/categoryId/394475af-1126-4c88-9394-a3bea139bc9f/deleteCategory')
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "cannot delete parent category");
            done();
        });
    });

});