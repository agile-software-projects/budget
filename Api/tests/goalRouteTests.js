const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const baseUrl = "http://localhost:3000/goal"


const goodData = {
    goalName: "yatch",
    goalAmount: 200000,
    targetDate: "2022-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000
}

const badName = {
    goalName: "h0u$e><",
    goalAmount: 200000,
    targetDate: "2022-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000
}

const badAmount = {
    goalName: "yatch",
    goalAmount: "sfdfdf",
    targetDate: "2022-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000
}

const badDate = {
    goalName: "yatch",
    goalAmount: 200000,
    targetDate: "30-30-30",
    active: true,
    currentAmount: 20000
}

const badBoolean = {
    goalName: "yatch",
    goalAmount: 200000,
    targetDate: "2022-01-27 23:07:51.523",
    active: "not a flag",
    currentAmount: 20000
}

describe.skip("Testing /:accountid/new", () => {
    
    it.skip("Return 400 if account does not exist", (done) => {
        chai.request(baseUrl)
            .post('/1/new') //can be post etc
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "account doesn't exist");
            done();
        });
    });

    it.skip("Inserting valid goal", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/new')
            .send(goodData) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    });

    it.skip("Return status 400 if goal name is bad", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/new')
            .send(badName) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "The goal name is not valid");
            done();
        });
    });

    it.skip("Return status 400 if goal amount is not a number", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/new')
            .send(badAmount) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "total goal amount must be of type Int");
            done();
        });
    });

    it.skip("Return status 400 if target date is not a date", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/new')
            .send(badDate) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "creation date must be of Date type");
            done();
        });
    });

    it.skip("Return status 400 if active is not a boolean", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/new')
            .send(badBoolean) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "The active flag is not valid");
            done();
        });
    });

});


describe.skip("Testing /:field/:code/getGoal", () => {

    it.skip("Get goal from goalId", (done) => {
        chai.request(baseUrl)
            .get('/goalId/6f372aa0-8e90-47a5-abcc-dbd3c35aa822/getGoal')
            .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    
    it.skip("Errors on a bad field", (done) => {
        chai.request(baseUrl)
            .get('/awefawef/6f372aa0-8e90-47a5-abcc-dbd3c35aa822/getGoal')
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "unknown field identifier");
            done();
        });
    });

    it("Errors on a bad goal id", (done) => {
        chai.request(baseUrl)
            .get('/goalId/fgfgfggf/getGoal')
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            assert.strictEqual(res.body.error, "goal id does not exist");
            done();
        });
    });

});

describe("Testing /:field/:code/updateGoal", () => {

    it.skip("Updates goal name from goalId", (done) => {
        chai.request(baseUrl)
            .post('/goalId/6f372aa0-8e90-47a5-abcc-dbd3c35aa822/updateGoal')
            .send({
                goalName: "mansion"
            })

        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it.skip("Updates goal amount from goalId", (done) => {
        chai.request(baseUrl)
            .post('/goalId/6f372aa0-8e90-47a5-abcc-dbd3c35aa822/updateGoal')
            .send({
                goalAmount: 500000
            })

        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it.skip("Updates goal target date from goalId", (done) => {
        chai.request(baseUrl)
            .post('/goalId/6f372aa0-8e90-47a5-abcc-dbd3c35aa822/updateGoal')
            .send({
                targetDate: "2021-12-31 23:07:51.523"
            })

        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it.skip("Updates goal active status from goalId", (done) => {
        chai.request(baseUrl)
            .post('/goalId/6f372aa0-8e90-47a5-abcc-dbd3c35aa822/updateGoal')
            .send({
                active: false
            })

        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it.skip("Updates goal current amount from goalId", (done) => {
        chai.request(baseUrl)
            .post('/goalId/6f372aa0-8e90-47a5-abcc-dbd3c35aa822/updateGoal')
            .send({
                currentAmount: 50000
            })

        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it.skip("Errors on a bad goal id", (done) => {
        chai.request(baseUrl)
            .post('/goalId/fasdcawe/updateGoal')
            .send({
                goalName: "new goal"
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            done();
        });
    });

    it.skip("Errors on no body", (done) => {
        chai.request(baseUrl)
            .post('/goalId/6f372aa0-8e90-47a5-abcc-dbd3c35aa822/updateGoal')
            .send({})
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "no goal name, amount, target date, active state or current amount detected in body");
            done();
        });
    });

});

describe("Testing /:field/:code/deleteGoal", () => {

    it.skip("Deletes an existing goal from goalId", (done) => {
        chai.request(baseUrl)
            .post('/goalId/efbd9a1a-d673-4951-9b9b-42744a3bdb19/deleteGoal')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });

    });

    it("Error when trying to delete an non-existing goal from goalId", (done) => {
        chai.request(baseUrl)
            .post('/goalId/efbd9a1a-d673-4951-9b9b-42744a3bdb19/deleteGoal')
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            assert.strictEqual(res.body.error, "Savings goal not deleted");
            done();
        });

    });

});
