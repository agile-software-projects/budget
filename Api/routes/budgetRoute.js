const express = require('express');
const router = express.Router();
const winston = require('../core/winston');
const env = require('../scripts/env');
const User = require('../classes/User');
const Budget = require('../classes/Budget');
const Account = require('../classes/Account');

router.all('*', (req, res, next) => {
    if(env.devDatabase.enabled) {
        next();
    } else {
        res.status(500).json({error: "No database detected"});
    }
});

router.post("/createDefaults", (req, res) => {
    if (req.session && req.session.user) {
        const user = new User({userId: req.session.user.userId})
        user.populate().then(() => {
            user.setupDefaults().then(() => {
                res.status(200);
                return res.json({success: true});
            }).catch((err) => {
                winston.error("could not set defaults /createDefaults")
                winston.error(err);
                res.status(500);
                return res.json(err);
            })
        }).catch((err) => {
            winston.error("could not populate user object while creating defaults");
            winston.error(err);
            res.status(400);
            return res.json(err);        
        })
    } else {
        winston.error("returning 401 creating defaults. No user");
        res.status(401);
        return res.json({error: "You are not authenticated"});
    }
});

//Must have an endpoint for creating new budgets
router.post("/:accountid/new", (req, res) => {
    const account = new Account({accountId: req.params.accountid});
    account.populate().then(() => {
        const budget = new Budget({
            budgetName: req.sanitize(req.body.budgetName),
            totalLimit: Number.parseInt(req.sanitize(req.body.totalLimit)),
            accountId: req.sanitize(req.params.accountid),
            active: req.body.active,
            resetPeriod: req.sanitize(req.body.resetPeriod),
            startDate: req.sanitize(req.body.startDate),
            endDate: req.sanitize(req.body.endDate)
        });
        try {
            budget.assignBudgetId().assignCurrentPeriodId().validate().insertBudget().then((success) => {
                if (success.success) {
                    res.status(200);
                    return res.json({success: true});
                } else {
                    winston.error("Error inserting budget");
                    res.status(500);
                    return res.json({error: "There was an error insertting"});
                }
            }).catch((err) => {
                winston.error(JSON.stringify(err));
                res.status(400);
                return res.json(err);
            });
        } catch(err) {
            winston.error(JSON.stringify(err));
            res.status(400);
            return res.json({error: err});
        }
    }).catch((err) => {
        winston.error(err);
        res.status(400);
        return res.json(err);
    })
});

//Must have an endpoint for updating existing categories
router.post("/:budgetid/update", (req, res) => {
    const budget = new Budget({budgetId: req.sanitize(req.params.budgetid)});
    budget.populate().then(() => {
        budget.budgetName = req.sanitize(req.body.budgetName);
        budget.totalLimit = Number.parseInt(req.sanitize(req.body.totalLimit));
        budget.active = req.body.active;
        budget.resetPeriod = req.sanitize(req.body.resetPeriod);
        try {
            budget.validate().update().then((success) => {
                if (success.success) {
                    res.status(200);
                    return res.json(success);
                } else {
                    res.status(500);
                    return res.json({error: "Could not update"});
                }
            }).catch((err) => {
                winston.error(JSON.stringify(err));
                res.status(400);
                return res.json(err);
            })
        } catch(err) {
            winston.error(JSON.stringify(err));
            res.status(400);
            return res.json({error: err});
        }
    }).catch((err) => {
        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);
    });
});

//Must have an endpoint for deleting existing budgets
router.post("/:budgetid/delete", (req, res) => {
    const budget = new Budget({budgetId: req.sanitize(req.params.budgetid)});
    budget.populate().then(() => {
        budget.deleteBudget().then((success) => {
            if (success.success) {
                res.status(200);
                return res.json(success);
            } else {
                res.status(500);
                return res.json({error: "could not delete"});
            }
        });
    }).catch((err) => {
        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);
    });
});

//Must have an endpoint to retreive a budget
router.get("/:budgetid/get", (req, res) => {
    const budget = new Budget({budgetId: req.sanitize(req.params.budgetid)});
    budget.populate().then(() => {
        res.status(200);
        return res.json({budget: budget.wholeBudget()});
    }).catch((err) => {
        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);
    });
});

//Must have an endpoint for retreiving all categories assosciated to a budget
router.get("/:budgetid/categories/getAll", (req, res) => {
    const budget = new Budget({budgetId: req.sanitize(req.params.budgetid)});
    budget.populate().then(() => {
        budget.getAllAccountCategories().then((categories) => {
            res.status(200);
            return res.json({categories: categories});
        }).catch((err) => {
            winston.error(JSON.stringify(err));
            res.status(400);
            return res.json(err);
        });
    }).catch((err) => {
        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);
    });
});

//Must have an endpoint to retreive all user budgets
//Can't easily create a unit test for this endpoint since it depends on the 
//user being in the req.session which only happens after authentication. 
//will have to be tested in frontend testing. 
router.get("/user/getAll", (req, res) => {
    //going to get from the stored user
    let userId = "";
    if (req.session.user) {
        userId = req.session.user.userId;
    } else {
        res.status(401);
        return res.json({error: "Somehow someone got past the authentication"})
    }

    const user = new User({userId: userId});
    user.populate().then(() => {
        user.getAllUserBudgets().then((budgets) => {
            res.status(200);
            return res.json(budgets);
        }).catch((err) => {
            winston.error(JSON.stringify(err));
            res.status(400);
            return res.json(err);
        });
    }).catch((err) => {
        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);
    });
});

module.exports = router;