const { v4: uuidv4 } = require('uuid');
const databaseFunctions = require('../core/databaseFunctions');

class Budget {
    constructor(budget) {
        this.budgetId = budget.budgetId;
        this.budgetName = budget.budgetName;
        this.totalLimit = budget.totalLimit;
        this.accountId = budget.accountId;
        this.active = budget.active;
        this.resetPeriod = budget.resetPeriod;
        this.currentPeriod = budget.currentPeriod;
        this.startDate = budget.startDate;
        this.endDate = budget.endDate;
    }

    wholeBudget() {
        return {
            budgetId: this.budgetId,
            budgetName: this.budgetName,
            totalLimit: this.totalLimit,
            accountId: this.accountId,
            active: this.active,
            resetPeriod: this.resetPeriod,
            currentPeriod: this.currentPeriod,
            startDate: this.startDate,
            endDate: this.endDate
        }
    }

    assignBudgetId() {
        this.budgetId = uuidv4();
        return this;
    }

    assignCurrentPeriodId() {
        this.currentPeriod = uuidv4();
        return this;
    }

    validate() {

        if (this.budgetId.length !== 36) { throw("The budgetId is not valid") }
        if (this.accountId.length !== 36) { throw("The accountId is not valid") }
        if (typeof this.active !== "boolean" && this.active !== 1 && this.active !== 0) { throw("The active flag is not valid") }

        if (!(Number.isInteger(this.totalLimit))) { throw("total limit must be of type Int") }

        const stringRe = /^(?=[a-zA-Z0-9._\ \-]{1,32}$)/;
        if (!(stringRe.test(this.budgetName))) { throw("The budget name is not valid") }

        const resetPeriodRe = /^([0-9])+([dwmy])$/;
        if (!(resetPeriodRe.test(this.resetPeriod))) { throw("Reset period must be numeric value plus d, w m or y (eg 1m for monthly)") }

        const isDate = (date) => {
            return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
        }
        if (!(isDate(this.startDate))) { throw new Error("start date must be of Date type"); }
        if (!(isDate(this.endDate))) { throw new Error("end date must be of Date type"); }
        
        return this;
    }

    activate() {
        this.active = true;
        return this;
    }

    deactivate() {
        this.active = false;
        return this;
    }  

    verifyAccount() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("account", {accountId: this.accountId}).then((accounts) => {
                if (accounts.length === 1) {
                    resolve(accounts[0]);
                } else {
                    reject({error: "The account does not exist"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    insertBudget() {
        //check the budget exists and insert if not
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("budget", {budgetId: this.budgetId}).then((budgets) => {
                if (budgets.length === 0) {
                    databaseFunctions.insertTableRow("budget", this.wholeBudget()).then((rowsAffected) => {
                        if (rowsAffected[0] === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "Could not insert budget"});
                        }
                    }).catch((err) => {
                        reject({error: err});
                    });
                }
            }).catch((err) => {
                reject({error: err});
            })
        });
    }   

    populate() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("budget", {budgetId: this.budgetId}).then((budgets) => {
                if (budgets.length === 1) {
                    const budget = budgets[0];
                    this.budgetId = budget.budgetId;
                    this.budgetName = budget.budgetName;
                    this.totalLimit = budget.totalLimit;
                    this.accountId = budget.accountId;
                    this.active = budget.active;
                    this.resetPeriod = budget.resetPeriod;
                    this.currentPeriod = budget.currentPeriod;
                    this.startDate = budget.startDate;
                    this.endDate = budget.endDate;
                    resolve({success: true});
                } else {
                    reject({error: "Budget not found"});
                }
            }).catch((err) => {
                reject({error: err});
            })
        });
    }

    update() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("budget", {budgetId: this.budgetId}).then((budgets) => {
                if (budgets.length === 1) {
                    databaseFunctions.updateTableRow("budget", {budgetId: this.budgetId}, this.wholeBudget()).then((rowsAffected) => {
                        if (rowsAffected === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "Unable to update budget"});
                        }
                    }).catch((err) => {
                        reject({error: err});
                    })
                } else {
                    reject({error: "Budget does not exist"});
                }
            }).catch((err) => {
                reject({error: err});
            })
        });
    }

    deleteBudget() {
        return new Promise((resolve, reject) => {
            databaseFunctions.deleteTableRows("budget", {budgetId: this.budgetId}).then((rowsAffected) => {
                if (rowsAffected.rowsAffected[0] === 1) {
                    resolve({success: true});
                } else {
                    reject({error: "Unable to delete budget"});
                }
            }).catch((err) => {
                reject({error: err});
            })
        });
    }

    getAllAccountCategories() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("budgetCategoriesView", {budgetId: this.budgetId}).then((categories) => {
                if (categories.length > 0) {
                    resolve(categories);
                } else {
                    reject({error: "No Categories found"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }
}

module.exports = Budget;