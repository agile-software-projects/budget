const assert = require('assert');
const Category = require('../classes/Category');

const goodDataNoParent = {
    categoryId: "00000000-0000-0000-0000-000000000000",
    categoryName: "Transport",
    parentCategoryId: null
}

const badGuid = {
    categoryId: "0-000000000000",
    categoryName: "Transport",
    parentCategoryId: null
}

const badName = {
    categoryId: "00000000-0000-0000-0000-000000000000",
    categoryName: "Tr@spo&t",
    parentCategoryId: null
}

const badParent = {
    categoryId: "00000000-0000-0000-0000-000000000000",
    categoryName: "Transport",
    parentCategoryId: "0-000000000000"
}

const goodDataWithParent = {
    categoryId: "00000000-0000-0000-0000-000000000000",
    categoryName: "Taxi",
    parentCategoryId: "0b46b90d-8ae7-41c4-9913-68fc1aa9ff03"
}



describe("Tests the category class", () => {

    it("returns the whole category", (done) => {
        const category = new Category(goodDataNoParent);
        assert.strictEqual(category.categoryId, goodDataNoParent.categoryId);
        assert.strictEqual(category.categoryName, goodDataNoParent.categoryName);
        assert.strictEqual(category.parentCategoryId, goodDataNoParent.parentCategoryId);
        done();
    });

    it("successfully validates a good category object", (done) => {
        const category = new Category(goodDataNoParent);
        assert.strictEqual(category.validate(), category);
        done();
    });

    it("fails validation on bad ID", (done) => {
        const category = new Category(badGuid);
        assert.strictEqual(category.validate(), false);
        done();
    });

    it("fails validation on bad name", (done) => {
        const category = new Category(badName);
        assert.strictEqual(category.validate(), false);
        done();
    });

    it("fails validation on bad parent", (done) => {
        const category = new Category(badParent);
        assert.strictEqual(category.validate(), false);
        done();
    });

    it.skip("inserts a category without parent", (done) => {
        const category = new Category(goodDataNoParent);

        setTimeout(() => {
            category.validate().assignCategoryId().insertCategory().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("verifies the parent category", (done) => {
        const category = new Category(goodDataWithParent);

        setTimeout(() => {
            category.verifyParentID().then((parent) => {
                assert.strictEqual(parent.categoryId, goodDataWithParent.parentCategoryId);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("inserts a category with parent", (done) => {
        const category = new Category(goodDataWithParent);

        setTimeout(() => {
            category.validate().assignCategoryId().insertCategory().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("populates a category from the id", (done) => {
        const categoryId = "cd58e83f-4787-4e2e-862d-5f2393931cb8";
        const category = new Category({categoryId: categoryId});
    
        setTimeout(() => {
            category.populate().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });

    it.skip("updates a category", (done) => {
        const categoryId = "cd58e83f-4787-4e2e-862d-5f2393931cb8";
        
        const category = new Category(goodDataWithParent);
        category.categoryName = "Bus";
        category.categoryId = categoryId;
        setTimeout(() => {
            category.validate().update().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    }); 

    it.skip("deletes a category", (done) => {
        const categoryId = "dca535b6-4f59-401b-b8fd-6ac411b8f05d";
        
        const category = new Category(goodDataNoParent);
        category.categoryId = categoryId;

        setTimeout(() => {
            category.deleteCategory().then((success) => {
                assert.strictEqual(success.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    }); 

});