import { useState } from 'react';

const useModal = () => {
  const [isShowing, setIsShowing] = useState(false)

    const toggle = () => {
        setIsShowing(!isShowing)
    }

    return [
        isShowing,
        toggle as any,
    ]
}

export default useModal;