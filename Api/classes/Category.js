const { v4: uuidv4 } = require('uuid');
const databaseFunctions = require('../core/databaseFunctions');
const sql = require('mssql')

class Category {
    constructor(category) {
        this.categoryId = category.categoryId;
        this.categoryName = category.categoryName;
        this.parentCategoryId = category.parentCategoryId;
    }

    wholeCategory() {
        return {
            categoryId: this.categoryId,
            categoryName: this.categoryName,
            parentCategoryId: this.parentCategoryId
        }
    }

    assignCategoryId() {
        this.categoryId = uuidv4();
        return this;
    }

    validate() {
        if (this.categoryId.length !== 36) { return false; }

        const stringRe = /^(?=[a-zA-Z0-9._\ \-]{1,20}$)/;
        if (!(stringRe.test(this.categoryName))) { return false; }

        if (this.parentCategoryId) { 
            if (this.parentCategoryId.length !== 36) {
                return false;
            }
        }

        return this;
    }

    verifyParentID() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("category", {categoryId: this.parentCategoryId}).then((categories) => {
                if (categories.length === 1) {
                    resolve(categories[0]);
                } else {
                    reject({error: "Parent category does not exist"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
        
    }

    isParentCategory() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("category", {parentCategoryId: this.categoryId}).then((categories) => {
                if (categories.length === 0) {
                    return false;
                } else {
                    return true;
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    insertCategory() {
        //check category doesnt exist and insert if not
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("category", {categoryId: this.categoryId}).then((categories) => {
                if (categories.length === 0) {
                    databaseFunctions.insertTableRow("category", this.wholeCategory()).then((rowsAffected) => {
                        if (rowsAffected[0] === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "there was an error inserting category"});
                        }
                    });
                } else {
                    reject({error: "category already exists"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    populate() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("category", {categoryId: this.categoryId}).then((categories) => {
                if (categories.length === 1) {
                    const category = categories[0];
                    this.categoryId = category.categoryId;
                    this.categoryName = category.categoryName;
                    this.parentCategoryId = category.parentCategoryId;
                    resolve({success: true});
                } else {
                    reject({error: "category does not exist"});
                }
            }).catch((err) => { 
                reject({error: err});
            });
        });
    }

    update() {
        //check category exists and update if it does
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("category", {categoryId: this.categoryId}).then((categories) => {
                if (categories.length === 1) {
                    databaseFunctions.updateTableRow("category", {categoryId: this.categoryId}, this.wholeCategory()).then((rowsAffected) => {
                        if (rowsAffected === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "could not update"});
                        }
                    }).catch((err) => {
                        reject({error: err});
                    });
                } else {
                    reject({error: "category does not exist"});
                }
            }).catch((err) => { 
                reject({error: err});
            })
        });
    }

    deleteCategory() {
        //delete the category
        return new Promise((resolve, reject) => {
            databaseFunctions.deleteTableRows("category", {categoryId: this.categoryId}).then((rowsAffected) => {
                if (rowsAffected.rowsAffected[0] === 1) {
                    resolve({success: true});
                } else {
                    reject({error: "could not be deleted"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    static getAll(){
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("category", {isDefault: true}).then((categories) => {
                resolve(categories)
            }).catch((err) => { 
                reject(err)
            })
        })
    }

    static getCategoryTransactions({categoryId, from, to, userId}){
        return new Promise((resolve, reject) => {
            sql.query`
            SELECT transactionCategoryLink.linkId, transactionCategoryLink.categoryId, T.accountId, T.transactionDate, T.amount, T.transactionDirection, A.userId
            FROM transactionCategoryLink

            INNER JOIN transactions AS T 
            ON transactionCategoryLink.transactionId=T.transactionId 

            INNER JOIN account as A 
            ON T.accountId=A.accountId

            INNER JOIN users as U 
            ON A.userId=U.userId
            
            WHERE transactionCategoryLink.categoryId = ${categoryId}
            AND T.transactionDate BETWEEN ${from} AND ${to}
            AND A.userId = ${userId};`
            .then(result => {
                resolve(result.recordset)
            }).catch(err => {
                reject(err)
            })
        })
    }

    linkToBudget(budgetId) {
        return new Promise((resolve, reject) => {
            databaseFunctions.insertTableRow("budgetCategoryLink", {
                linkId: uuidv4(),
                budgetId: budgetId,
                categoryId: this.categoryId
            }).then((rowsAffected) => {
                if (rowsAffected[0] === 1) {
                    resolve({success: true});
                } else {
                    reject({error: "there was an error inserting category"});
                }
            }).catch((err) => {
                reject(err);
            })
        })
    }
}

module.exports = Category;