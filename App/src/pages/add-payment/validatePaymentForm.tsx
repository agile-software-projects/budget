export default function validatePaymentForm(values: any, isSavings: boolean) {

    let errors: any = {};

    const inFuture = (date: Date) => {
        let transDate = new Date(date);
        return transDate.setHours(0, 0, 0, 0) > new Date().setHours(0, 0, 0, 0)
    };

    if (!values.descr.trim()) {
        errors.descr = "Description required";
    } else if (!/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/.test(values.descr)) {
        errors.descr = "Only letters and numbers allowed";
    }

    if (!values.accountId) {
        errors.accountId = "Please select an account";
    }

    if (isSavings) {
        if (!values.goalId) {
            errors.goalId = "Please select a savings goal";
        }
    }

    if (!values.transDate.trim()) {
        errors.transDate = "Transaction date required";
    } else if (inFuture(values.transDate)) {
        errors.transDate = "Transaction date cannot be after current date";
    }

    return errors;
}