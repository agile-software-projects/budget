import { SignupFormValues } from "./form-interfaces";

export function validateSignupForm(values: SignupFormValues) {
    
    let errors = {} as SignupFormValues

    if (!values.username.trim()) {
        errors.username = "Username is required";
    } else if (!/^[A-Za-z0-9]*$/.test(values.username)) {
        errors.username = "Only letters and numbers allowed"
    }

    if (!values.email.trim()) {
        errors.email = "Email is required"
    } else if(!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(values.email)){
        errors.email = "Invalid email format"
    }

    if (!values.password.trim()) {
        errors.password = "Password is required";
    } else if (values.password.trim()){
        if(!/(?=.*[!@#$%^&*])/.test(values.password)){
            errors.password = "Must contain at least one special char"
        }else if(!/(?=.*[0-9])/.test(values.password)){
            errors.password = "Must contain at least 1 numeric character"
        }else if(!/(?=.{8,})/.test(values.password)){
            errors.password = "Must contain at least 8 chars"
        }
    }
    
    return errors;
}