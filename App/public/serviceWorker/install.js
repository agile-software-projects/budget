self.addEventListener('install', event => {
    event.waitUntil(
        caches.open(cacheName).then((cache) => {
            cache.addAll(cacheData)
        }).then(() => {
            self.skipWaiting()
        })
    );
    console.log("SW installed");
});