const express = require('express');
const router = express.Router();
const winston = require('../core/winston');
const env = require('../scripts/env');
const {v4:uuidv4} = require('uuid');
const authMiddleware = require('../scripts/middleware/authMiddleware');
const User = require('../classes/User');

router.all('*', (req, res, next) => {
    if(env.devDatabase.enabled) {
        next();
    } else {
        res.status(500).json({error: "No database detected"});
    }
});

//open access - no auth token required
//password reset
/*
 * reset request by email or userid or username 
 */
//needs a merge from auth route /user class changes to implement
router.post("/:field/:code/updatePassword", (req, res) => {
    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);
    const userData = {};
    switch(field) {
        case "userId":
            userData.userId = code;
            break;
        case "email":
            userData.email = code;
            break;
        case "username":
            userData.username = code;
            break;
        default:
            res.status(400);
            return res.json({error: "unknown field identifier"});
    }
    const user = new User(userData);
    user.populate().then(() => {
        //throw if no password
        if ((Object.keys(req.body).length === 0) || (!(req.body.password))) {
            res.status(400);
            return res.json({error: "no password detected in body"});
        }
        const password = req.body.password;
        user.password = password;
        user.hashPassword().then(() => {
            user.update().then(() => {
                res.status(200);
                return res.json({success: true});
            }).catch((err) => {
                winston.error(JSON.stringify(err));
                res.status(500);
                return res.json(err);
            });
        }).catch((err) => {
            res.status(500);
            return res.json({error: "Error hashing new password"});
        });
    }).catch((err) => {
        console.log(err);
        winston.error(JSON.stringify(err));
        res.status(500);
        return res.json(err);
    });
});

//closed access - requires auth token
// router.all('*', authMiddleware.authenticateToken, (req, res, next) => {
//     next();
// }); 

// get user details
/*
 * Must not include the password field under any circumstances
 * Username, email and userId only
 * 
 * Get by userId, username or email
 */
router.get("/:field/:code/getUser", (req, res) => {
    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);
    const userData = {};
    switch(field) {
        case "userId":
            userData.userId = code;
            break;
        case "email":
            userData.email = code;
            break;
        case "username":
            userData.username = code;
            break;
        default:
            res.status(400);
            return res.json({error: "unknown field identifier"});
    }
    const user = new User(userData);
    user.populate().then(() => {
        res.status(200);
        return res.json({
            userId: user.userId,
            email: user.email,
            username: user.username
        });
    }).catch((err) => {
        winston.error(err);
        res.status(500);
        return res.json(err);
    });
});

//update user details
/*
 * Email is only field that can be updated
 * Update by username or userId only
 */
router.post("/:field/:code/updateUser", (req, res) => {
    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);
    const userData = {};
    switch(field) {
        case "userId":
            userData.userId = code;
            break;
        case "username":
            userData.username = code;
            break;
        default:
            res.status(400);
            return res.json({error: "unknown field identifier"});
    }
    const user = new User(userData);
    user.populate().then(() => {
        //throw with no email to update
        if ((Object.keys(req.body).length === 0) || (!(req.body.email))) {
            res.status(400);
            return res.json({error: "no email detected in body"});
        }
        user.email = req.sanitize(req.body.email);
        user.validate().update().then(() => {
            res.status(200);
            return res.json({success: true});
        }).catch((err) => {
            winston.error(err);
            res.status(500);
            return res.json(err);
        })
    }).catch((err) => {
        winston.error(err);
        res.status(500);
        return res.json(err);
    });
});

router.all("*", authMiddleware.authenticateToken, (req, res, next) => {
    next();
});

router.get("/loggedInUser", (req, res) => {
    if (req.session && req.session.user) {
        res.status(200);
        return res.json({userId: req.session.user.userId});
    } else {
        res.status(401);
        res.json({error: "user not authenticated"});
    }
});

module.exports = router;