import React from 'react';
import { Redirect, Route } from 'react-router-dom';

import {
    IonApp,
    IonIcon,
    IonRouterOutlet,
    IonTabBar,
    IonTabButton,
    IonTabs
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { homeOutline, analyticsOutline, copyOutline, addOutline, walletOutline } from 'ionicons/icons';
import Home from '../pages/home/Home';
import Categories from '../pages/categories/Categories';

import Dashboard from '../pages/dashboard/Dashboard';
import AddPayment from '../pages/add-payment/AddPayment';
import useModal from '../helpers/useModal';
import Goals from '../pages/goals/Goals';
import Budget from '../pages/budget/Budget';
import TransactionHistory from '../pages/transactionHistory/TransactionHistory';
import RecurringExpenses from '../pages/recurring-expenses/RecurringExpenses';


const Layout = (props: any) => {
    const [isDashboardOpen, toggleDashboard] = useModal()
	const [isFormOpen, toggleForm] = useModal()

	return (
        <IonApp>

			<Dashboard isOpen={isDashboardOpen} hide={toggleDashboard}/>

			<AddPayment isOpen={isFormOpen} hide={toggleForm}/>

			<IonReactRouter>
				<IonTabs>
					<IonRouterOutlet>
						<Route path="/home" exact={true}><Home/></Route>
						<Route path="/account" exact={true} ><Home /></Route>
						<Route path="/goals" component={Goals} />
						<Route path="/dashboard"><Home/></Route>
						<Route path="/budget" component={Budget} />
						<Route path="/transaction-history" component={TransactionHistory} />
						<Route path="/recurring-expenses" component={RecurringExpenses} />
						<Route path="/categories" component={Categories} />
						<Route path="/" render={() => <Redirect to="/home" />} exact={true} />
					</IonRouterOutlet>
					<IonTabBar slot="bottom" className="main-nav">
						<IonTabButton tab="home" href="/home">
							<IonIcon icon={homeOutline} />
						</IonTabButton>
						<IonTabButton>
							<IonIcon icon={analyticsOutline} onClick={toggleDashboard as any} />
						</IonTabButton>
						<IonTabButton className="mid-nav" />
						<IonTabButton className="btn-add-payment">
							<IonIcon icon={addOutline} onClick={toggleForm as any} />
						</IonTabButton>
						<IonTabButton tab="goals" href="/goals">
							<IonIcon icon={copyOutline} />
						</IonTabButton>
						<IonTabButton tab="budget" href="/budget">
							<IonIcon icon={walletOutline} />
						</IonTabButton>
						<IonTabButton tab="history" href="/transaction-history" className="no-display"></IonTabButton>
						<IonTabButton tab="recurring" href="/recurring-expenses" className="no-display"></IonTabButton>
						<IonTabButton tab="categories" href="/categories" className="no-display"></IonTabButton>
					</IonTabBar>
				</IonTabs>
			</IonReactRouter>
		</IonApp>
    )
}

export default Layout