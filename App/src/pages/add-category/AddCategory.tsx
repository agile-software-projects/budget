import React, { useState, useEffect } from 'react';
import { IonSelect, IonSelectOption, IonRow, IonCol, IonInput, IonButton, IonProgressBar, IonLabel } from '@ionic/react';
import validate from './validateCategoryForm';
import './AddCategory.css';
import icon_list from './icons'

const AddCategory = () => {
    const [values, setValues] = useState({
        catName: "",
        catColour: "",
        catIcon: ""
    })

    interface IErrors {
        catName?: string,
        catColour?: string,
        catIcon?: string
    }

    const [errors, setErrors] = useState<IErrors>({});

    const handleChange = (e: any) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e: any) => {
        e.preventDefault();
        setErrors(validate(values));
        setIsSubmitted(true);
    }

    const messages = () => {
        if (submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error saving the category</IonLabel>;
        } else if (submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if (submission === 2) {
            return <IonLabel color="success" className="submission-msg">Category saved</IonLabel>;
        } else {
            return null;
        }
    }


    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success

    useEffect(() => {
        if (isSubmitted && Object.keys(errors).length === 0) {
            setSubmission(1);
            fetch('https://jsonplaceholder.typicode.com/posts', {
                method: 'POST',
                mode: 'no-cors',
            })
                .then(data => setSubmission(2))
                .catch((error) => setSubmission(-1));
        } else if (Object.keys(errors).length !== 0) {
            setSubmission(-1);
        }
    },
        [isSubmitted, errors]
    );

    return (
        <div>
            <IonRow className="add-category">
                <IonCol>
                    <IonInput
                        type="text"
                        name="catName"
                        placeholder="Name"
                        className="form-input"
                        value={values.catName}
                        onIonChange={handleChange}
                    />
                </IonCol>
                <IonCol>
                    <input
                        type="color"
                        name="catColour"
                        placeholder="Colour"
                        className="form-input"
                        value={values.catColour}
                        onChange={handleChange}
                    />
                </IonCol>
                <IonCol>
                    <IonSelect value={values.catIcon} placeholder="Icon" name="catIcon" onIonChange={e => handleChange(e)}>
                        {icon_list.map((item: string, index: number) => <IonSelectOption value={item} key={index} >{item}</IonSelectOption>)}
                    </IonSelect>
                </IonCol>
                <IonCol>
                    <ion-icon name={values.catIcon}></ion-icon>
                </IonCol>
                <IonCol>
                    <IonButton expand="block" type="button" color="primary" id="btn-add-category" className="ion-margin" onClick={handleSubmit}>Add</IonButton>
                </IonCol>
            </IonRow>
            <IonRow>
                <IonCol>
                    {messages()}
                </IonCol>
            </IonRow>
        </div>
    );
};

export default AddCategory;
