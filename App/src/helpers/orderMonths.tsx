// inputs an array containing all 12 months, january to december, and the index of the current month
// ouputs a reordered array with the current month first, followed by the previous month and so on
import moment from 'moment'

const orderMonths = (currentMonthIndex: number) => {
    let c = currentMonthIndex
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    const newMonthsArr = [];
    for (let i=0; i<c+1; i++) {
        newMonthsArr.push(months[c - i])
    }
    for (let i=0; i<(12-c-1); i++) {
        newMonthsArr.push(months[11 - i])
    }
    return newMonthsArr;
}

export default orderMonths;

export const getLastMonthsRange = () => {
    const today =  moment()
    const numbers = new Array(12).fill(0)
    const months = numbers.map((n, i) => {
        const m = i === 0 ? today : today.subtract(1, 'month')
        return m.format('YYYY-MM-DD')
    })
    return months
}

export const getMonthRange = (month: string) => {
    const date = moment(month)
    const startOfMonth = date.clone().startOf('month').format('YYYY-MM-DD')
    const endOfMonth   = date.clone().endOf('month').format('YYYY-MM-DD')
    return {from: startOfMonth, to: endOfMonth}
}