const assert = require('assert');
const { v4: uuidv4 } = require('uuid');
const User = require('../classes/User');

const badData = {
    userId: uuidv4(),
    username: "testusername",
    password: "somepass",
    email: "test@test"
}

const goodData = {
    userId: uuidv4(),
    username: "testusername",
    password: "s0meG00dP@55!",
    email: "test@test.com"
}

describe("Testing the user Object", () => {
    it("Should retain the values of the user entity", (done) => {
        const user = new User(goodData);
        assert.strictEqual(user.userId.length, 36);
        assert.strictEqual(user.username, "testusername");
        assert.strictEqual(user.password, "s0meG00dP@55!");
        assert.strictEqual(user.email, "test@test.com");
        done();
    });

    it("Should have a method to validate a user and returns true for valid data", (done) => {
        const user = new User(goodData)
        assert.strictEqual(user.validate(), user);
        done();
    });

    it("Should have a method to validate a user and returns false for bad data", (done) => {
        const user = new User(badData)
        assert.strictEqual(user.validate(), false);
        done();
    });

    it("should have a method to return the whole user object", (done) => {
        const user = new User(goodData);
        assert.strictEqual(Object.keys(user.wholeUser()).length, 4);
        done();
    });

    it.skip("should insert a valid user", (done) => {
        const user = new User(goodData);
        
        setTimeout(() => {
            user.assignUserId().insertUser().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });

    it.skip("should not insert an invalid user", (done) => {
        const user = new User(badData);
        
        setTimeout(() => {
            user.assignUserId().insertUser().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(err.error, "user not validated");
                done();
            })
        }, 3000);
    });

    const id = "d30cc1c0-7932-4f38-8421-ea56cd3220eb"
    it.skip("should get the users details from a value (id)", (done) => {
        const user = new User({userId: id});
        
        setTimeout(() => {
            user.populate().then(() => {
                assert.strictEqual(user.userId, id);
                assert.strictEqual(user.username, goodData.username);
                assert.strictEqual(user.password, goodData.password);
                assert.strictEqual(user.email, goodData.email);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(err.error, "user not found");
                done();
            })
        }, 3000);    
    });

    it.skip("should get the users details from a value (username)", (done) => {
        const user = new User({username: "testusername"});
        
        setTimeout(() => {
            user.populate().then(() => {
                assert.strictEqual(user.userId, id);
                assert.strictEqual(user.username, goodData.username);
                assert.strictEqual(user.password, goodData.password);
                assert.strictEqual(user.email, goodData.email);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(err.error, "user not found");
                done();
            })
        }, 3000);    
    });

    it.skip("should get the users details from a value (email)", (done) => {
        const user = new User({email: "test@test.com"});
        
        setTimeout(() => {
            user.populate().then(() => {
                assert.strictEqual(user.userId, id);
                assert.strictEqual(user.username, goodData.username);
                assert.strictEqual(user.password, goodData.password);
                assert.strictEqual(user.email, goodData.email);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(err.error, "user not found");
                done();
            })
        }, 3000);    
    });

    it("should update a user record", (done) => {
        const user = new User(goodData);
        
        setTimeout(() => {
            user.email = "test@testing.com"
            user.validate().update().then(() => {
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    })
});
