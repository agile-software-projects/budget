import React from 'react';
import { IonRow, IonCol, IonSelect, IonSelectOption, IonIcon } from '@ionic/react';
import './CustomSelect.css';
import { addOutline, remove } from 'ionicons/icons';

const CustomSelect = (props: any) => {
    return (
        <IonRow>
            <IonCol size="11">
                <IonSelect value={props.value} placeholder={props.placeholder} name={props.name} onIonChange={e => props.handle(e)}>
                    {props.list.map((item:string, index:number) => <IonSelectOption value={item} key={index} >{item}</IonSelectOption>)}
                </IonSelect>
            </IonCol>
            <IonCol size="1" className="add-icon" onClick={e => props.handleAdd(e)}>
                {props.status ? <IonIcon icon={addOutline} /> : <IonIcon icon={remove} />}
            </IonCol>
        </IonRow>
    );
};

export default CustomSelect;
