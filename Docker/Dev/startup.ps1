$regex = '.=.'
$replacementContentsArr = @()

#Function to test if the ports are ints
Function Get-isInt
{
    Param([Int] $i)

    return $true
}

Write-Host "Please follow the prompts to begin setting up your environment. If the default values are OK then you can leave them blank" -ForegroundColor green
Write-Host ""

#Loop through the environment variables and set them. 
Get-Content .\.env | Where-Object {$_ -match $regex} | ForEach-Object {
    $procede = $false
    while(!$procede) {
        $lineItem = $_
        $envVarArr = $lineItem.Split('=')
        $label = $envVarArr[0]
        $default = $envVarArr[1]
        $notDockerComposeEnv = $true
        switch ($label) {
            "COMPOSE_PROJECT_NAME" {
                Write-Host "What would you like to name your project? " -ForegroundColor yellow -NoNewLine
                Write-Host "(lowercase with words separated by '-' and no spaces) " -ForegroundColor cyan -NoNewline
            }

            "COMPOSE_FILE" {
                $notDockerComposeEnv = $false
            }

            "APP_PORT" {
                Write-Host "Please choose a local port for the app to run on " -ForegroundColor yellow -NoNewLine
                Write-Host "(This will be ignored if you have chosen no Database container) " -ForegroundColor red -NoNewLine
            }

            "API_PORT" {
                Write-Host "Please choose a local port for the api to run on " -ForegroundColor yellow -NoNewLine
                Write-Host "(This will be ignored if you have chosen no Database container) " -ForegroundColor red -NoNewLine
            }

            "APP_DEBUG_PORT" {
                Write-Host "Please choose a local port for the app debug to attach to " -ForegroundColor yellow -NoNewLine
                Write-Host "(This will be ignored if you have chosen no app container) " -ForegroundColor red -NoNewLine
            }

            "API_DEBUG_PORT" {
                Write-Host "Please choose a local port for the api debug to attach to " -ForegroundColor yellow -NoNewLine
                Write-Host "(This will be ignored if you have chosen no api container) " -ForegroundColor red -NoNewLine
            }

            "DB_PORT" {
                Write-Host "Please choose a local port for the Database container " -ForegroundColor yellow -NoNewLine
                Write-Host "(This will be ignored if you have chosen no Database container) " -ForegroundColor red -NoNewLine
            }

            "CORE_VERSION" {
                Write-Host "Please choose your version of Core " -ForegroundColor yellow -NoNewLine
            }

            "HTTP_PORT" {
                Write-Host "Please choose a local port to be mapped to nginx https port " -ForegroundColor yellow -NoNewLine
            }

            "HTTPS_PORT" {
                Write-Host "Please choose a local port to be mapped to nginx https port " -ForegroundColor yellow -NoNewLine
            }

            "SA_PASSWORD" {
                Write-Host "Please set an SA Password for the dev database container (if used) " -ForegroundColor yellow -NoNewLine
            }
            
            Default {
                Write-Host "Please set the $label " -ForegroundColor yellow -NoNewLine
            }
        }
        if ($notDockerComposeEnv) {
            Write-Host "(the Default is $default): " -ForegroundColor white -NoNewLine
            $envVar = Read-Host
        } else {
            $notDockerComposeEnv = $true
        }

        if($envVar -eq $default -Or $envVar -eq "") {
            if (!("$label" -eq "COMPOSE_FILE")) {
                $replacementContentsArr += "$label=$default"
                $procede = $true
            } else {
                $procede = $true
            }
        } else {
            switch ($label) {
                "COMPOSE_PROJECT_NAME" {
                    $procede = $true
                    $replacementContentsArr += "$label=$envVar"
                }

                "COMPOSE_FILE" {
                    Write-Host "hit COMPOSE_FILE"
                    $procede = $true
                }

                "APP_PORT" { 
                    if(Get-isInt -i $envVar) {
                        $procede = $true
                        $replacementContentsArr += "$label=$envVar"
                    }
                }

                "API_PORT" {
                    if(Get-isInt -i $envVar) {
                        $procede = $true
                        $replacementContentsArr += "$label=$envVar"
                    }
                }

                "DB_PORT" {
                    if(Get-isInt -i $envVar) {
                        $procede = $true
                        $replacementContentsArr += "$label=$envVar"
                    }
                }

                "APP_DEBUG_PORT" {
                    if(Get-isInt -i $envVar) {
                        $procede = $true
                        $replacementContentsArr += "$label=$envVar"
                    }
                }

                "API_DEBUG_PORT" {
                    if(Get-isInt -i $envVar) {
                        $procede = $true
                        $replacementContentsArr += "$label=$envVar"
                    }
                }

                "CORE_VERSION" {
                    $procede = $true
                    $replacementContentsArr += "$label=$envVar"
                }

                "HTTP_PORT" {
                    if(Get-isInt -i $envVar){
                        $procede = $true
                        $replacementContentsArr += "$label=$envVar"
                    }
                }

                "HTTPS_PORT" {
                    if(Get-isInt -i $envVar){
                        $procede = $true
                        $replacementContentsArr += "$label=$envVar"
                    }
                }

                "SA_PASSWORD" {
                    $procede = $true
                    $replacementContentsArr += "$label=$envVar"
                }
                
                Default {
                    #do nothing. 
                }
            }
        }
    }
}

#Set the stack requirements
$app = $false
$api = $false
$database = $false

$reply = Read-Host -Prompt "Do you require an app container? [y/n]"
if ( $reply -match "[yY]" ) { 
    $app = $true
} else {
    $app = $false
}

$reply = Read-Host -Prompt "Do you require an api container? [y/n]"
if ( $reply -match "[yY]" ) { 
    $api = $true
} else {
    $api = $false
}

$reply = Read-Host -Prompt "Do you require a database container? [y/n]"
if ( $reply -match "[yY]" ) { 
    $database = $true
} else {
    $database = $false
}


if ($app) {
    if ($api) {
        if ($database) {
            Write-Host "Creating containers for an app, api and database" -ForegroundColor red
            $replacementContentsArr += "COMPOSE_FILE=docker-compose.yml"
        } else {
            Write-Host "Creating app and api containers" -ForegroundColor red
            $replacementContentsArr += "COMPOSE_FILE=docker-compose.app_api.yml"
        }
    } else {
        if ($database) {
            Write-Host "Creating app and database containers" -ForegroundColor red
            $replacementContentsArr += "COMPOSE_FILE=docker-compose.app_db.yml"
        } else {
            Write-Host "Only creating a standalone express app" -ForegroundColor red
            $replacementContentsArr += "COMPOSE_FILE=docker-compose.app.yml"
        }
    }
} else {
    if ($api) {
        if ($database) {
            Write-Host "Spinning up an api and database" -ForegroundColor red
            $replacementContentsArr += "COMPOSE_FILE=docker-compose.api_db.yml"
        } else {
            Write-Host "Only creating an api" -ForegroundColor red
            $replacementContentsArr += "COMPOSE_FILE=docker-compose.api.yml"
        }
    } else {
        if ($database) {
            Write-Host "Spinning up a database only" -ForegroundColor red
            $replacementContentsArr += "COMPOSE_FILE=docker-compose.db.yml"
        } else {
            Write-Host "You have selected no options. The stack will not start." -ForegroundColor red
        }
    }
}

if ($app -Or $api -Or $database) {
    Set-Content './.env' ""
    $path = './.env'
    $n = 0
    $replacementContentsArr | Where-Object {$_ -match $regex} | ForEach-Object {
        if($n -gt 0) {
            Set-Content -path $path -value "`r`n$_"
        } else {
            Add-Content -path $path -value "$_"
        }
    }
    
    
    #Import the certificate
    Write-Host ""
    Write-Host "Importing Certicate. This may prompt a dialogue box that likes to hide behind open applications sometimes so please check for it." -ForegroundColor green
    Write-Host "When you find it please click 'Yes' to import!" -ForegroundColor green
    Write-Host ""
    
    Import-Certificate -FilePath ".\ssl\certificate.crt" -CertStoreLocation 'Cert:\CurrentUser\Root' -Verbose
    
    Write-Host ""
    Write-Host "All done! Spinning up the container dev environment now in accordance to your settings...." -ForegroundColor yellow
    Write-Host ""
    
    #START THE CONTAINERS
    docker-compose up
}