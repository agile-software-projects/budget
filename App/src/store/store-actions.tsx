import { useContext } from "react";
import { StoreContext } from "./store";
import { Account, actions, Budget, Category, Goal, Transaction, User } from "./store-types";
import {v4 as uuid} from 'uuid'
import { convertPriceFormat } from "../helpers/price";
import {apiUrl} from "../helpers/env";

export const useStore = () => {
    const {state, dispatch} = useContext(StoreContext)

    const setLoadingApp = (data: boolean) => {
        if(data === true){
            dispatch({type: actions.app.LOADING})
        }else{
            dispatch({type: actions.app.LOADED})
        }
    }

    const setLoggedIn = (data: boolean) => {
        if(data === true){
            dispatch({type: actions.auth.LOGIN})
        }else{
            dispatch({type: actions.auth.LOGOUT})
        }
    }

    const setShowNotification = (data: any) => {
        if(data.show === true){
            setTimeout(() => {
                dispatch({type: actions.app.SHOW_NOTIFICATION, payload: data})
            }, 500)
        }else{
            dispatch({type: actions.app.HIDE_NOTIFICATION, payload: data})
        }
    }
    
    const setUser = (data: User) => {
        dispatch({type: actions.user.SET_USER, payload: data})
    }

    const setCurrency = (data: any) => {
        dispatch({type: actions.user.SET_CURRENCY, payload: data});
    }

    const setAccounts = (data: Account[]) => {
        dispatch({type: actions.accounts.SET_ACCOUNTS, payload: data})
    }

    const selectAccount = (data: Account) => {
        dispatch({type: actions.accounts.SELECT_ACCOUNT, payload: data})
    }

    const addAccount = async (data: Account) => {
        const accountId = uuid()
        const currentBalance = convertPriceFormat(data.currentBalance)
        dispatch({type: actions.accounts.ADD_ACCOUNT, payload: {...data, accountId, currentBalance}})
        setShowNotification({show: true, message: 'Account Created', color: 'secondary'})
        try{
            const res = await fetch(`${apiUrl}/account/insertAccount`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({...data, accountId, currentBalance}),
                credentials: "include"
            })
            if(!res.ok) throw Error()
        }catch(e){
            dispatch({type: actions.accounts.DELETE_ACCOUNT, payload: accountId})
            setShowNotification({show: true, message: 'Error Adding Account', color: 'danger'})
        }
    }

    const deleteAccount = async (account: Account) => {
        const accountId = account.accountId
        dispatch({type: actions.accounts.DELETE_ACCOUNT, payload: accountId})
        dispatch({type: actions.accounts.SELECT_ACCOUNT, payload: null})
        setShowNotification({show: true, message: 'Account Deleted', color: 'secondary'})
        try{
            const res = await fetch(`${apiUrl}/account/${accountId}/deleteAccount`, {
                method: 'POST',
                credentials: "include"
            })
            if(!res.ok) throw Error()

            //delete all transactions
            await fetch(`${apiUrl}/transaction/${accountId}/deleteAll`, {
                method: 'POST',
                credentials: "include"
            })
        }catch(e){
            setShowNotification({show: true, message: 'Error Deleting Account', color: 'danger'})
        }
    }

    const setAllTransactions = (allTransactions: Transaction[]) => {
        dispatch({type: actions.transactions.SET_ALL_TRANSACTIONS, payload: allTransactions})
    }

    const setAccountTransactions = (accountTransactions: Transaction[]) => {
        dispatch({type: actions.transactions.SET_ACCOUNT_TRANSACTIONS, payload: accountTransactions});
    }

    const addTransaction = async (transaction: Transaction, category: string, account: Account) => {
        const transactionId = uuid()
        const data = {
            ...transaction,
            transactionId,
        }

        const amount = transaction.amount
        account.currentBalance = transaction.transactionDirection === 'income' ? account.currentBalance+amount : account.currentBalance-amount

        dispatch({type: actions.transactions.ADD_TRANSACTION, payload: data})
        setShowNotification({show: true, message: 'Transaction Created', color: 'secondary'})

        try{
            // insert transaction in db
            const res = await fetch(`${apiUrl}/transaction/${transaction.accountId}/${category}/new`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data),
                credentials: "include"
            })
            if(!res.ok) throw Error()

            // update account balance
            await fetch(`${apiUrl}/account/${account.accountId}/updateAccount`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({currentBalance: account.currentBalance}),
                credentials: "include"
            })
        }catch(e){
            dispatch({type: actions.transactions.DELETE_TRANSACTION, payload: transactionId})
            setShowNotification({show: true, message: 'Error Adding Transaction', color: 'danger'})
        }
    }

    const saveTransaction = async (transaction: Transaction, categoryId: any, account: Account, oldPrice: number) => {
        dispatch({type: actions.transactions.SAVE_TRANSACTION, payload: transaction})
        setShowNotification({show: true, message: 'Transaction Saved', color: 'secondary'})

        if(oldPrice !== transaction.amount){
            const amount = transaction.amount - oldPrice
            account.currentBalance = transaction.transactionDirection === 'income' ? account.currentBalance+amount : account.currentBalance-amount
            dispatch({type: actions.accounts.SAVE_ACCOUNT, payload: account})
        }
        
        try{
            const res = await fetch(`${apiUrl}/transaction/${transaction.transactionId}/edit`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({...transaction, categoryId}),
                credentials: "include"
            })
            if(!res.ok) throw Error()

            // update account balance
            if(oldPrice !== transaction.amount){
                await fetch(`${apiUrl}/account/${account.accountId}/updateAccount`, {
                    method: 'POST',
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({currentBalance: account.currentBalance}),
                    credentials: "include"
                })
            }
        }catch(e){
            dispatch({type: actions.transactions.DELETE_TRANSACTION, payload: transaction.transactionId})
            setShowNotification({show: true, message: 'Error Saving Transaction', color: 'danger'})
        }
    }

    const deleteTransaction = async (transaction: Transaction, account: Account) => {
        const amount = transaction.amount
        account.currentBalance = transaction.transactionDirection === 'income' ? account.currentBalance-amount : account.currentBalance+amount

        dispatch({type: actions.transactions.DELETE_TRANSACTION, payload: transaction.transactionId})
        setShowNotification({show: true, message: 'Transaction Deleted', color: 'secondary'})

        try{
            const res = await fetch(`${apiUrl}/transaction/${transaction.transactionId}/delete`, {
                method: "post",
                credentials: "include"
            })
            if(!res.ok) throw Error()

            // update account balance
            await fetch(`${apiUrl}/account/${account.accountId}/updateAccount`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({currentBalance: account.currentBalance}),
                credentials: "include"
            })
        }catch(e){
            dispatch({type: actions.transactions.ADD_TRANSACTION, payload: transaction})
            setShowNotification({show: true, message: 'Error Deleting Transaction', color: 'danger'})
        }
    }

    const selectTransaction = (data: any) => {
        dispatch({type: actions.transactions.SET_SINGLE_TRANSACTION, payload: data})
    }

    const setCategories = (data: Category[]) => {
        dispatch({type: actions.categories.SET_CATEGORIES, payload: data})
    }

    const setGoals = (data: Goal[]) => {
        dispatch({type: actions.goals.SET_GOALS, payload: data})
    }

    const selectGoal = (data: Goal) => {
        dispatch({type: actions.goals.SELECT_GOAL, payload: data})
    }

    const addGoal = async (data: any) => {
        const goalId = uuid()
        dispatch({type: actions.goals.ADD_GOAL, payload: {...data, goalId}})
        setShowNotification({show: true, message: 'Goal Added', color: 'secondary'})

        const formValues = {
            goalId,
            "goalName": data.goalName,
            "goalAmount": data.goalAmount,
            "active": data.active,
            "targetDate" : data.targetDate,
            "creationDate": new Date(),
            "currentAmount" : data.currentAmount
        }

        try{
            const res = await fetch(`${apiUrl}/goal/${data.accountId}/new`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                credentials: "include",
                body: JSON.stringify(formValues)
            })
            if(!res.ok) throw Error()
        }catch(e){
            console.error(e)
            dispatch({type: actions.goals.DELETE_GOAL, payload: goalId})
            setShowNotification({show: true, message: 'Error Adding Goal', color: 'danger'})
        }
    }

    const saveGoal = async (data: any) => {
        dispatch({type: actions.goals.SAVE_GOAL, payload: data})
        setShowNotification({show: true, message: 'Goal Saved', color: 'secondary'})

        // make api request...
        try{
            const res = await fetch(`${apiUrl}/goal/goalId/${data.goalId}/updateGoal`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data),
                credentials: "include"
            })
            if(!res.ok) throw Error()
        }catch(e){
            dispatch({type: actions.goals.DELETE_GOAL, payload: data.goalId})
            setShowNotification({show: true, message: 'Error Saving Goal', color: 'danger'})
        }
    }

    const deleteGoal = async (data: any) => {

        dispatch({type: actions.goals.DELETE_GOAL, payload: data.goalId})
        setShowNotification({show: true, message: 'Goal Deleted', color: 'secondary'})
        try{
            const res = await fetch(`${apiUrl}/goal/goalId/${data.goalId}/deleteGoal`, {
				method: 'POST',
			    credentials: 'include'
			})
            if(!res.ok) throw Error()
        }catch(e){
            console.error(e)
            dispatch({type: actions.goals.ADD_GOAL, payload: data})
            setShowNotification({show: true, message: 'Error Deleting Goal', color: 'danger'})
        }
    }

    const setAccountGoals = (data: Goal[]) => {
        dispatch({type: actions.goals.SET_ACCOUNT_GOALS, payload: data});
    }

    const addSavings = async(data: any, account: Account, transaction: Transaction) => {
        try {
            const transactionId = uuid()
            const trans = {
                ...transaction,
                transactionId,
            }
        
            dispatch({type: actions.transactions.ADD_TRANSACTION, payload: trans})
            setShowNotification({show: true, message: 'Transaction Created', color: 'secondary'})
    
            account.currentBalance = transaction.transactionDirection === 'savingsOut' ? account.currentBalance + data.amount : account.currentBalance - data.amount;
            dispatch({type: actions.accounts.SAVE_ACCOUNT, payload: account});
            
            await fetch(`${apiUrl}/goal/${transactionId}/addSavings`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data),
                credentials: "include"
            });

            await fetch(`${apiUrl}/account/${account.accountId}/updateAccount`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({currentBalance: account.currentBalance}),
                credentials: "include"
            })

            const goal = state.goals.filter((g:any) => g.goalId === data.goalId)[0];
            goal.currentAmount =  transaction.transactionDirection === 'savingsIn' ? goal.currentAmount + data.amount : goal.currentAmount - data.amount
            await fetch(`${apiUrl}/goal/currentAmount/${data.goalId}/updateGoal`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(goal),
                credentials: "include"
            });
            dispatch({type: actions.goals.SAVE_GOAL, payload: goal})

            dispatch({type: actions.goals.ADD_SAVINGS, payload: data})
            setShowNotification({show: true, message: 'Savings recorded', color: 'secondary'});
        } catch(err) {
            console.log(err);
            setShowNotification({show: true, message: 'Error recording savings', color: 'danger'});
        }
    }

    const selectBudget = (data: any) => {
        dispatch({type: actions.budget.SELECT_BUDGET, payload: data})
    }

    const setBudgets = (data: Budget[]) => {
        dispatch({type: actions.budget.SET_BUDGETS, payload: data})
    }

    const addBudget = async (data: any) => {
        const budgetId = uuid()
        dispatch({type: actions.budget.ADD_BUDGET, payload: {...data, budgetId}})
        setShowNotification({show: true, message: 'Budget Added', color: 'secondary'})

        // build the data to pass to the endpoint

        const formValues = {
            budgetId: data.budgetId,
            budgetName: data.budgetName,
            totalLimit: data.totalLimit,
            endDate: data.endDate,
            active: 1,
            currentAmount: data.currentAmount,
            accountId: data.accountId,
            categoryName: data.categoryName,
            resetPeriod: data.resetPeriod
        }

        try{
            const res = await fetch(`${apiUrl}/budget/${data.accountId}/new`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                credentials: "include",
                body: JSON.stringify(formValues)
            })
            if(!res.ok) throw Error()
        }catch(e){
            console.error(e)
            dispatch({type: actions.budget.DELETE_BUDGET, payload: budgetId})
            setShowNotification({show: true, message: 'Error Adding Budget', color: 'danger'})
        }
    }

    const saveBudget = async (data: any) => {

        dispatch({type: actions.budget.SAVE_BUDGET, payload: data})
        setShowNotification({show: true, message: 'Budget Saved', color: 'secondary'})

        // make api request...
        try{
            const res = await fetch(`${apiUrl}/budget/${data.budgetId}/update`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(data),
                credentials: "include"
            })
            if(!res.ok) throw Error()
        }catch(e){
            dispatch({type: actions.budget.SAVE_BUDGET, payload: data.budgetId})
            setShowNotification({show: true, message: 'Error Saving Budget', color: 'danger'})
        }

    }

    const deleteBudget = async (data: any) => {
        dispatch({type: actions.budget.DELETE_BUDGET, payload: data.budgetId})
        setShowNotification({show: true, message: 'Budget Deleted', color: 'secondary'});
        try{
            const res = await fetch(`${apiUrl}/budget/${data.budgetId}/delete`, {
				method: 'POST',
			    credentials: 'include'
			})
            if(!res.ok) throw Error()
        }catch(e){
            console.error(e)
            dispatch({type: actions.budget.ADD_BUDGET, payload: data})
            setShowNotification({show: true, message: 'Error Deleting Budget', color: 'danger'})
        }
    }

    return {
        store: state,
        loadingApp: state.loadingApp,
        loggedIn: state.loggedIn,
        showNotification: state.showNotification,
        user: state.user as User,
        accounts: state.accounts as Account[],
        allTransactions: state.allTransactions as Transaction[],
        selectedAccount: state.selectedAccount as Account,
        categories: state.categories as Category[],
        goals: state.goals as Goal[],
        selectedGoal: state.selectedGoal as Goal,
        budgets: state.budgets as Budget[],
        selectedBudget: state.selectedBudget as Budget,
        selectedTransaction: state.selectedTransaction as Transaction,
        currency: state.currency as any,
        accountTransactions: state.accountTransactions as Transaction[],
        accountGoals: state.accountGoals as Goal[],
        
        addSavings,
        setAccountGoals,
        setLoadingApp,
        setLoggedIn,
        setShowNotification,
        setUser, 
        setAccounts,
        selectAccount,
        addAccount,
        deleteAccount,
        setAllTransactions,
        selectTransaction,
        addTransaction,
        saveTransaction,
        deleteTransaction,
        setCategories,
        setGoals,
        selectGoal,
        addGoal,
        saveGoal,
        deleteGoal,
        setBudgets,
        selectBudget,
        addBudget,
        deleteBudget,
        saveBudget,
        setCurrency,
        setAccountTransactions
    }
}

