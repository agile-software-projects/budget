const { v4: uuidv4 } = require('uuid');
const databaseFunctions = require('../core/databaseFunctions');
const bcrypt = require('bcrypt');
const env = require('../scripts/env');
const winston = require('../core/winston');
const Budget = require("./Budget");
const Account = require("./Account");
const Category = require('./Category');

class User {
    constructor(user) {
        this.userId = user.userId;
        this.username = user.username;
        this.password = user.password;
        this.email = user.email;
        this.active = user.active
    }

    wholeUser() {
        return {
            userId: this.userId,
            username: this.username,
            password: this.password,
            email: this.email,
            active: this.active
        }
    }

    assignUserId() {
        this.userId = uuidv4();
        return this;
    }

    validate() {
        //user id length === 36 (as is uuid)
        if (!(this.userId.length === 36)) { throw(new Error("The userId is not valid")); }

        //email
        const emailRe = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!(emailRe.test(this.email))) { throw(new Error("The email address is not valid")); }

        //username
        const usernameRe = /^(?=[a-zA-Z0-9._]{4,32}$)(?!.*[_.]{2})[^_.].*[^_.]$/;
        if (!(usernameRe.test(this.username))) { throw(new Error("The username is not valid")); }

        return this;
    }

    insertUser() {
        return new Promise((resolve, reject) => {
            if (this.validate()) {
                //first check user does not exist
                const promiseArr = [];
                promiseArr.push(databaseFunctions.selectTable("users", {userId: this.userId}));
                promiseArr.push(databaseFunctions.selectTable("users", {username: this.username}));
                promiseArr.push(databaseFunctions.selectTable("users", {email: this.email}));
                Promise.all(promiseArr).then((results) => {
                    //this means the user exists and cannot be inserted. we should reject with the field that matches
                    if (results[0].length !== 0) { reject({error: "a user with this id already exists"}) }
                    if (results[1].length !== 0) { reject({error: "the username already exists"}) }
                    if (results[2].length !== 0) { reject({error: "the email already exists"}) } 
                    //the user does not exist and can be inserted
                    if ((results[0].length === 0) && (results[1].length === 0) && (results[2].length === 0)) {
                        databaseFunctions.insertTableRow("users", this.wholeUser()).then((rowsAffected) => {
                            if (rowsAffected[0] === 1) {
                                resolve({success: true});
                            } else {
                                reject({error: "there was an error inserting"});
                            }
                        }).catch((err) => {
                            reject({error: err});
                        })
                    } else {
                        reject({error: "The user exists already"});
                    }
                }).catch((err) => {
                    reject({error: err});
                });
            } else {
                reject({error: "user not validated"});
            }
        });
    }

    populate() {
        //this will populate the user based on a userId
        return new Promise((resolve, reject) => {
            const promiseArr = [];
            promiseArr.push(databaseFunctions.selectTable("users", {userId: this.userId}));
            promiseArr.push(databaseFunctions.selectTable("users", {email: this.email}));
            promiseArr.push(databaseFunctions.selectTable("users", {username: this.username}));
            Promise.all(promiseArr).then((results) => {
                let foundUser = {};
                //match priority userId > email > username
                if (results[2].length === 1) { foundUser = results[2][0] }
                if (results[1].length === 1) { foundUser = results[1][0] }
                if (results[0].length === 1) { foundUser = results[0][0] }
                if (Object.keys(foundUser).length > 0) {
                    this.userId = foundUser.userId;
                    this.username = foundUser.username;
                    this.password = foundUser.password;
                    this.email = foundUser.email;
                    resolve(true);
                } else {
                    reject({error: "no user found"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    update() {
        return new Promise((resolve, reject) => {
            databaseFunctions.updateTableRow("users", {userId: this.userId}, this.wholeUser()).then((rowsAffected) => {
                if (rowsAffected === 1) {
                    resolve(true);
                } else {
                    reject({error: "something went wrong updating the user record"})
                }
            }).catch((err) => {
                reject(err);
            })
        });
    }

    hashPassowrd() {
        return new Promise((resolve, reject) => {
            bcrypt.hash(this.password, env.bcrypt.salt).then((hash) => {
                this.password = hash;
                resolve({success: true});
            }).catch((err) => {
                winston.error(`error hashing password: ${err}`);
                reject({error: err});
            });
        })
    }

    comparePassword(pwd) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(pwd, this.password).then((result) => {
                if(result) {
                    resolve({success: true});
                } else {
                    reject({error: "could not authenticate"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    getAllUserBudgets() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("userBudgetsView", {userId: this.userId}).then((budgets) => {
                if (budgets.length > 0) {
                    resolve(budgets);
                } else {
                    resolve([]);
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    getAllAccounts() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("account", {userId: this.userId}).then((accounts) => {
                if (accounts.length > 0) {
                    resolve(accounts);
                } else {
                    reject({error: "user has no associated accounts"});
                }
            }).catch((err) => {
                reject(err);
            })
        });
    }

    markActive(active) {
        return new Promise((resolve, reject) => {
            databaseFunctions.updateTableRow("users", {userId: this.userId}, {active: active}).then((rowsModified) => {
                if (rowsAffected === 1) {
                    resolve(true);
                } else {
                    reject({error: "something went wrong updating the user record"})
                }  
            }).catch((err) => {
                resolve({error: "Unable to update active status"});
            })
        })
    }

    getAllUserGoals() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("userGoalsView", {userId: this.userId}).then((goals) => {
                if (goals.length > 0) {
                    resolve(goals);
                } else {
                    resolve([]);
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    setupDefaults() {
        return new Promise((resolve, reject) => {
            //user needs an account
            try {
                const account = new Account({
                    userId: this.userId,
                    accountType: "default",
                    creationDate: new Date(),
                    accountEnabled: true,
                    currentBalance: 0,
                    lastUpdated: new Date(),
                    accountName: "my-default-account"
                });
                account.assignAccountId().validate().insertAccount().then(() => {
                    //account needs a buget
                    const endDate = new Date();
                    endDate.setDate(endDate.getDate() + 30);
                    const budget = new Budget({
                        budgetName: "default",
                        totalLimit: 99900,
                        accountId: account.accountId,
                        active: true,
                        resetPeriod: "30d",
                        currentPeriod: uuidv4(),
                        startDate: new Date(),
                        endDate: endDate
                    });
                    budget.assignBudgetId().validate().insertBudget().then(() => {
                        //budget needs default cats
                        databaseFunctions.selectTable("category", {isDefault: true}).then((categories) => {
                            const defaultLinks = [];
                            categories.forEach((cat) => {
                                const category = new Category(cat);
                                defaultLinks.push(category.linkToBudget(budget.budgetId));
                            });
                            Promise.all(defaultLinks).then(() => {
                                resolve();
                            }).catch((err) => {
                                winston.debug("in (promise)catch in User class setupDefaults");
                                winston.debug("in promise.all adding budget cat links");
                                winston.error(err);
                                reject(err);
                            });
                        }).catch((err) => {
                            winston.debug("in dbfunction catch inserting budget link from promise arr");
                            winston.error(err);
                            reject(err);
                        });
                    }).catch((err) => {
                        winston.debug("in (promise)catch in User class setupDefaults");
                        winston.debug("in either assign budgetId, validate or insertBudget");
                        winston.error(err);
                        reject(err);
                    });
                }).catch((err) => {
                    winston.debug("in (promise)catch in User class setupDefaults");
                    winston.debug("error threw either assign accountID, validate or insertAccount");
                    winston.error(JSON.stringify(err));
                    winston.error(err.message);
                    console.log(err);
                    reject(err);
                });
            } catch(err) {
                winston.debug("in (try)catch block in User class setupDefaults")
                winston.error(err.message);
                reject({error: err.message});
            }
        });
    };
}

module.exports = User;