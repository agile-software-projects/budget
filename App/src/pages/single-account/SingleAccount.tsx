import React, { useState, useEffect, useRef } from 'react';
import { IonCard, IonCardHeader, IonModal, IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonLabel, IonList, IonItem, IonNote, IonProgressBar, IonPopover, IonCardTitle } from '@ionic/react';
import { ellipsisVertical, chevronBack, trashOutline, createOutline } from 'ionicons/icons';
import './SingleAccount.css';
import { useStore } from '../../store/store-actions';
import {sum} from 'lodash'
import { priceFormat } from '../../helpers/price';
import EditTransaction from "../edit-transaction/EditTransaction";

const SingleAccount = (props: any) => {
    const { isOpen, hide } = props
    const {selectedAccount, deleteAccount, allTransactions, deleteTransaction, selectTransaction, currency, accountTransactions, setAccountTransactions} = useStore()
    const [popoverState, setShowPopover] = useState({ showPopover: false, event: undefined });
    const [isEditTransactionOpen, setIsEditTransactionOpen] = useState(false);
    
    const getAccountTransactions = () => {
        return allTransactions.filter(t => t.accountId === selectedAccount?.accountId)
    }

    
    const getToalExpense = () => {
        return priceFormat(sum(getAccountTransactions().filter(t => t.transactionDirection === 'expense').map(t => t.amount)))
    }

    const getToalIncome = () => {
        return priceFormat(sum(getAccountTransactions().filter(t => t.transactionDirection === 'income').map(t => t.amount)));
    }

    const progressVal = () => {
        const expense = getToalExpense()
        const income = getToalIncome()
        if(income > expense){
            return {income: 1, expense: 1/(expense===0?1:expense)} //because we cant divide by zero
        }
        return {income: 1/(income===0?1:income), expense: 1}
    }

    const handleCTransactionDelete = (transaction: any) => {
        deleteTransaction(transaction, selectedAccount).then(() => {
            setAccountTransactions(accountTransactions.filter((trans) => trans.transactionId !== transaction.transactionId)); 
        });
    }

    const handleOpenEditTransaction = (transaction: any) => {
        selectTransaction(transaction);
        setIsEditTransactionOpen(true);
    }

    const isInitialMount = useRef(true);
    useEffect(() => {
        if (isInitialMount.current) {
            isInitialMount.current = false;
        } else {
            if (selectedAccount) {
                setAccountTransactions(allTransactions.filter((trans: any) => trans.accountId === selectedAccount.accountId));
            }
        }
    }, [allTransactions])
    
	return (
		<IonModal isOpen={isOpen}>
            <IonContent fullscreen color="primary">
                <IonHeader className="modal-header">
                    <IonToolbar className="top-bar" >
                        <IonButtons slot="start">
                            <IonButton onClick={() => {hide(!isOpen)}}>
                                <IonIcon slot="icon-only" icon={chevronBack} color="light"/>
                            </IonButton>
                        </IonButtons>
                        <IonTitle className="account-page-title" color="light">{selectedAccount?.accountName}</IonTitle>
                        <IonButtons slot="end">
                            <IonButton onClick={(e: any) => {
                                e.persist();
                                setShowPopover({ showPopover: true, event: e })
                            }}>
                                <IonIcon slot="icon-only" icon={ellipsisVertical} color="light"/>
                            </IonButton>
                        </IonButtons>
                        <IonPopover event={popoverState.event} isOpen={popoverState.showPopover} onDidDismiss={() => setShowPopover({ showPopover: false, event: undefined })}>
                            <div 
                            className="popover-item" 
                            onClick={() => {
                                hide(false);
                                deleteAccount(selectedAccount);
                             }}
                            >Delete Account</div>
                        </IonPopover>
                    </IonToolbar>
                </IonHeader>
                <div className="account-balance">{currency}{selectedAccount ? priceFormat(selectedAccount.currentBalance) : 0}</div>
                <div className="account-pnl-div">
                    {/* <div color="light" className="overview-title">Overview</div> */}
                    <IonItem color="primary" lines="none" className="ion-no-padding ion-margin progress-section">
                        <IonLabel className="payment-label" color="medium">
                            <div className="progress-labels">
                                <span className="progress-def">Income</span>
                                <span className="progress-value">{getToalIncome()}</span>
                            </div>
                            <IonProgressBar value={progressVal().income} color="green"></IonProgressBar>
                        </IonLabel>
                    </IonItem>
                    <IonItem color="primary" lines="none" className="ion-no-padding ion-margin progress-section">
                        <IonLabel className="payment-label" color="medium">
                            <div className="progress-labels">
                                <span className="progress-def">Expense</span>
                                <span className="progress-value">{getToalExpense()}</span>
                            </div> 
                            <IonProgressBar value={progressVal().expense} color="tertiary"></IonProgressBar>
                        </IonLabel>
                    </IonItem>
                </div>
                <IonModal isOpen={isEditTransactionOpen}>
                    <EditTransaction
                        hide={setIsEditTransactionOpen}
                    />
                </IonModal>
                <div className="account-payment-summary">
                    <div className="payment-summary-title">Payouts</div>
                    <IonList>
                        {accountTransactions.map((transaction, i) => 
                            <div key={i}>
                                <IonCard className="card-transaction">
                                    <IonCardHeader className="transaction-header">
                                        <IonCardTitle className="transaction-title">{transaction.transactionDescription}</IonCardTitle>
                                        {
                                            ((transaction.transactionDirection === "income")||(transaction.transactionDirection === "expense")) ?
                                            <div>
                                                <IonButton className="trashIcon" fill="clear" onClick={() => {handleCTransactionDelete(transaction)}}>
                                                    <IonIcon slot="icon-only" icon={trashOutline} color="dark" className="trashBackground"/>
                                                </IonButton>
                                                <IonButton className="editIcon" fill="clear" onClick={() => handleOpenEditTransaction(transaction)}>
                                                    <IonIcon slot="icon-only" icon={createOutline} color="dark"  className="trashBackground"/>
                                                </IonButton>
                                            </div>
                                            :""
                                        }
                                    </IonCardHeader>
                                    <IonItem lines="none">
                                        <IonLabel className="payment-label" color="medium">{((transaction.transactionDirection === 'expense')||(transaction.transactionDirection === 'savingsIn')) ? 'Expense' : 'Income'}</IonLabel>
                                        <IonNote slot="end" className="payment-value" color={((transaction.transactionDirection === 'expense')||(transaction.transactionDirection === 'savingsIn')) ? 'danger' : 'secondary'}>{((transaction.transactionDirection === 'expense')||(transaction.transactionDirection === 'savingsIn')) ? '-' : '+'} {currency}{priceFormat(transaction.amount).toFixed(2)}</IonNote>
                                    </IonItem>
                                </IonCard>
                            </div>
                        )}
                    </IonList>
                </div>
            </IonContent>
        </IonModal>
	);
};

export default SingleAccount;