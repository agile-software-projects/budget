export default function validateGoalForm(values: any, isBudgetForm: boolean) {
    
    let errors: any = {};

    const inFuture = (date: Date) => {
        let targetDate = new Date(date);
        return targetDate.setHours(0,0,0,0) >= new Date().setHours(0,0,0,0)
    };

    if (!values.description.trim()) {
        errors.description = "Description required";
    } else if (!/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/.test(values.description)) {
        errors.description = "Only letters and numbers allowed";
    }

    if (!values.targetAmount.trim()) {
        errors.targetAmount = "Target amount required";
    } else if (!/^[1-9]\d*(\.\d+)?$/.test(values.targetAmount)) {
        errors.targetAmount = "Amount should be numbers only";
    }

    if (!values.currentAmount.trim()) {
        errors.currentAmount = "Current amount required";
    } else if (!/^[0-9]\d*(\.\d+)?$/.test(values.currentAmount)) {
        errors.currentAmount = "Amount should be numbers only";
    }

    if (!values.targetDate.trim()) {
        errors.targetDate = "Target date required";
    } else if (!inFuture(values.targetDate)) {
        errors.targetDate = "Target date cannot be before current date";
    }

    if (isBudgetForm) {
        if (!values.category.trim()) {
            errors.category = "Category required";
        }
    }
    
    return errors;
}