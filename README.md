# **Express Base app framework**

This framework consists of 3 parts within this repository, but it needs four as it is designed to be run in a container environemnt. The fourth component is defined in the docker-compose file and can be pulled from docker hub. The three components in this repository are an Api express server and an App express framework. 

---

## To effectively work with this repository please have Docker installed and Linux contaners switched on...

---

## In order to develop in this environment please follow the following steps

1. Clone the repository
2. In the ./Docker/Dev folder create a file named *.env*. Fill this with the contents listed below.
3. In the Api *scripts* folder create a file called env.js. Fill these with the contents listed below and ammend as necessary. 
4. In a command line temrinal navigate to the /docker/Dev folder
5. Run the powershell script *startup.ps1*. This will be a nice easy guided setup where you can select you ports and select the core version. (Minimum version required currently 4.1.0-react)
6. Ammend your host file *(on windows its in /Windows/System32/drivers/etc)*. Add the line ```127.0.0.1   webprojects-dev.co.uk```
7. Once the containers are spun up, go to https://webprojects-dev.co.uk and start developing.

*Please note: The sstartup script is a powershell script for windows. If you wish for a similar experience on a mac, then buy a Windows pc. If you would like a similar script on any other non apple OS with a Linux kernal, it will come in a future version.*

*In the mean time, to ammend the ports on a non-windows computer, just change the port numbers in the .env file.*

*To create the containers run **docker-compose up***

## .env file contents

```
COMPOSE_PROJECT_NAME=testing
APP_PORT=3000
API_PORT=4000
DB_PORT=5000
API_DEBUG_PORT=9928
CORE_VERSION=4.1.0-react
HTTP_PORT=80
HTTPS_PORT=443
SA_PASSWORD=DOsql2005Docker!
COMPOSE_FILE=docker-compose.yml
```

---

Once the script (or docker-compose up) is run, it will create up to **5** containers based on your selection running the startup script:

1. dev_api
2. dev_app
3. dev_database
4. dev_core
5. dev_server

The volumes are mounted so you can develop on the host and *nodemon* will propogate the changes through to the container environment restarting the app scripts on save of any **.js** files. 

*Please note that the core folder contents are currently not visible on the docker host environment. A workaround for this is to use VS Code with the remote development extension and remote into the containers individually. You will then be able to view the scripts in the core folder in read-only mode. All other files are free to edit.*

---
## Additional Setup

In the api script folder please include a env.js file to hold your project specific environment variables. This can be required in the scripts you need to access them in: ```const env = require('./scripts/env');```

The file should have the following as standard:
```
module.exports = {
    port: 3000, //leave this alone
    devDatabase: {
        enabled: false,
        username: "sa",
        password: "DOsql2005Docker!",
        server: "database",
        database: "ExampleDB" //if you create a new database change this
    },
    session: {
        secret: "randomstringhere",
        maxage: 1000 * 60 * 60 * 24 
    },
    winston: {
        debugLevel: 'debug'
    }
    
}
```

---
## App

This is the main app designed to be the front end of any developed product.

This is purely a UI component and is using the React framework

---

## Api

This server is designed to handle any back end logic for the application. This keeps your business logic seperate from your front end and helps to modularise your code base. There is no view engine for this as it is not intended to render any of the front end components.  

## Database

This stack has the option to include an mssql express database container. The database will spin up with an example database but you can use a program such as Sql Server Management Studio to add a new database and tables.

The configuration file for the database is in the folder *./Api/scripts/db.js*. If you wish to use the database connection on the App then please copy the file across to the App's *scripts* folder as when running the container will not have access to the Api's *scripts* folder. 

---

## Core

The Core container contains all the core app scripts and managed seperately. It is desined like this so if there are any major fixes that would need to be applied to n number of seperate apps, it is just one codebase to be modified and containers using these can be rebuilt. 

The core app contains the following:
**winston**
Winston is a logging script that has different levels of debugging. These are:

1. error
2. warn
3. info
4. http
5. websocket
6. sql
7. debug

You can require winston in any script by running ```const winston = require('./core/winston')```. The format for any console logs will be:

 ```
 2020-01-01 00:12:59[debug_level]: console log message
 ```

The logging levels are colour coded and can be changed in the env.js file you created in the scripts folder on each app.

Winston will also keep a history of your logs in the logs folder.

**databaseFunctions**
This contains some generic database functions for mssql using the tedious driver. If you use a different database to mssql, you should include your own functions in the scripts folder. You can require this in any script with the follwoing ```const databaseFunctions = require('./core/databaseFunctions');```

**If you find you have to do complicated join operations it is reccomended you do these as sql views and use the *selectTable* function to call them.**

The functions you can access in the scripts folder include are as follows:

**SELECT TABLE**
```
//The operand value can be AND or OR. Its an optional value that defaults to AND
databaseFunctions.selectTable("tablename", {tableField: "filterValue"}, operand).then((results) => {
    /*this will return the results in an array. */
}).catch((err) => {
    /*handle any errors */
});
```

**INSERT TABLE ROW**
```
databaseFunctions.insertTableRow("tablename", {tableField: "filterValue"}).then((rowsAffected) => {
    /*this will return the rowsAffected for your confirmation. */
}).catch((err) => {
    /*handle any errors */
});
```

**UPDATE TABLE ROW**
```
/*The first object in the parameters is to set filters, the second is the object to be updated. 
!!IMPORTANT!! The "DESTROY" paramer is optional and required only if you are running an update changing multiple rows without a filter. If there is no filter object and no "DESTROY" command the update will fail*/ 
databaseFunctions.updateTableRow("tablename", {oldTableField: "filterValue"}, {tableField: "newValue", "DESTROY"}).then((rowsAffected) => {
    /*this will return the rowsAffected for your confirmation. */
}).catch((err) => {
    /*handle any errors */
});
```

**EXECUTE STORED PROCEDURE**
```
databaseFunctions.executeStoredProcedure("storedProcedureName", {paramName: "paramValue"}).then((results) => {
    /*this will return the results in an array. */
}).catch((err) => {
    /*handle any errors */
});
```

**DELETE TABLE ROW**
```
//The operand is optional and defaults to AND when not set
databaseFunctions.deleteTableRows("tablename", {tableField: "filterValue"}, operand).then((rowsAffected) => {
    /*this will return the rowsAffected for your confirmation */
}).catch((err) => {
    /*handle any errors */
});
```
---
# Test Driven Development

The container stacks have some node modules preinstalled. This includes **mocha**, **chai** and **chai-http** to enable you to follow a Test Driven Development pattern. 

To create unit tests please follow the examples in the App and Api's *test* folders. 

**To run a test script please do the following:**
1. Open a console and run the command ```docker exec -it {container_name} sh``` replacing *{container_name}* with the name of your container.
2. You are now inside the container and able to run commands. navigate to your test folder by running the command ```cd /usr/src/node/app/tests``` or ```cd /usr/src/node/api/tests``` depending on the container the tests are written for.
3. Run your tests by running the command ```npm test ./{test_filename}``` where *{test_filename}* is the name of your test script including the .js extension.

---
# Debugging

When using this stack in combination with VS Code, the configuration to run a debugger has already been included. You can attach a debugger using native Debug module and add breakpoints in the App and Api scripts. 

While running the step debugger you have the ability to see the value of variables at any point in the programs execution and can go into 3rd party libraries code. 