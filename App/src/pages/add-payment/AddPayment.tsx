import React, { useState, useEffect } from 'react';
import { IonModal, IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonLabel, IonToggle, IonInput, IonDatetime, IonSelect, IonSelectOption, IonGrid, IonRow, IonCol, IonProgressBar, IonItem } from '@ionic/react';
import AddCategory from '../add-category/AddCategory';
import Toggle from '../../ui-compontent/toggle/Toggle';
import CustomSelect from '../../ui-compontent/custom-select/CustomSelect';
import { closeOutline } from 'ionicons/icons';
import './AddPayment.css';
import validate from './validatePaymentForm';
import { useStore } from '../../store/store-actions';
import { convertPriceFormat } from '../../helpers/price';

const AddPayment = (props: any) => {
    const units = ["d", "w", "m", "y"]

    const { isOpen, hide } = props
    
    interface IErrors {
        accountId?: string,
        descr?: string,
        transDate?: string
        amount?: number,
        category?: string,
        freqNr?: number,
        freqUnit?: string,
        goalId?: string
    }

    const [values, setValues] = useState({
        accountId: "",
        descr: "",
        transDate: "",
        amount: null,
        category: "",
        freqNr: 1,
        freqUnit: "Days",
        goalId: ""
    });

    const [errors, setErrors] = useState<IErrors>({} as any);

    const [expense, setExpense] = useState<boolean>(true) //init on expense
    const [recurring, setRecurring] = useState<boolean>(false)

    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success
    const {categories, addTransaction, accounts, selectedAccount, selectAccount, currency, goals, accountGoals, setAccountGoals, addSavings} = useStore();
    const handleChange = (e: any) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e: any) => {
        setErrors(validate(values, isSavings));
        setIsSubmitted(true);
    }

    const selectAccountGoals = (value: any) => {
        setValues({
            ...values,
            goalId: value
        });
    }

    useEffect(() => {
        // const userContext = [];
        // userContext.push(idb.getRecordFromId("clientUser", 1));
        // userContext.push(idb.loadFromDb("accounts"));
        // Promise.all(userContext).then((results) => {
            // const userId = results[0].userId;
            // const accounts = results[1]
            if (isSubmitted && Object.keys(errors).length === 0) {
                setSubmission(1)
                try{
                    //const account = accounts[0]
                    const data: any = {
                        accountId: values.accountId,
                        transactionDescription: values.descr,
                        transactionDate: values.transDate,
                        transactionDirection: expense ? "expense" : "income",
                        amount: convertPriceFormat(values.amount),
                        recurring: recurring,
                        recurrencePeriod: `${values.freqNr}${values.freqUnit}`,
                    }
                    if (isSavings) {
                        const transaction = data;
                        data.goalId = values.goalId;
                        data.transactionDirection = expense ? "savingsOut" : "savingsIn";
                        addSavings(data, selectedAccount, transaction);
                    } else {
                        addTransaction(data, values.category, selectedAccount);
                    }
                    
                    setSubmission(2);
                    setValues({
                        ...values,
                        descr: '',
                        transDate: '',
                        amount: null,
                        freqNr: 0,
                        freqUnit: ''
                    });
                    setRecurring(false);
                    props.hide();

                    setTimeout(() => setSubmission(0), 1000)
                }catch(e){
                    console.log(e);
                    setSubmission(-1);
                }
                
            } else if (Object.keys(errors).length !== 0) {
                setSubmission(-1);
            }
        // }).catch((err) => {
        //     console.log(err);
        //     setSubmission(-1);

        // });
    }, [isSubmitted, errors])

    const messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error saving the transaction</IonLabel>;
        } else if(submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Transaction saved</IonLabel>;
        } else {
            return null;
        }
    }

    const [showAddCategory, setShowAddCategory] = useState(false)
    const [isSavings, setIsSavings] = useState(false);
    const toggleAdd = (e:any) => {
        setShowAddCategory(!showAddCategory);
    }

    const handleAccount = (value:any) => {
        setValues({
            ...values,
            accountId: value
        })
        const account = accounts.filter((acc: any) => acc.accountId === value);        
        selectAccount(account[0]);
        setAccountGoals(goals.filter((goal: any) => goal.accountId === value));
    }

    const handleSavingsChange = (value: any) => {
        if (value) {
            setExpense(false);
            setValues({
                ...values,
                descr: "savings"
            });
            setIsSavings(true);
        } else {
            setValues({
                ...values,
                descr: ""
            });
            setIsSavings(false);
        }
    }

    return (
        <IonModal isOpen={isOpen} cssClass='add-payment'>
            <IonContent fullscreen>
                <IonHeader className="modal-header">
                    <IonToolbar className="top-bar">
                        <IonTitle className="add-payment-title">New Transaction</IonTitle>
                        <IonButtons slot="end">
                            <IonButton onClick={hide} className="btn-menu">
                                <IonIcon slot="icon-only" icon={closeOutline} className="menu-icon" />
                            </IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>
                <div className="modal-content">
                    <div className="payment-form">
                        <IonGrid>
                            {!isSavings?
                            <Toggle expense={expense} first="Income" second="Expense" handle={(value: boolean) => setExpense(value)} />:
                            <Toggle expense={expense} first="Save" second="Deduct" handle={(value: boolean) => setExpense(value)} />}
                            <IonItem className="ion-no-padding ion-margin">
                                <IonSelect id="account" name="account" placeholder="Account" onIonChange={e => handleAccount(e.detail.value)}>
                                    {accounts.map((account:any) => (
                                        <IonSelectOption key={account.accountId} value={account.accountId}>{account.accountName}</IonSelectOption>
                                    ))}
                                </IonSelect>
                            </IonItem>
                            <IonItem  className="ion-no-padding ion-margin" lines="none">
                                <IonCol size="11">
                                    <IonLabel>Add to Savings</IonLabel>
                                </IonCol>
                                <IonCol size="1">
                                    <IonToggle checked={isSavings} onIonChange={() => {handleSavingsChange(!isSavings)}}/>
                                </IonCol>
                            </IonItem>
                            
                            {!isSavings? 
                                <IonItem className="ion-no-padding ion-margin">
                                    <IonInput value={values.descr} placeholder="Description" name="descr" onIonChange={handleChange}></IonInput>
                                </IonItem>:
                                <IonItem className="ion-no-padding ion-margin">
                                    <IonSelect id="goal" name="goal" placeholder="Savings Goals" onIonChange={e => selectAccountGoals(e.detail.value)}>
                                        {accountGoals.map((goal:any) => (
                                            <IonSelectOption key={goal.goalId} value={goal.goalId}>{goal.goalName}</IonSelectOption>
                                        ))}
                                    </IonSelect>
                                </IonItem>    
                            }
                            <IonItem className="ion-no-padding ion-margin">
                                <IonDatetime displayFormat="DD MM YY" placeholder="Date" name="transDate" value={values.transDate} onIonChange={handleChange}></IonDatetime>
                            </IonItem>
                            <IonItem className="ion-no-padding ion-margin">
                                <IonLabel position="fixed" className="fixedLabel">{currency}</IonLabel>
                                <IonInput type="number" step="0.01" value={values.amount} min="0.01" placeholder="Amount" name="amount" className={expense ? "negative" : "positive"} onIonChange={handleChange}></IonInput>
                            </IonItem>

                            {!isSavings?
                                <div>
                                    <CustomSelect id="category" value={values.category} placeholder="Category" name="category" status={!showAddCategory} handle={handleChange} handleAdd={(e: any) => toggleAdd(e)} list={categories.map(c => c.categoryName)} />
                                    {showAddCategory ? <AddCategory /> : null}
                                </div>:""
                            }

                            {!isSavings?
                                <div>
                                    <IonItem className="ion-no-padding ion-margin" lines="none">
                                        <IonCol id="recurring" size="11">
                                            <IonLabel>Recurring</IonLabel>
                                        </IonCol>
                                        <IonCol size="1">
                                            <IonToggle checked={recurring} onIonChange={e => setRecurring(e.detail.checked)} />
                                        </IonCol>
                                    </IonItem>
                                    {recurring ?
                                        <IonItem className="ion-no-padding ion-margin">
                                            <IonRow>
                                                <IonCol><IonInput min="1" type="number" name="freqNr" value={values.freqNr} onIonChange={handleChange}></IonInput></IonCol>
                                                <IonCol>
                                                    <IonSelect value={values.freqUnit} name="freqUnit" onIonChange={handleChange}>
                                                        {units.map((unit, index) => <IonSelectOption value={unit} key={index} >{unit}</IonSelectOption>)}
                                                    </IonSelect>
                                                </IonCol>
                                            </IonRow>
                                        </IonItem>
                                        : null
                                    }
                                </div>:""
                            }
                            <IonRow>
                                <IonCol>
                                    <IonButton expand="block" type="button" color="primary" id="btn-save-goal" className="ion-margin" onClick={handleSubmit}>Save</IonButton>
                                </IonCol>
                            </IonRow>
                            <IonRow>
                                <IonCol>
                                    {messages()}
                                </IonCol>
                            </IonRow>
                        </IonGrid>
                    </div>
                </div>
            </IonContent>
        </IonModal>
    );
};

export default AddPayment;
