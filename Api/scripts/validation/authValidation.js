const winston = require('../../core/winston');
const env = require('../env');
const { v4: uuidv4 } = require('uuid');

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

const validatePassword = (email) => {
    const re = /^^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/;
    return re.test(String(email).toLowerCase());
}

const validateUsername = (email) => {
    const re = /^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$/;
    return re.test(String(email).toLowerCase());
}

module.exports.validateSignup = (body) => {
    return new Promise((resolve, reject) => {
        //check the body exists
        if (Object.keys(body).length < 1) {reject("No body detected")}

        const expectedKeys = ["username", "email", "password"];
        
        //check all required body values exist
        expectedKeys.forEach((key, i) => {
            if (!(Object.keys(body)[i])) {
                reject(`the ${key} field is missing from the request body`);
            }
        });
        
        //check fot invalid pairs
        if (Object.keys(body).length !== expectedKeys.length) {
            reject("There are invalid properties in the request body")
        }

        //check the username has no illegal characters
        if (!(validateUsername(body.username))) {
            reject("The username is not valid");
        }

        //check the email correctly formatted
        if (!(validateEmail(body.email))) {
            reject("The email is not valid");
        }

        //check the password meets requirements
        if (!(validatePassword(body.password))) {
            reject("The password is not strong enough");
        }

        //add a userId
        body.userId = uuidv4();

        //resolve with a valid user object
        resolve(body);
    });
}

module.exports.validateLogin = (body) => {
    return new Promise((resolve, reject) => {
        //check the body exists
        if (Object.keys(body).length < 1) {reject("No body detected")}

        const expectedKeys = ["username", "password"];
        
        //check all required body values exist
        expectedKeys.forEach((key, i) => {
            if (!(Object.keys(body)[i])) {
                reject(`the ${key} field is missing from the request body`);
            }
        });
        
        //check fot invalid pairs
        if (Object.keys(body).length !== expectedKeys.length) {
            reject("There are invalid properties in the request body")
        }

        //check the username has no illegal characters
        if (!(validateUsername(body.username))) {
            reject("The username or password are incorrect");
        }

        //check the password meets requirements
        if (!(validatePassword(body.password))) {
            reject("The username or password are incorrect");
        }

        //resolve with a valid user object
        resolve(body);
    });
}

module.exports.validateEmail = validateEmail;
module.exports.validateUsername = validateUsername;
module.exports.validatePassword = validatePassword;