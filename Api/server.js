//requires
const express = require('express');
const winston = require('./core/winston');
const env = require('./scripts/env');
var bodyParser = require('body-parser');
var session = require('express-session');
var path = require('path');
const { urlencoded } = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser')
const expressSanitizer = require('express-sanitizer');
const authMiddleware = require('./scripts/middleware/authMiddleware')
//set the port
const port = env.port;

//set the app
const app = express();
const http = require('http').createServer(app);

//set up cors whitelist to enable credentials: "include" in headers
const prodUrl = "https://budgetplaner.co.uk"; //for prod build
const devUrl = "https://webprojects-dev.co.uk"; //for dev
const localUrl = "http://localhost:3000";
const allowList = [localUrl, devUrl, prodUrl];
const corsOptions = {
    origin: function(origin, callback) {
        if (allowList.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error("Not allowed by cors"))
        }
    },
    credentials: true
    
}

app.use(cors(corsOptions));

//app.uses
app.use(express.static('views'));
app.use(bodyParser.json());

app.use(expressSanitizer());

//include the cookie parser so we can read the cookies
app.use(cookieParser());

app.use(session({
    secret: env.session.secret,
    resave: true,
    saveUninitialized: false,
    cookie: {
        secure: false,
        maxAge: env.session.maxage
    }    
}));

//------------------------------------------------------------------------------------------------
//define routes
const authRoute = require('./routes/authRoute');
const goalRoute = require('./routes/goalRoute');
const categoryRoute = require('./routes/categoryRoute');
const budgetRoute = require('./routes/budgetRoute');

//routes
app.use('/auth', authRoute);

const userRoute = require('./routes/userRoutes');
const accountRoute = require('./routes/account-route');
const transactionRoute = require('./routes/transactionRoute');

//routes
app.use('/auth', authRoute);
app.use('/user', userRoute);

//use routers where possible
app.get('/', (req, res) => {
    winston.info('from the api');
    res.json('hello world 7');
});

//all app.use(routes) below here will need authentication
app.all('*', authMiddleware.authenticateToken, (req, res, next) => {
    next();
});

// protected routes
app.use('/account', accountRoute);
app.use('/transaction', transactionRoute);

app.use('/goal', goalRoute);
app.use('/category', categoryRoute);
app.use('/budget', budgetRoute);

app.get("/isLoggedIn", (req, res) => {
    const user = req.session.user
    return res.json({userId: user.userId, email: user.email, username: user.username });
});

//------------------------------------------------------------------------------------------------

http.listen(port, () => {
    winston.info(`Node app is listening on port: ${port}`);
});
