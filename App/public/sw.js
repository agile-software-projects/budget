const swVersion = 1.00;
const cacheName = 'budgetMainCache';
const cacheData = [
    //put data to cache here
    "."
];

importScripts("/serviceWorker/install.js");

importScripts("/serviceWorker/activate.js");

importScripts("/serviceWorker/fetch.js");