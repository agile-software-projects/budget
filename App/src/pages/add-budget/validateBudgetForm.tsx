export default function validateBudgetForm(values: any) {
    
    let errors: any = {};

    if (!values.budgetName.trim()) {
        errors.budgetName = "Description required";
    } else if (!/^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/.test(values.budgetName)) {
        errors.budgetName = "Only letters and numbers allowed";
    }

    if (!values.totalLimit.trim()) {
        errors.totalLimit = "Target amount required";
    } else if (!/^[1-9]\d*(\.\d+)?$/.test(values.totalLimit)) {
        errors.totalLimit = "Amount should be numbers only";
    }

    if (!values.currentAmount.trim()) {
        errors.currentAmount = "Current amount required";
    } else if (!/^[0-9]\d*(\.\d+)?$/.test(values.currentAmount)) {
        errors.currentAmount = "Amount should be numbers only";
    }

    if (!values.categoryName.trim()) {
        errors.categoryName = "Category required";
    }
    
    if (values.accountId !== undefined && !values.accountId.trim()) {
        errors.categoryName = "Account required";
    }

    return errors;
}