import React, {useState} from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButtons, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';
import './Home.css';
import Menu from '../menu/Menu';
import CreateAccount from '../create-account/CreateAccount';
import SingleAccount from '../single-account/SingleAccount';
import { useStore } from '../../store/store-actions';
import { priceFormat } from '../../helpers/price';

const Home = (props: any) => {

	const {accounts, user, selectAccount, currency, allTransactions, setAccountTransactions} = useStore()

	const [iscreateAccountOpen, toggleCreateAccount] = useState(false)
	const [isAccountOpen, toggleAccount] = useState(false);

	const handleOpenSingleAccount = (account: any) => {
		selectAccount(account);
		setAccountTransactions(allTransactions.filter((trans: any) => trans.accountId === account.accountId));
		toggleAccount(!isAccountOpen);
	}	

	return (
		<IonPage className="home">
			<CreateAccount isOpen={iscreateAccountOpen} hide={toggleCreateAccount}/>
			<SingleAccount isOpen={isAccountOpen} hide={toggleAccount}/>
			<IonContent fullscreen color="light">
				<IonHeader className="header">
					<IonToolbar color="light" className="top-bar">
						<IonTitle size="small" className="sub-title">welcome back</IonTitle>
						<IonTitle size="large" className="title home-title">{user?.username}</IonTitle>
						<IonButtons slot="end">
							<Menu setLoggedIn={props.setLoggedIn}/>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
				<div className="body-actions">
					<span className="body-title">My Accounts</span>
					<span className="body-action" onClick={() => {toggleCreateAccount(true)}}>+ Add account</span>
				</div>
				<div className="accounts-list">
					{accounts.map((account, i) => 
						<IonCard key={i}>
							<IonCardHeader onClick={() => {handleOpenSingleAccount(account)}}>
								<div className="card-top">{account.accountName}</div>
								<div className="card-bottom">
									<IonCardTitle>{currency}{priceFormat(account.currentBalance)}</IonCardTitle>
									<IonCardSubtitle>{account.accountType}</IonCardSubtitle>
								</div>
							</IonCardHeader>
						</IonCard>
					)}
				</div>
			</IonContent>
		</IonPage>
	);
};

export default Home;