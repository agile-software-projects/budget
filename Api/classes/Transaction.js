const { v4: uuidv4 } = require('uuid');
const databaseFunctions = require('../core/databaseFunctions');
const Category = require("./Category");
const Budget = require("./Budget");

class Transaction {
    constructor(transaction) {
        this.transactionId = transaction.transactionId;
        this.accountId = transaction.accountId;
        this.transactionDescription = transaction.transactionDescription;
        this.transactionDate = transaction.transactionDate;
        this.transactionDirection = transaction.transactionDirection;
        this.amount = transaction.amount;
        this.recurring = transaction.recurring || false;
        this.recurrencePeriod = transaction.recurrencePeriod;
    }

    wholeTransaction() {
        return {
            transactionId: this.transactionId,
            accountId: this.accountId,
            transactionDescription: this.transactionDescription,
            transactionDate: this.transactionDate,
            transactionDirection: this.transactionDirection,
            amount: this.amount,
            recurring: this.recurring,
            recurrencePeriod: this.recurrencePeriod
        }
    }

    assignTransactionId() {
        this.transactionId = uuidv4();
        return this;
    }

    validate() {
        if (this.transactionId.length !== 36) { throw("transactionId length is wrong"); }
        if (this.accountId.length !== 36) { throw("accountId length is wrong") }

        const isDate = (date) => {
            return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
        }
        if (!(isDate(this.transactionDate))) { throw("date is in a wrong format"); }

        const stringRe = /^(?=[a-zA-Z0-9._\ \-]{1,32}$)/;
        if (!(stringRe.test(this.transactionDescription))) { throw("transaction description failed validation"); }

        const validDirections = ["expense", "income", "savingsIn", "savingsOut"];
        if (!(validDirections.includes(this.transactionDirection))) { throw("transaction direction is neither income not expense"); }

        if (!(Number.isInteger(this.amount))) { throw("ammount is not an integer"); }

        if (!isNaN(this.recurring)) {
            if (this.recurring === 1) {
                this.recurring = true;
            } else if (this.recurring === 0) {
                this.recurring = false;
            }
        } 
        if (!(typeof this.recurring === "boolean")) { throw("recurring must be a boolean"); }

        if (this.recurring === true || this.recurring === 1) {
            const resetPeriodRe = /^([0-9])+([dwmy])$/;
            if (!(resetPeriodRe.test(this.recurrencePeriod))) { throw("reset period is incorrectly formatted"); }
        }

        return this;
    }

    verifyAccount() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("account", {accountId: this.accountId}).then((accounts) => {
                if (accounts.length === 1) {
                    resolve(accounts[0]);
                } else {
                    reject({error: "account does not exist"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    insertTransaction() {
        //check transaction doesnt exist and insert if not
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("transactions", {transactionId: this.transactionId}).then((transactions) => {
                if (transactions.length === 0) {
                    databaseFunctions.insertTableRow("transactions", this.wholeTransaction()).then((rowsAffected) => {
                        if (rowsAffected[0] === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "there was an error inserting transaction"});
                        }
                    });
                } else {
                    reject({error: "transaction already exists"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    insertCategoryLink(categoryId) {
        return new Promise((resolve, reject) => {
            const linkLine = {
                linkId: uuidv4(),
                transactionId: this.transactionId,
                categoryId: categoryId
            }
            databaseFunctions.insertTableRow("transactionCategoryLink", linkLine).then((rowsAffected) => {
                if (rowsAffected[0] === 1) {
                    resolve({success: true});
                } else {
                    reject({error: "there was an error inserting transaction"});
                }       
            }).catch((err) => {
                reject({error: err});
            })
        });
    }

    updateCategoryLink(categoryId) {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("transactionCategoryLink", {transactionId: this.transactionId}).then((links) => {
                if (links.length > 0) {
                    const linkLine = {
                        categoryId: categoryId
                    }
                    databaseFunctions.updateTableRow("transactionCategoryLink", {linkId: links[0].linkId}, linkLine).then((rowsAffected) => {
                        if (rowsAffected === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "there was an error updating category"});
                        }       
                    }).catch((err) => {
                        reject({error: err});
                    })
                } else {
                    reject({error: "no links to update"});
                }
            }).catch((err) => {
                reject(err);
            })
            
        });
    }

    populate() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("transactions", {transactionId: this.transactionId}).then((transactions) => {
                if (transactions.length === 1) {
                    const transaction = transactions[0];
                    this.transactionId = transaction.transactionId;
                    this.accountId = transaction.accountId;
                    this.transactionDescription = transaction.transactionDescription
                    this.transactionDate = transaction.transactionDate;
                    this.transactionDirection = transaction.transactionDirection;
                    this.amount = transaction.amount;
                    this.recurring = transaction.recurring;
                    this.recurrencePeriod = transaction.recurrencePeriod;
                    resolve({success: true});
                } else {
                    reject({error: "transaction does not exist"});
                }
            }).catch((err) => { 
                reject({error: err});
            });
        });
    }

    update() {
        //check transaction exists and update if it does
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("transactions", {transactionId: this.transactionId}).then((transactions) => {
                if (transactions.length === 1) {
                    databaseFunctions.updateTableRow("transactions", {transactionId: this.transactionId}, this.wholeTransaction()).then((rowsAffected) => {
                        if (rowsAffected === 1) {
                            resolve({success: true});
                        } else {
                            reject({error: "could not update"});
                        }
                    }).catch((err) => {
                        reject({error: err});
                    });
                } else {
                    reject({error: "transaction does not exist"});
                }
            }).catch((err) => { 
                reject({error: err});
            })
        });
    }

    deleteTransaction() {
        //delete the transaction
        return new Promise((resolve, reject) => {
            databaseFunctions.deleteTableRows("transactions", {transactionId: this.transactionId}).then((rowsAffected) => {
                if (rowsAffected.rowsAffected[0] === 1) {
                    resolve({success: true});
                } else {
                    reject({error: "could not be deleted"});
                }
            }).catch((err) => {
                console.log(err);
                reject({error: err});
            });
        });
    }

    getAccountCategories() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("accountCategoryView", {accountId: this.accountId}).then((categories) => {
                if (categories.length > 0) {
                    resolve(categories);
                } else {
                    reject("No categories found. You should have defaults but you don't?");
                }
            }).catch((err) => {
                console.log(err);
                reject(err);
            });
        });
    }

    getTransactionCategory() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("transactionCategoryView", {transactionId: this.transactionId}).then((categories) => {
                if (categories.length === 1) {
                    resolve(categories[0]);
                } else {
                    reject("No category found");
                }
            }).catch((err) => {
                reject(err);
            })
        })
    }
}

module.exports = Transaction;