import React, { useEffect, useState } from 'react';
import { IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonList, IonModal, IonSpinner, IonTitle, IonToolbar } from "@ionic/react"
import './Signup.css';
import { closeOutline } from 'ionicons/icons';
import * as validate from '../../helpers/validate';
import { SignupFormValues } from '../../helpers/form-interfaces';

import Auth from "../../helpers/Auth";

const Signup = (props: any) => {
    const { isOpen, hide } = props

    const [errors, setErrors] = useState<SignupFormValues>({} as SignupFormValues)
    const [errorMsg, setErrorMsg] = useState("Error");
    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success

    const [values, setValues] = useState<SignupFormValues>({
        username: '',
        email: '',
        password: '',
    })

    const handleChange = (e: any) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
    }

    const handleSubmit = (e: any) => {
        e.preventDefault()
        setErrors(validate.validateSignupForm(values))
        setIsSubmitted(true)
    }

    useEffect(
        () => {
            if (isSubmitted && !Object.keys(errors).length) {
                setSubmission(1);
                const auth = new Auth(values.username, values.password, values.email);
                try {
                    auth.validateSignupCredentials().signup().then((signedUp) => {
                        if(signedUp.status === 200) {
                            setSubmission(2)
                            props.onSignup()
                        } else {
                            setErrorMsg("There has been a problem signing up. It may be the username or password are in use.");
                            setSubmission(-1);
                        }
                    }).catch((err) => {
                        console.log(err);
                        setSubmission(-1);        
                    })
                } catch(err) {
                    console.log(err);
                    setSubmission(-1);      
                }
            } else if (Object.keys(errors).length) {
                setSubmission(-1)
            }
        },
        [isSubmitted, errors]
    )

    const Messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">{errorMsg}</IonLabel>
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Saved</IonLabel>
        } else {
            return null
        }
    }

    const Loading = () => {
        if(submission === 1){
            return (
                <div className="backdrop-spinner">
                    <IonSpinner name="crescent" color="white" />
                </div>
            )
        }
        return null
    }

    return (
        <IonModal isOpen={isOpen} cssClass='signup'>
            <IonContent fullscreen color="primary">

                <Loading />
                
                <IonHeader className="modal-header">
					<IonToolbar color="primary" className="top-bar">
						<IonTitle size="large" className="title">Signup</IonTitle>
						<IonButtons slot="end">
							<IonButton onClick={hide} className="btn-menu btn-menu-contrast">
								<IonIcon slot="icon-only" icon={closeOutline} className="menu-icon"/>
							</IonButton>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
                <form className="signup-form" onSubmit={handleSubmit}>
                    <IonList className="signup-controls">
                        <IonItem color="primary" lines="full" className="username">
                            <IonLabel position="floating">Username</IonLabel>
                            <IonInput name="username" onIonChange={handleChange} value={values.username}></IonInput>
                        </IonItem>
                        <IonLabel color="danger" className="ion-padding-start">{errors.username}</IonLabel>

                        <IonItem color="primary" lines="full" className="email">
                            <IonLabel position="floating">Email</IonLabel>
                            <IonInput name="email" onIonChange={handleChange} value={values.email}></IonInput>
                        </IonItem>
                        <IonLabel color="danger" className="ion-padding-start">{errors.email}</IonLabel>
                        
                        <IonItem color="primary" lines="full" className="password">
                            <IonLabel position="floating">Password</IonLabel>
                            <IonInput name="password" type="password" onIonChange={handleChange} value={values.password}></IonInput>
                        </IonItem>
                        <IonLabel color="danger" className="ion-padding-start">{errors.password}</IonLabel>
                        
                        <div className="form-actions">
                            <IonButton expand="block" type="submit" color="secondary" className="btn-signup">Signup</IonButton>
                        </div>
                    </IonList>

                    <Messages />
                </form>
            </IonContent>
        </IonModal>
    )
}

export default Signup