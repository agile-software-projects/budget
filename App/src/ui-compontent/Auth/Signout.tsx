import React from 'react';
import './Signout.css';
import Auth from "../../helpers/Auth";
const Signout = (props: any) => {

	const handleSignout = () => {
		const auth = new Auth();
		auth.signout().then(() => {
            props.setLoggedIn(false);
		}).then((err) => {
			console.log(err);
		});
	}

	return (
        <a className="signout" onClick={handleSignout}>Sign out</a>
	);
};

export default Signout;