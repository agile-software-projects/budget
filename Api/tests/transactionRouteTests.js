const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const baseUrl = "http://localhost:3000/transaction" //can be localhost
//any other consts (like auth)

const goodNewData = {
    transactionDescription: "pizza",
    transactionDate: new Date(),
    transactionDirection: "expense",
    amount: 1099
}

//FOR TESTING YOU NEED TO DISABLE THE AUTH MIDDLEWARE IN THE SERVER JS
//JUST COMMENT IT OUT FOR NOW

describe.skip("Testing /:accountid/:category/new", () => {
    it.skip("Rejectes if the account does not exist", (done) => {
        chai.request(baseUrl)
            .post('/00000000-0000-0000-0000-000000000000/food/new')
            .send(goodData) 
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "account does not exist");
            done();
        });
    });

    it.skip("Rejectes if there are no categories linked", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/food/new')
            .send(goodData) 
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "No categories found");
            done();
        });
    });

    it.skip("Rejects if there are no matching categories", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/pizza/new')
            .send(goodData) 
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "No matching categories found");
            done();
        });
    });

    it.skip("Returns 200 if insert successful", (done) => {
        chai.request(baseUrl)
            .post('/245784f3-3fb5-47f6-8263-f6ce7933bd13/food/new')
            .send(goodData) 
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    });
});

const goodEditData = {
    transactionDescription: "pizza",
    transactionDate: new Date(),
    transactionDirection: "expense",
    amount: 1299
    
}

describe.skip("Test /:transactionid/edit", () => { 
    it.skip("returns 400 when no transaction exists", (done) => {
        chai.request(baseUrl)
            .post("/00000000-0000-0000-0000-000000000000/edit")
            .send(goodData)
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "transaction does not exist");
            done();
        });
    });

    it.skip("returns 400 when no body is attached", (done) => {
        chai.request(baseUrl)
            .post("/ec6e468c-ad93-40ee-8411-2c8b41a62b6d/edit")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "no body detected");
            done();
        });
    });

    it("returns 200 when successfully updates", (done) => {
        chai.request(baseUrl)
            .post("/ec6e468c-ad93-40ee-8411-2c8b41a62b6d/edit")
            .send(goodEditData)
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    })
});

describe.skip("testing the /:transactionid/delete endpoint", () => {
    it.skip("returns 400 when the transaction doesn't exist", (done) => {
        chai.request(baseUrl)
            .post("/00000000-0000-0000-0000-000000000000/delete")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "transaction does not exist");
            done();
        });
    });
    //insert a record with id 1 for this to work
    it.skip("returns 200 on successful deletion", (done) => {
        chai.request(baseUrl)
            .post("/1/delete")
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    });
});

describe.skip("testing the /:accountid/deleteAll endpoint", () => {
    it.skip("returns 400 on invalid account", (done) => {
        chai.request(baseUrl)
            .post("/1/deleteAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "account doesn't exist");
            done();
        });
    });

    it.skip("returns 400 when the account has no transactions", (done) => {
        chai.request(baseUrl)
            .post("/00000000-0000-0000-0000-000000000000/deleteAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "account has no associated transactions");
            done();
        });  
    });

    it.skip("returns 200 when transactions are deleted", (done) => {
        chai.request(baseUrl)
            .post("/00000000-0000-0000-0000-000000000000/deleteAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });  
    });
});

describe.skip("testing /:accountid/accountTransaction/getAll", () => {
    it.skip("returns 400 when the account doesn't exist", (done) => {
        chai.request(baseUrl)
            .get("/1/accountTransaction/getAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "account doesn't exist");
            done();
        });
    });

    it("returns 400 when the account has no transactions", (done) => {
        chai.request(baseUrl)
            .get("/00000000-0000-0000-0000-000000000000/accountTransaction/getAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "account has no associated transactions");
            done();
        });
    });

    it("returns 200 when all ok", (done) => {
        chai.request(baseUrl)
            .get("/245784f3-3fb5-47f6-8263-f6ce7933bd13/accountTransaction/getAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });
});

//won't work. needs a user in the session object
describe.skip("testing /userTransaction/getAll", () => {
    it.skip("returns 400 when the user doesnt exist", (done) => {
        chai.request(baseUrl)
            .get("/userTransaction/getAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "no user found");
            done();
        });
    });

    it.skip("returns 400 when the user has no accounts", (done) => {
        chai.request(baseUrl)
            .get("/userTransaction/getAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "user has no associated accounts");
            done();
        });
    });

    it("returns 200 when all ok", (done) => {
        chai.request(baseUrl)
            .get("/userTransaction/getAll")
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });
});

describe.skip("testing /:transactionid/get", () => {
    it("returns 400 when transaction doesnt exist", (done) => {
        chai.request(baseUrl)
            .get("/1/get")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "transaction does not exist");
            done();
        });
    });

    it("returns 200 when all ok", (done) => {
        chai.request(baseUrl)
            .get("/ec6e468c-ad93-40ee-8411-2c8b41a62b6d/get")
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });
});

const goodRecurringData = {
    recurring: true,
    recurrencePeriod: "1m"
}

const badRecurringData = {
    recurring: true,
    recurrencePeriod: "awefwwef"
}

describe.skip("testing /:transactionid/setRecurring", () => {
    it.skip("returns 400 if the transaction doesnt exist", (done) => {
        chai.request(baseUrl)
            .post("/1/setRecurring")
            .send(goodRecurringData)
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "transaction does not exist");
            done();
        });
    });

    it.skip("returns 500 if the body fails validation", (done) => {
        chai.request(baseUrl)
            .post("/ec6e468c-ad93-40ee-8411-2c8b41a62b6d/setRecurring")
            .send(badRecurringData)
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            assert.strictEqual(res.body.error, "reset period is incorrectly formatted");
            done();
        });
    });

    it.skip("returns 200 when all ok", (done) => {
        chai.request(baseUrl)
            .post("/ec6e468c-ad93-40ee-8411-2c8b41a62b6d/setRecurring")
            .send(goodRecurringData)
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    });
});

describe.skip("testing /:transactionid/recurring/toggle", () => {
    it.skip("returns 400 when transaction doesnt exist", (done) => {
        chai.request(baseUrl)
            .post("/1/recurring/toggle")
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "transaction does not exist");
            done();
        });
    });

    it("returns 200 when all ok", (done) => {
        chai.request(baseUrl)
            .post("/ec6e468c-ad93-40ee-8411-2c8b41a62b6d/recurring/toggle")
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            done();
        });
    });
});

