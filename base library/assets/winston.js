const winston = require('winston');
const env = require('../scripts/env');

const logLevels = {
    levels: {
      error: 0,
      warn: 1,
      info: 2,
      http: 3,
      websocket: 4,
      sql: 5,
      debug: 6
    },
    colors: {
      error: "red",
      warn: "yellow",
      info: "blue",
      http: "green",
      websocket: "cyan",
      sql: "gray",
      debug: "magenta"
    }
};

winston.addColors(logLevels);

const alignedWithColorsAndTime = winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.splat(),
    winston.format.printf(info => {const {
        timestamp, level, message, ...args
      } = info;

      const ts = timestamp.slice(0, 19).replace('T', ' ');
      return `${ts} [${level}]: ${message} ${Object.keys(args).length ? JSON.stringify(args, null, 2) : ''}`;
    }),
);

const alignedWithTime = winston.format.combine(
    winston.format.timestamp(),
    winston.format.align(),
    winston.format.splat(),
    winston.format.printf(info => {const {
        timestamp, level, message, ...args
      } = info;

      const ts = timestamp.slice(0, 19).replace('T', ' ');
      return `${ts} [${level}]: ${message} ${Object.keys(args).length ? JSON.stringify(args, null, 2) : ''}`;
    }),
);

const logger = winston.createLogger({
    level: env.winston.debugLevel,
    levels: logLevels.levels,
    transports: [
        new winston.transports.Console({format: alignedWithColorsAndTime}),
        new winston.transports.File({level: 'error', filename: 'logs/error.log', format: alignedWithTime}),
        new winston.transports.File({level: 'warn', filename: 'logs/warn.log', format: alignedWithTime}),
        new winston.transports.File({level: 'info', filename: 'logs/info.log', format: alignedWithTime}),
        new winston.transports.File({level: 'http', filename: 'logs/http.log', format: alignedWithTime}),
        new winston.transports.File({level: 'websocket', filename: 'logs/websocket.log', format: alignedWithTime}),
        new winston.transports.File({level: 'sql', filename: 'logs/sql.log', format: alignedWithTime}),
        new winston.transports.File({level: 'debug', filename: 'logs/debug.log', format: alignedWithTime})
    ]
})

module.exports = logger;