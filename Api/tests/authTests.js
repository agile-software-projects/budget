const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const baseUrl = "http://localhost:3000/auth" //can be localhost
//any other consts (like auth)

describe.skip("Testing the signup endpoint", () => {
    it("All is well. Status 200", (done) => {
        chai.request(baseUrl)
            .post('/signup')
            .send({
                username: "richardsefton",
                email: "richard.sefton@googlemail.com",
                password: "somePa55w0rd!"
            }) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            assert.strictEqual(typeof res.body.error, 'undefined')
            done();
        });
    });

    it("Return status 400 if email is bad", (done) => {
        chai.request(baseUrl)
            .post('/signup') //can be post etc
            .send({
                username: "richardsefton",
                email: "richard.seftongooglemail.com",
                password: "somePa55w0rd!",
            }) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "New user could not be validated");
            done();
        });
    });

    it("Return status 400 if username is bad", (done) => {
        chai.request(baseUrl)
            .post('/signup') //can be post etc
            .send({
                username: "richardsefton;",
                email: "richard.sefton@googlemail.com",
                password: "somePa55w0rd!",
            }) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "New user could not be validated");
            done();
        });
    });

    it("Return status 400 if user already exists", (done) => {
        chai.request(baseUrl)
            .post('/signup') //can be post etc
            .send({
                username: "something",
                email: "richard.sefton@googlemail.com",
                password: "somePa55w0rd!",
            }) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "The user exists already");
            done();
        });
    });
});

describe("Testing the login endpoint", () => {
    it.skip("All is well. Status 200", (done) => {
        chai.request(baseUrl)
            .post('/login')
            .send({
                username: "richardsefton",
                password: "somePa55w0rd!"
            }) //body
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            assert.strictEqual(res.body.success, true);
            assert.strictEqual(typeof res.body.error, 'undefined')
            done();
        });  
    });

    //cant be validated
    it.skip("Return status 400 if user doesn't exist", (done) => {
        chai.request(baseUrl)
            .post('/login')
            .send({
                username: "duck",
                password: "somePa55w0rd!",
            }) 
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "The username or password are incorrect");
            done();
        });
    });

    it("Return status 400 if password doesn't match the database", (done) => {
        chai.request(baseUrl)
            .post('/login')
            .send({
                username: "richardsefton",
                password: "someP55frawefw0rd!",
            }) 
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "could not authenticate");
            done();
        });
    });
});