import IDB from "./IDB";
import { apiUrl } from "./env";

export default class Auth {
    constructor(username, password, email) {
        this.username = username;
        this.password = password;
        this.email = email;

        this.idb = new IDB("budget", 1);
    }

    validateEmail() {
        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(this.email));
    }
    
    validatePassword() {
        const re = /^^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,32}$/;
        return re.test(this.password);
    }
    
    validateUsername() {
        const re = /^(?=[a-zA-Z0-9._]{4,32}$)(?!.*[_.]{2})[^_.].*[^_.]$/;
        return re.test(this.username);
    }

    validateLoginCredentials() {
        if (!(this.validateUsername())) { throw(new Error("The username could not be validated")); }
        if (!(this.validatePassword())) { throw(new Error("The password could not be validated")); }
        return this;
    }

    validateSignupCredentials() {
        if (!(this.validateUsername())) { throw(new Error("The username could not be validated")); }
        if (!(this.validateEmail())) { throw(new Error("The email address could not be validated")); }
        if (!(this.validatePassword())) { throw(new Error("The password could not be validated")); }
        return this;       
    }

    loginBody() {
        return {
            username: this.username,
            password: this.password
        }
    }

    signupBody() {
        return {
            username: this.username,
            email: this.email,
            password: this.password
        }
    }

    login() {
        return new Promise((resolve, reject) => {
            fetch(`${apiUrl}/auth/login`, {
                method: "post",
                headers: {
                    "Content-Type": "application/json"
                },
                credentials: "include",
                body: JSON.stringify(this.loginBody())
            }).then((response) => {
                if(!response.ok){
                    throw new Error('Error logging in')
                }
                return response.json();
            }).then((responseJson) => {
                // this.setUserContext();
                resolve(responseJson);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    checkLoginState() {
        return new Promise((resolve, reject) => {
            fetch((`${apiUrl}/isLoggedIn`), {
                method: 'get',
                credentials: "include",
            }).then((response) => {
                if(!response.ok){
                    throw new Error('No authenticated')
                }
                return response.json();
            }).then((responseJson) => {
                resolve(responseJson);
            }).catch((err) => {
                reject(err);
            });
        });
    }

    signup() {
        return new Promise((resolve, reject) => {
            fetch(`${apiUrl}/auth/signup`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(this.signupBody()),
                credentials: "include"
            }).then(response => {
                if(!response.ok){
                    throw new Error('Error on sign up')
                }
                const clonedResponse = response.clone();
                return {
                    responseJson: response.json(),
                    status: clonedResponse.status
                };
            }).then((responseObj) => {
                this.login().then(() => {
                    this.setDefaults().then(() => {
                        // this.setUserContext();
                        resolve(responseObj);
                    }).catch((err) => {
                        reject(err);
                    });
                }).catch((err) => {
                    reject(err);
                })
            }).catch((err) => {
                reject(err);
            });
        });
    }

    setDefaults() {
        return new Promise((resolve, reject) => {
            fetch(`${apiUrl}/budget/createDefaults`, {
                method: "post",
                credentials: "include"
            }).then((response) => {
                response.status === 200 ? resolve() : reject("unable to create defaults");
            }).catch((err) => {
                reject(err);
            });
        });
    }

    signout() {
        this.clearUserContext();
        return new Promise((resolve, reject) => {
            fetch(`${apiUrl}/auth/signout`, {
                method: "post",
                credentials: "include"
            }).then((response) => {
                if (response.ok) {
                    resolve();
                } else { 
                    reject();
                }
            }).catch((err) => {
                reject(err);
            })
        })
    }

    setUserContext() {
        fetch(`${apiUrl}/user/loggedInUser`, {
                method: "get",
                credentials: "include"
        }).then((response) => {
            return response.json()
        }).then(responseJson => {
            if (responseJson.userId) {
                responseJson.id = 1;
                this.idb.dbInsert("clientUser", [responseJson]);
            }
        }).catch((err) => {
            console.log(err);
        });

        fetch(`${apiUrl}/account/getAccounts`, {
            method: "get",
            credentials: "include"
        }).then((response) => {
            return response.json();
        }).then((accounts) => {
            this.idb.dbInsert("accounts", accounts);
        }).catch((err) => {
            console.log(err);
        });

        fetch(`${apiUrl}/category/getAll`, {
            method: "get",
            credentials: "include"
        }).then((response) => {
            return response.json();
        }).then((categories) => {
            this.idb.dbInsert("categories", categories);
        }).catch((err) => {
            console.log(err);
        });
        
        return this;
    }

    clearUserContext() {
        this.idb.loadFromDb("clientUser").then(clientUsers => {
            clientUsers.forEach(cU => {
                this.idb.removeRecord("clientUser", cU.id);
            });
        });
        this.idb.loadFromDb("accounts").then(accounts => {
            accounts.forEach(acc => {
                this.idb.removeRecord("accounts", acc.id);
            });
        });
        this.idb.loadFromDb("categories").then(categories => {
            categories.forEach(cat => {
                this.idb.removeRecord("categories", cat.id);
            });
        });
    }
}