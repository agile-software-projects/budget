const winston = require('../../core/winston');
const env = require('../env');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
var databaseFunctions = null;

if(env.devDatabase.enabled) {
    databaseFunctions = require('../../core/databaseFunctions');
}

const checkUserExists = (username) => {
    return new Promise((resolve, reject) => {
        databaseFunctions.selectTable("users", {username: username}).then((users) => {
            if (users.length !== 1) {
                reject("No matching user");
            } else {
                if (users[0].active === 1){
                    resolve(users[0]);
                } else {
                    reject("Token has been manually expired");
                }
            }
        })
    })
} 

module.exports.authenticateToken = (req, res, next) => {
    if (env.devDatabase.enabled) {
        if (req.cookies.token) {
            jwt.verify(req.cookies.token, env.jwt.key, (err, decode) => {
                if (err) {
                    res.status(401)
                    res.json({error: "Unauthorised: Invalid token"});
                } else {
                    checkUserExists(decode.username).then((user) => {
                        req.session.user = user;
                        next();
                    }).catch((err) => {
                        res.status(401);
                        return res.json({error: err});
                    })
                }    
            })
        } else {
            res.status(401);
            return res.json({error: "Unauthorised: No token"});
        }
    } else {
        res.status(500);
        return res.json({error: "Internal server error"});
    }
} 