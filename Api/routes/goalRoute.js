const express = require('express');
const router = express.Router();
const winston = require('../core/winston');
const env = require('../scripts/env');
const {v4:uuidv4} = require('uuid');
const Goal = require('../classes/SavingsGoal');
const Account = require('../classes/Account');
const User = require('../classes/User');

router.all('*', (req, res, next) => {

    if(env.devDatabase.enabled) {
        next();
    } else {
        res.status(500).json({error: "No database detected"});
    }

});

router.post("/:transactionId/addSavings", (req, res) => {
    const goal = new Goal({goalId: req.sanitize(req.body.goalId)});
    const data = req.body;
    const transactionId = req.params.transactionId;
    goal.populate().then(() => {
        goal.addSavingsLine(data, transactionId).then((result) => {
            if (result.success) {
                res.status(200);
                return res.json(result);
            } else {
                res.status(400);
                return res.json({error: "Savings failed to save"});
            }
        }).catch((err) => {
            winston.error(JSON.stringify(err));
            res.status(400);
            return res.json(err);
        })
    }).catch((err) => {
        winston.error(err.message);
        res.status(400);
        return res.json(err);
    });  
});

router.post("/:accountid/new", (req, res) => {

    const account = new Account({accountId: req.params.accountid});
    account.populate().then(() => {

        const goal = new Goal({
            goalId: req.sanitize(req.body.goalId),
            goalName: req.sanitize(req.body.goalName),
            goalAmount: Number.parseInt(req.sanitize(req.body.goalAmount)),
            accountId: req.sanitize(req.params.accountid),
            active: req.body.active,
            creationDate: req.sanitize(req.body.creationDate),
            targetDate: req.sanitize(req.body.targetDate),
            currentAmount: Number.parseInt(req.body.currentAmount)
        });

        try {
            goal.validate().insertSavingsGoal().then((success) => {

                if (success.success) {
                    res.status(200);
                    return res.json({success: true});
                } else {
                    winston.error("Error inserting goal");
                    res.status(500);
                    return res.json({error: "There was an error insertting"});
                }
            }).catch((err) => {
                winston.error(JSON.stringify(err));
                res.status(400);
                return res.json(err);
            });

        } catch(err) {
            winston.error(JSON.stringify(err.message));
            res.status(400);
            return res.json({error: err.message});
        }

    }).catch((err) => {

        winston.error(err);
        res.status(400);
        return res.json(err);
    })

});


router.get("/:field/:code/getGoal", (req, res) => {

    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);

    const goalData = {};

    if (field !== "goalId") {
        res.status(400);
        return res.json({error: "unknown field identifier"});
    }

    goalData.goalId = code;
    const goal = new Goal(goalData);

    goal.populate().then((result) => {
        if (result.success === true) {
            res.status(200);
            return res.json({
                goalId: goal.goalId,
                goalName: goal.goalName,
                goalAmount: goal.goalAmount,
                creationDate: goal.creationDate,
                targetDate: goal.targetDate,
                active: goal.active,
                currentAmount: goal.currentAmount,
                accountId: goal.accountId
            });
        } 
        res.status(500);
        return res.json({error: "goal id does not exist"});
    }).catch((err) => {
        winston.error(err);
        res.status(500);
        return res.json(err);
    });

});

router.post("/:field/:code/updateGoal", (req, res) => {

    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);    

    const goalData = {};
    goalData.goalId = code;

    const goal = new Goal(goalData);

    goal.populate().then(() => {

        //throw with no goal name to update
        if ((Object.keys(req.body).length === 0) || 
            (!(req.body.goalName) && 
            !(req.body.goalAmount) &&
            !(req.body.targetDate) &&
            !(req.body.currentAmount) &&
            !(req.body.accountId) &&
            (req.body.active == undefined))) {
            res.status(400);
            return res.json({error: "no goal name, amount, target date, active state or current amount detected in body"});
        }

        if (req.body.goalName) { goal.goalName = req.body.goalName; }
        if (req.body.goalAmount) { goal.goalAmount = req.body.goalAmount; }
        if (req.body.targetDate) { goal.targetDate = req.body.targetDate; }
        if (req.body.currentAmount) { goal.currentAmount = req.body.currentAmount; }
        if (req.body.active != undefined) { goal.active = req.body.active; }
        if (req.body.accountId) { goal.accountId = req.body.accountId; }

        goal.validate().update().then(() => {
            res.status(200);
            return res.json({success: true});

        }).catch((err) => {
            winston.error(err.message);
            res.status(500);
            return res.json(err);
        })

    }).catch((err) => {
        winston.error(err.message);
        res.status(500);
        return res.json(err);
    });

});

router.post("/:field/:code/deleteGoal", (req, res) => {

    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);

    const goalData = {};
    goalData.goalId = code;

    const goal = new Goal(goalData);

    goal.deleteSavingsGoal().then(() => {
        res.status(200);
        return res.json({success: true});
    }).catch((err) => {
        winston.error(err);
        res.status(500);
        return res.json(err);
    })

});

router.get("/user/getAll", (req, res) => {
    //going to get from the stored user
    let userId = "";
    if (req.session.user) {
        userId = req.session.user.userId;
    } else {
        res.status(401);
        return res.json({error: "Somehow someone got past the authentication"})
    }

    const user = new User({userId: userId});

    user.populate().then(() => {
        user.getAllUserGoals().then((goals) => {
            res.status(200);
            return res.json(goals);

        }).catch((err) => {
            winston.error(JSON.stringify(err));
            res.status(400);
            return res.json(err);
        });

    }).catch((err) => {

        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);

    });

});

router.get("/user/getAllAccounts", (req, res) => {

    //going to get from the stored user
    let userId = "";

    if (req.session.user) {
        userId = req.session.user.userId;
    } else {
        res.status(401);
        return res.json({error: "Somehow someone got past the authentication"})
    }

    const user = new User({userId: userId});

    user.populate().then(() => {

        user.getAllAccounts().then((accounts) => {
            res.status(200);
            return res.json({accounts: accounts});
        }).catch((err) => {
            winston.error(JSON.stringify(err));
            res.status(400);
            return res.json(err);
        });

    }).catch((err) => {

        winston.error(JSON.stringify(err));
        res.status(400);
        return res.json(err);

    });

});

module.exports = router;