const assert = require('assert');
const SavingsGoal = require('../classes/SavingsGoal');

const goodData = {
    goalId: "00000000-0000-0000-0000-000000000000",
    goalName: "yatch",
    goalAmount: 200000,
    creationDate: "2021-01-27 23:07:51.523",
    targetDate: "2021-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000,
    accountId: "00000000-0000-0000-0000-000000000000"
}

const badGuid = {
    goalId: "00000000-0000-00000",
    goalName: "yatch",
    goalAmount: 200000,
    creationDate: "2021-01-27 23:07:51.523",
    targetDate: "2021-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000,
    accountId: "00000000-0000-0000-0000-000000000000"
}

const badAccountid = {
    goalId: "00000000-0000-0000-0000-000000000000",
    goalName: "yatch",
    goalAmount: 200000,
    creationDate: "2021-01-27 23:07:51.523",
    targetDate: "2021-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000,
    accountId: "00000000-0000-0000-0000"
}

const badInt = {
    goalId: "00000000-0000-0000-0000-000000000000",
    goalName: "yatch",
    goalAmount: 2000.002345,
    creationDate: "2021-01-27 23:07:51.523",
    targetDate: "2021-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000,
    accountId: "00000000-0000-0000-0000-000000000000"
}

const badDate = {
    goalId: "00000000-0000-0000-0000-000000000000",
    goalName: "yatch",
    goalAmount: 200000,
    creationDate: "2021-01-27dafwefw:51.523",
    targetDate: "2021-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000,
    accountId: "00000000-0000-0000-0000-000000000000"
}

const badBool = {
    goalId: "00000000-0000-0000-0000-000000000000",
    goalName: "yatch",
    goalAmount: 200000,
    creationDate: "2021-01-27 23:07:51.523",
    targetDate: "2021-01-27 23:07:51.523",
    active: 12,
    currentAmount: 20000,
    accountId: "00000000-0000-0000-0000-000000000000"
}

const badName = {
    goalId: "00000000-0000-0000-0000-000000000000",
    goalName: "y$%^atch",
    goalAmount: 200000,
    creationDate: "2021-01-27 23:07:51.523",
    targetDate: "2021-01-27 23:07:51.523",
    active: true,
    currentAmount: 20000,
    accountId: "00000000-0000-0000-0000-000000000000"
}

describe("Testing the savings goal class", () => {
    it.skip("returns a whole object", (done) => {
        const goal = new SavingsGoal(goodData);
        assert.strictEqual(Object.keys(goal.wholeSavingsGoal()).length, 8);
        done();
    });

    it.skip("assigns a valid Guid", (done) => {
        const goal = new SavingsGoal(goodData);
        goal.assignSavingsGoalId();
        assert.strictEqual(goal.goalId.length, 36);
        done();
    });

    it("pass the validation", (done) => {
        const goal = new SavingsGoal(goodData);
        assert.strictEqual(goal.validate(), goal);
        done();
    });

    it("fail the validation on guid", (done) => {
        const goal = new SavingsGoal(badGuid);
        let validate = function () { goal.validate() };
        assert.throws(validate, Error, "The goalId is not valid");
        done();
    });

    it("fail the validation on bad account id", (done) => {
        const goal = new SavingsGoal(badAccountid);
        let validate = function () { goal.validate() };
        assert.throws(validate, Error, "The accountId is not valid");
        done();
    });

    it("fail the validation on name", (done) => {
        const goal = new SavingsGoal(badName);
        let validate = function () { goal.validate() };
        assert.throws(validate, Error, "The goal name is not valid");
        done();
    });

    it("fail the validation on int", (done) => {
        const goal = new SavingsGoal(badInt);
        let validate = function () { goal.validate() };
        assert.throws(validate, Error, "total goal amount must be of type Int");
        done();
    });

    it("fail the validation on date", (done) => {
        const goal = new SavingsGoal(badDate);
        let validate = function () { goal.validate() };
        assert.throws(validate, Error, "creation date must be of Date type");
        done();
    });

    it("fail the validation on bool", (done) => {
        const goal = new SavingsGoal(badBool);
        let validate = function () { goal.validate() };
        assert.throws(validate, Error, "The active flag is not valid");
        done();
    });

    it("activates the savings goal", (done) => {
        const goal = new SavingsGoal(goodData);
        goal.activate();
        assert.strictEqual(goal.active, true);
        done();
    });

    it("deactivate the savings goal", (done) => {
        const goal = new SavingsGoal(goodData);
        goal.deactivate();
        assert.strictEqual(goal.active, false);
        done();
    });

    it.skip("verify the account", (done) => {
        const goal = new SavingsGoal(goodData);

        setTimeout(() => {
            goal.validate().verifyAccount().then((account) => {
                assert.strictEqual(account.accountId, goal.accountId);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            });
        }, 3000);
    });

    it.skip("insert the savings goal", (done) => {
        const goal = new SavingsGoal(goodData);

        setTimeout(() => {
            goal.assignSavingsGoalId().validate().insertSavingsGoal().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });

    const goalId = "b5dbd5bd-3ca9-4e43-ad80-02ea04b3d90f";
    it.skip("populate the object", (done) => {
        const goal = new SavingsGoal({goalId: goalId});

        setTimeout(() => {
            goal.populate().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });

    it.skip("update the record", (done) => {
        const goal = new SavingsGoal(goodData);
        goal.goalId = goalId;
    
        setTimeout(() => {
            goal.deactivate().update().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });

    it.skip("delete the record", (done) => {
        const goal = new SavingsGoal(goodData);
        goal.goalId = goalId;
    
        setTimeout(() => {
            goal.deleteSavingsGoal().then((result) => {
                assert.strictEqual(result.success, true);
                done();
            }).catch((err) => {
                console.log(err);
                assert.strictEqual(1, 0);
                done();
            })
        }, 3000);
    });
    
});
