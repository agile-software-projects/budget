import React, { useEffect } from 'react';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './theme/custom-colors.css';
import './theme/custom.css';

import Layout from './layout/Layout'
import Login from './pages/login/Login';
import { IonLoading, IonToast } from '@ionic/react';
import * as initialize from './store/store-init';
import { useStore } from './store/store-actions';

const App: React.FC = () => {
	const {loadingApp, setLoadingApp, loggedIn, setLoggedIn, setUser, setAccounts, setCategories, setGoals, setBudgets, setAllTransactions, showNotification, setShowNotification} = useStore()

	const init = async () => {
		setLoadingApp(true)
		try{
			// load user
			const user = await initialize.getUser()
			setUser(user)
    		setLoggedIn(true)

			// load user accounts
			const accounts = await initialize.getAccounts()
			setAccounts(accounts)

			// load all transactions
			initialize.getAllTransactions().then(allTransactions => {
				setAllTransactions(allTransactions)
			})

			// load all categories
			initialize.getCategories().then(categories => {
				setCategories(categories)
			})

			// load all user goals
			initialize.getGoals().then(goals => {
				setGoals(goals)
			})

			// load all user busgets
			initialize.getBudgets().then(budgets => {
				setBudgets(budgets)
			})

			setLoadingApp(false)
		}catch(e){
			console.error(e)
			setLoadingApp(false)
		}
	}

	useEffect(() => {
		init()
	}, [])

	const Default = () => {
		if(!loggedIn){
			return <Login onLoggedIn={() => init()}/>
		}
		return <Layout/>
	}

	return (
		<>
			<IonLoading cssClass='loading' isOpen={loadingApp} message={'Loading...'} />
			<IonToast isOpen={showNotification?.show} onDidDismiss={() => setShowNotification({show: false, message: null, color: null})} message={showNotification?.message} duration={4000} color={showNotification?.color}/>
			{Default()}
		</>
	)
}

export default App