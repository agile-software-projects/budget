const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');

chai.use(chaiHttp);

const baseUrl = "http://localhost:3000/user"; //can be localhost
//any other consts (like auth)

describe.skip("Testing /:field/:code/updatePassword", () => {
    it("Changes password user userId", (done) => {
        chai.request(baseUrl)
            .post('/userId/245784f3-3fb5-47f6-8263-f6ce7933bd13/updatePassword')
            .send({password: "somepass"})
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Changes password user  email", (done) => {
        chai.request(baseUrl)
            .post('/email/richard.sefton@googlemail.com/updatePassword')
            .send({password: "somepass"})
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Changes password user username", (done) => {
        chai.request(baseUrl)
            .post('/username/richardsefton/updatePassword')
            .send({password: "somepass"})
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("errors on a bad field if", (done) => {
        chai.request(baseUrl)
            .post('/awefwef/richardsefton/updatePassword')
            .send({password: "somepass"})
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "unknown field identifier");
            done();
        });
    });

    it("errors on user not found", (done) => {
        chai.request(baseUrl)
            .post('/username/awefawc/updatePassword')
            .send({password: "somepass"})
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            assert.strictEqual(res.body.error, "The username or password are incorrect")
            done();
        });
    });

    it("errors with no password", (done) => {
        chai.request(baseUrl)
            .post('/username/awefawc/updatePassword')
            .send({awef: "somepass"})
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "no password detected in body")
            done();
        });
    });

    it("errors with no body", (done) => {
        chai.request(baseUrl)
            .post('/username/awefawc/updatePassword')
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "no password detected in body")
            done();
        });
    });
});

//lack of auth cookie will fail the tests. They will return 401 because auth works
//disable auth in user route to try them
describe.skip("Testing /:field/:code/getUser", () => {
    it("Get user from userId", (done) => {
        chai.request(baseUrl)
            .get('/userId/245784f3-3fb5-47f6-8263-f6ce7933bd13/getUser')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Gets user from email", (done) => {
        chai.request(baseUrl)
            .get('/email/richard.sefton@googlemail.com/getUser')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Gets user from username", (done) => {
        chai.request(baseUrl)
            .get('/username/richardsefton/getUser')
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("errors on a bad field", (done) => {
        chai.request(baseUrl)
            .get('/awefawef/richardsefton/getUser')
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "unknown field identifier");
            done();
        });
    });

    it("errors on a bad user", (done) => {
        chai.request(baseUrl)
            .get('/username/fasdcawe/getUser')
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            assert.strictEqual(res.body.error, "The username or password are incorrect")
            done();
        });
    });
});

describe.skip("Testing /:field/:code/updateUser", () => {
    it("Updates user from userId", (done) => {
        chai.request(baseUrl)
            .post('/userId/245784f3-3fb5-47f6-8263-f6ce7933bd13/updateUser')
            .send({
                email: "test@test.com"
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("Updates user from username", (done) => {
        chai.request(baseUrl)
            .post('/username/richardsefton/updateUser')
            .send({
                email: "test@test.com"
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 200);
            done();
        });
    });

    it("errors on a bad field", (done) => {
        chai.request(baseUrl)
            .post('/awefawef/richardsefton/updateUser')
            .send({
                email: "test@test.com"
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "unknown field identifier");
            done();
        });
    });

    it("errors on a bad user", (done) => {
        chai.request(baseUrl)
            .post('/username/fasdcawe/updateUser')
            .send({
                email: "test@test.com"
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 500);
            assert.strictEqual(res.body.error, "The username or password are incorrect")
            done();
        });
    });

    it("errors on no email", (done) => {
        chai.request(baseUrl)
            .post('/username/richardsefton/updateUser')
            .send({
                awef: "test@test.com"
            })
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "no email detected in body");
            done();
        });
    });
    
    it("errors on no body", (done) => {
        chai.request(baseUrl)
            .post('/username/richardsefton/updateUser')
        .end((err, res) => {
            assert.strictEqual(res.status, 400);
            assert.strictEqual(res.body.error, "no email detected in body");
            done();
        });
    });
});
