import React, {useState} from 'react';

import { IonButton, IonContent, IonInput, IonItem, IonLabel, IonList, IonPage, IonText } from "@ionic/react"
import './Login.css';

import Auth from "../../helpers/Auth";

import Signup from '../signup/Signup';
import useModal from '../../helpers/useModal';
import { useStore } from '../../store/store-actions';

const Login = (props: any) => {
    const [isSignupOpen, toggleForm] = useModal()
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [triggerError, setTriggerError] = useState(false);
    const [error, setError] = useState("");
    const {setLoadingApp} = useStore()

    const handleLogin = (e: any) => {
        setLoadingApp(true)
        setTriggerError(false);
        const auth = new Auth(username, password, "")
        try {
            auth.validateLoginCredentials().login().then((res: any) => {
                if ((res) && (res.success === true)) {
                    auth.checkLoginState().then((user: any) => {
                        props.onLoggedIn()
                    })
                } else {
                    //handle the errors
                    setError("Your credentials either do not match or do not exist");
                    setTriggerError(true);
                    setLoadingApp(false)
                }
            }).catch((err) => {
                setLoadingApp(false)
                setError(err.toString());
                setTriggerError(true);                
            })
        } catch(error) {
            setLoadingApp(false)
            setError(error.toString());
            setTriggerError(true);
        }
    }

    return (
        <IonPage className="login">

            <Signup isOpen={isSignupOpen} hide={toggleForm} onSignup={() => props.onLoggedIn()}/>

            <IonContent fullscreen color="primary">
                <div className="login-container">
                    <div className="logo">
                        <h1>Logo</h1>
                        <p>personal finance app</p>
                    </div>
                    
                    <div className="login-form">
                        <IonList class="login-controls">
                            <IonItem color="primary" lines="full" className="username">
                                <IonLabel position="floating">Username</IonLabel>
                                <IonInput value={username} name="username" onIonChange={(e: any) => setUsername(e.target.value)}></IonInput>
                            </IonItem>
                            <IonItem color="primary" lines="full" className="password">
                                <IonLabel position="floating">Password</IonLabel>
                                <IonInput type="password" name="password" value={password} onIonChange={(e: any) => setPassword(e.target.value)}></IonInput>
                            </IonItem>
                            {
                                triggerError ? 
                                <IonItem>
                                    <IonText color="warning">
                                        <b>There has been an error signing you in: {error}</b>
                                    </IonText>
                                </IonItem>
                                : <div></div>
                            }
                            <IonButton expand="block" color="secondary" className="btn-login" onClick={handleLogin}>Login</IonButton>
                        </IonList>
                    </div>
                    <div>Don't have an account? <button id="sign-up" onClick={toggleForm as any} color="secondary">Sign Up</button></div>
                </div>
            </IonContent>
        </IonPage>
    )
}

export default Login