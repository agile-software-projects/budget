export default class IDB {
    constructor(dbName, dbVersion) {
        this.dbName = dbName;
        this.dbVersion = dbVersion;
    }

    createKeyStore(objectStores) {
        const self = window.self;
        if (self.indexedDB) {
            const request = self.indexedDB.open(this.dbName, this.dbVersion);
            request.onsuccess = (event) => {
                console.log("[on success create key stores", request.result);
            }
            request.onupgradeneeded = (event) => {
                const db = event.target.result;
                objectStores.forEach(objectStore => {
                    db.createObjectStore(objectStore, {
                        keyPath: "id",
                        autoIncrement: true
                    })
                });
            }
        }
    }

    dbInsert(dbTable, payload) {
        const self = window.self;
        if (self.indexedDB) {
            const request = self.indexedDB.open(this.dbName, this.dbVersion);
            request.onsuccess = (event) => {
                const db = event.target.result;
                const transaction = db.transaction(dbTable, "readwrite");
                transaction.onsuccess = (event) => {
                    console.log("[transaction inserts] all done");
                }

                const productStore = transaction.objectStore(dbTable);
                payload.forEach(product => {
                    productStore.add(product);
                });
            }
        }
    }

    loadFromDb(dbTable) {
        return new Promise((resolve, reject) => {
            if (window.indexedDB) {
                const request = window.indexedDB.open(this.dbName, this.dbVersion);
                request.onsuccess = event => {
                    const db = event.target.result;
                    const transaction = db.transaction(dbTable, "readwrite");
                    const productStore = transaction.objectStore(dbTable);
                    const results = productStore.getAll();
                    results.onsuccess = event => {
                        resolve(event.target.result);
                    }
                }
            } else {
                reject("indexedDB not available");
            }
        });
    }

    getRecordFromId(dbTable, id) {
        return new Promise((resolve, reject) => {
            if (window.indexedDB) {
                const request = window.indexedDB.open(this.dbName, this.dbVersion);
                request.onsuccess = event => {
                    const db = event.target.result;
                    const transaction = db.transaction(dbTable, "readwrite");
                    const productStore = transaction.objectStore(dbTable);
                    const results = productStore.get(id);
                    results.onsuccess = event => {
                        resolve(event.target.result);
                    }
                }
            } else {
                reject("indexedDB not available");
            }
        }); 
    }

    dbEdit(dbTable, payload, id) {
        if (window.indexedDB) {
            const request = window.indexedDB.open(this.dbName, this.dbVersion);
            request.onsuccess = event => {
                payload.id = Number(id);
                const db = event.target.result;
                const transaction = db.transaction(dbTable, "readwrite");
                const productStore = transaction.objectStore(dbTable);
                productStore.put(payload);
            }
        }
    }

    removeRecord(dbTable, id) {
        if (window.indexedDB) {
            const request = window.indexedDB.open(this.dbName, this.dbVersion);
            request.onsuccess = event => {
                const db = event.target.result;
                const transaction = db.transaction(dbTable, "readwrite");
                const productStore = transaction.objectStore(dbTable);
                productStore.delete(id);
            }
        }
    }
}