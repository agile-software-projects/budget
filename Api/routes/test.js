const express = require('express');
const router = express.Router();
const winston = require('../core/winston');
const env = require('../scripts/env');
var databaseFunctions = null 
const Account = require('../classes/Account');

if(env.devDatabase.enabled) {
    databaseFunctions = require('../core/databaseFunctions');
}

router.all('*', (req, res, next) => {
    if(env.devDatabase.enabled) {
        next();
    } else {
        res.status(500).json({error: "No database detected"});
    }
});


// get transactions for an account
router.get('/getTransactions', async (req, res) => {
    try{
        res.status(400);
        const accountId = req.sanitize(req.query.accountId);

        //get transactions for given user
        const transactions = await databaseFunctions.selectTable("transactions", {accountId: accountId});
        res.status(200);
        res.json({transactions});

    }catch(e){
        console.error(e);
        res.status(res.statusCode);
        res.json({error: e});
    }
})

router.get('/testRoute', async (req, res) => {
    try{
        res.status(400);
        res.json({message: "hello"});

    }catch(e){
        console.error(e);
        res.status(res.statusCode);
        res.json({error: e});
    }
})
