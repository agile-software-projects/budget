self.addEventListener('activate', (event) => {
    event.waitUntil((async () => {
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.filter(cache => !cache === cacheName)
                .map(cachesToDelete => {
                    return caches.delete(cachesToDelete);
                }
            ));
        });
        if ('navigationPreload' in self.registration) {
            await self.registration.navigationPreload.enable();
        }
    })());
    self.clients.claim();
    console.log("SW activated");
});