import React, {createContext, useReducer} from 'react';
import { actions, Store } from './store-types';

// interface UserData {
//     user: any
//     setLoggedIn(val: boolean): void
//     getAccounts(): Promise<any[]>
// }

// const userData: UserData = {
// 	user: null,
// 	setLoggedIn: () => {},
//     getAccounts: () => Promise.resolve([])
// }

// export const UserContext = React.createContext(userData);

// store.js

interface StoreAction {
    type: string
    payload: any
}

const storeData: any = {
    loadingApp: true,
    loggedIn: false,
    showNotification: {show: false, message: null, color: null},
    user: null,
    accounts: [],
    allTransactions: [],
    accountTransactions: [],
    selectedAccount: null,
    categories: [],
    goals: [],
    budgets: [],
    selectedTransaction: {},
    currency: "£",
    accountGoals: []
}

const StoreContext = createContext(storeData)
const { Provider } = StoreContext

const StoreProvider = ({children}: any) => {
    const [state, dispatch] = useReducer((state: Store, action: StoreAction) => {
        switch(action.type) {

            // app actions
            case actions.app.LOADING:
                return {...state, loadingApp: true} 

            case actions.app.LOADED:
                return {...state, loadingApp: false} 

            case actions.app.SHOW_NOTIFICATION:
                return {...state, showNotification: {show: true, message: action.payload.message, color: action.payload.color}} 

            case actions.app.HIDE_NOTIFICATION:
                return {...state, showNotification: {show: false, message: action.payload.message, color: action.payload.color}} 

            // auth actions
            case actions.auth.LOGIN:
                return {...state, loggedIn: true} 

            case actions.auth.LOGOUT:
                return {...state, loggedIn: false} 

            case actions.user.SET_USER:
                return {...state, user: action.payload} 

            // user accounts actions
            case actions.accounts.SET_ACCOUNTS:
                return {...state, accounts: action.payload}

            case actions.accounts.SELECT_ACCOUNT:
                return {...state, selectedAccount: action.payload}

            case actions.accounts.ADD_ACCOUNT:
                return {...state, accounts: [action.payload, ...state.accounts]} 

            case actions.accounts.DELETE_ACCOUNT:
                const accounts = state.accounts.filter(a => a.accountId !== action.payload)
                return {...state, accounts}

            case actions.accounts.SAVE_ACCOUNT:
                const updatedAccounts = state.accounts.map(a => {
                    if(a.accountId === action.payload){
                        return action.payload
                    }
                    return a
                })
                return {...state, accounts: updatedAccounts} 

            // user transactions actions
            case actions.transactions.SET_ALL_TRANSACTIONS:
                return {...state, allTransactions: action.payload} 

            case actions.transactions.SET_ACCOUNT_TRANSACTIONS:
                return {...state, accountTransactions: action.payload}

            case actions.transactions.ADD_TRANSACTION:
                return {...state, allTransactions: [action.payload, ...state.allTransactions]} 
            
            case actions.transactions.SAVE_TRANSACTION:
                const updatedTransactions = state.allTransactions.map(t => {
                    if(t.transactionId === action.payload.transactionId){
                        return action.payload
                    }
                    return t
                })
                return {...state, allTransactions: updatedTransactions} 

            case actions.transactions.DELETE_TRANSACTION:
                const allTransactions = state.allTransactions.filter(t => t.transactionId !== action.payload)
                return {...state, allTransactions} 

            case actions.transactions.SET_SINGLE_TRANSACTION:
                return {...state, selectedTransaction: action.payload}
            
            // categories actions
            case actions.categories.SET_CATEGORIES:
                return {...state, categories: action.payload} 
            
            // goals actions
            case actions.goals.SET_GOALS:
                return {...state, goals: action.payload}

            case actions.goals.SELECT_GOAL:
                return {...state, selectedGoal: action.payload}

            case actions.goals.ADD_GOAL:
                return {...state, goals: [action.payload, ...state.goals]} 

            case actions.goals.DELETE_GOAL:
                const goals = state.goals.filter(g => g.goalId !== action.payload)
                return {...state, goals}

            case actions.goals.SAVE_GOAL:
                const updatedGoals = state.goals.map(g => {
                    if(g.goalId === action.payload.goalId){
                        return action.payload
                    }
                    return g
                })
                return {...state, goals: updatedGoals} 
            
            case actions.goals.SET_ACCOUNT_GOALS:
                return {...state, accountGoals: action.payload}
            
            case actions.goals.ADD_SAVINGS: 
                return {...state}
            // budgets actions
            case actions.budget.SET_BUDGETS:
                return {...state, budgets: action.payload}

            case actions.budget.SELECT_BUDGET:
                return {...state, selectedBudget: action.payload}

            case actions.budget.ADD_BUDGET:
                return {...state, budgets: [action.payload, ...state.budgets]} 

            case actions.budget.DELETE_BUDGET:
                const budgets = state.budgets.filter(b => b.budgetId !== action.payload)
                return {...state, budgets} 

            case actions.budget.SAVE_BUDGET:
                const updatedBudgets = state.budgets.map(b => {
                    if(b.budgetId === action.payload.budgetId){
                        return action.payload
                    }
                    return b
                })
                return {...state, budgets: updatedBudgets} 
                
            case actions.user.SET_CURRENCY:
                return {...state, currency: action.payload}
                
            default:
                throw new Error()
        }
    }, storeData)

    return <Provider value={{ state, dispatch }}>{children}</Provider>
}

export { StoreContext, StoreProvider }