import React, {useState, useEffect} from 'react';
import { IonModal, IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonGrid, IonItem, IonLabel, IonInput, IonProgressBar, IonSelect, IonSelectOption, IonCol, IonToggle } from '@ionic/react';
import { closeOutline } from 'ionicons/icons';
import './AddBudget.css';
import { useStore } from '../../store/store-actions';
import validate from './validateBudgetForm';
import {convertPriceFormat} from "../../helpers/price";
const AddBudget = (props: any) => {

	interface IErrors {
        budgetName?: string,
        totalLimit?: string,
		currentAmount?: string,
		categoryName?: string,
		accountId?: string
    }

    const [values, setValues] = useState({
        budgetName: "",
        totalLimit: "",
        active: 1,
        currentAmount: "",
		accountId: "",
		categoryName: ""
    })

	const { isOpen, hide } = props

	const [recurring, setRecurring] = useState<boolean>(false)
	
	const [errors, setErrors] = useState<IErrors>({});

    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success
	const {accounts, addBudget} = useStore();
	
	
	const handleChange = (e: any) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
	}
	
	// hide component need to reset the state
	const hidePage = () => {
		setValues({ 
			budgetName: "",
			totalLimit: "",
			active: 1,
			currentAmount: "",
			accountId: "",
			categoryName: ""
		});
		hide();
	}

    const handleSubmit = (e: any) => {
        e.preventDefault();
        setErrors(validate(values));
        setIsSubmitted(true);
	}
	
	useEffect(() => {
        if (isSubmitted && Object.keys(errors).length === 0) {

			setSubmission(1);

			const data: any = {
				budgetName: values.budgetName,
                totalLimit: convertPriceFormat(values.totalLimit),
                active: values.active,
				categoryName: values.categoryName,
                currentAmount: convertPriceFormat(values.currentAmount),
                accountId: values.accountId
            }

            try{
				setSubmission(2);
				addBudget(data);
                hide();
            }catch(e){
                console.error(e);
                setSubmission(-1);
            }
            
        } else if (Object.keys(errors).length !== 0) {
            setSubmission(-1);
        }
    },
        [isSubmitted, errors]
    );

    const messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error</IonLabel>;
        } else if(submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Saved</IonLabel>;
        } else {
            return null;
        }
    }

	return (
		<IonModal isOpen={isOpen} cssClass='add-goal'>
            <IonContent fullscreen>
				<IonHeader className="modal-header">
					<IonToolbar className="top-bar">
						<IonTitle className="add-goal-title">New Budget</IonTitle>
						<IonButtons slot="end">
							<IonButton onClick={hidePage} className="btn-menu">
								<IonIcon slot="icon-only" icon={closeOutline} className="menu-icon"/>
							</IonButton>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
				<div className="modal-content">
					<form className="goal-budget-form" onSubmit={handleSubmit}>
						<IonGrid>
							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="text" 
									name="budgetName"
									className="form-input"
									placeholder="Description"
									value={values.budgetName}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.budgetName}</IonLabel>
							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="number"
									step=".01"
									name="totalLimit"
									className="form-input"
									placeholder="Target Amount"
									value={values.totalLimit}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.totalLimit}</IonLabel>

							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="number"
									step=".01"
									name="currentAmount"
									className="form-input"
									placeholder="Current Amount"
									value={values.currentAmount}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.currentAmount}</IonLabel>

                            <IonItem className="ion-no-padding ion-margin" lines="none">
                                <IonCol id="recurring" size="11">
                                    <IonLabel>Recurring</IonLabel>
                                </IonCol>
                                <IonCol size="1">
                                    <IonToggle checked={recurring} onIonChange={e => setRecurring(e.detail.checked)} />
                                </IonCol>
                            </IonItem>

							<IonItem className="ion-no-padding ion-margin">
								<IonSelect onIonChange={handleChange} name="accountId" placeholder="Select an account" value={values.accountId}>
									{accounts.map((acc:any, i) => (
										<IonSelectOption value={acc.accountId} key={i}>
											{acc.accountName}
										</IonSelectOption>
									))}
								</IonSelect>
							</IonItem>

							<IonButton expand="block" type="submit" color="primary" id="btn-save" className="ion-margin">Save</IonButton>
							{messages()}
						</IonGrid>
					</form>
				</div>
            </IonContent>
        </IonModal>
	);
};

export default AddBudget;
