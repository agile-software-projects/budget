import Auth from '../helpers/Auth';
import { apiUrl } from '../helpers/env';
import IDB from '../helpers/IDB';
import { Account, Budget, Category, Goal, Transaction, User } from './store-types';

const idb = new IDB("budget", 1)

export const getUser = () => new Promise<User>(async (resolve, reject) => {
    try{
        const auth = new Auth()
        const user: User = await auth.checkLoginState()
        if(!user){
            throw 'No authenticated'
        }
        const userRecord = {...user, id: 1}
        idb.dbInsert("clientUser", [userRecord])
        resolve(user)
    }catch(e){
        reject(e)
    }
})

export const getAccounts = () => new Promise<Account[]>(async (resolve, reject) => {
    const response = await fetch(`${apiUrl}/account/getAccounts`, {
        method: "get",
        credentials: "include"
    })

    const accounts = await response.json()
    const filterEnabledAccounts = accounts.filter((acc:any) => acc.accountEnabled === 1)
    const accountArr = filterEnabledAccounts.map((account: any) => account)
    idb.dbInsert("accounts", accountArr)
    resolve(accountArr)
})

export const getAllTransactions = () => new Promise<Transaction[]>(async (resolve, reject) => {
    try{
        const response = await fetch(`${apiUrl}/transaction/userTransaction/getAll`, {
            method: "get",
            credentials: "include"
        })

        if(!response.ok){
            throw Error()
        }
        const allTransactions = await response.json()
        resolve(allTransactions)
    }catch(e){
        reject()
    }
})

export const getCategories = () => new Promise<Category[]>(async (resolve, reject) => {
    try{
        const response = await fetch(`${apiUrl}/category/getAllDefault`, {
            method: "get",
            credentials: "include"
        })

        const cats = await response.json()
        idb.dbInsert("categories", cats)
        resolve(cats)
        
    }catch(e){
        reject()
    }
})

export const getGoals = () => new Promise<Goal[]>(async (resolve, reject) => {
    try{
        const response = await fetch(`${apiUrl}/goal/user/getAll`, {
            method: "get",
            credentials: "include"
        })

        const goals = await response.json()
        const filterActiveGoals = goals.filter((goal:any) => goal.active === 1);

        resolve(filterActiveGoals)
    }catch(e){
        reject()
    }
})

export const getBudgets = () => new Promise<Budget[]>(async (resolve, reject) => {
    try{
        const response = await fetch(`${apiUrl}/budget/user/getAll`, {
            method: "get",
            credentials: "include"
        })

        const budgets = await response.json()

        const filterActiveBudget = budgets.filter((budget:any) => budget.active === 1);

        resolve(filterActiveBudget)
    }catch(e){
        reject()
    }
})