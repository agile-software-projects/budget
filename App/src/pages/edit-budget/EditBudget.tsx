import React, {useState, useEffect} from 'react';
import { IonContent, IonHeader, IonTitle, IonToolbar, IonButtons, IonButton, IonIcon, IonGrid, IonItem, IonLabel, IonInput, IonDatetime, IonProgressBar, IonSelect, IonSelectOption, IonPage } from '@ionic/react';
import { closeOutline } from 'ionicons/icons';
import './EditBudget.css';
import { useStore } from '../../store/store-actions';
import validate from './validateBudgetForm';
import { convertPriceFormat, priceFormat } from '../../helpers/price';

const EditBudget = (props: any) => {


	interface IErrors {
        budgetName?: string,
        totalLimit?: string,
        endDate?: string,
        currentAmount?: string
    }

	const { hide } = props
	
	const [errors, setErrors] = useState<IErrors>({});

    const [isSubmitted, setIsSubmitted] = useState(false)
    const [submission, setSubmission] = useState(0) // -1 => error, 0 => not submitted, 1 submitted but not returned value, 2 returned value with success
	const {selectedBudget, saveBudget, accounts} = useStore();

	const [values, setValues] = useState({
		budgetId: selectedBudget.budgetId,
        budgetName: selectedBudget.budgetName,
        totalLimit: priceFormat(selectedBudget.totalLimit),
        endDate: selectedBudget.endDate,
        active: 1,
        currentAmount: priceFormat(selectedBudget.currentAmount),
		accountId: selectedBudget.accountId,
		categoryName: selectedBudget.categoryName,
		resetPeriod: selectedBudget.resetPeriod
    })
	
	const handleChange = (e: any) => {

        setValues({
            ...values,
            [e.target.name]: e.target.value
        })
	}

    const handleSubmit = (e: any) => {
        e.preventDefault();
        setErrors(validate(values));
        setIsSubmitted(true);
	}
	
	useEffect(() => {
        if (isSubmitted && Object.keys(errors).length === 0) {

			setSubmission(1);

            const data: any = {

				budgetId: values.budgetId,
				budgetName: values.budgetName,
				totalLimit: convertPriceFormat(values.totalLimit),
				endDate: values.endDate,
				active: 1,
				currentAmount: convertPriceFormat(values.currentAmount),
				accountId: values.accountId,
				categoryName: values.categoryName,
				resetPeriod: values.resetPeriod
            }

            try{
				setSubmission(2);
				saveBudget(data);
                hide(false);
            }catch(e){
                console.error(e)
                setSubmission(-1);
            }
            
        } else if (Object.keys(errors).length !== 0) {
            setSubmission(-1);
        }
    },
        [isSubmitted, errors, values.budgetId, values.budgetName, values.totalLimit, values.endDate, values.currentAmount, values.accountId, values.categoryName, values.resetPeriod, saveBudget, hide]
    );

    const messages = () => {
        if(submission === -1) {
            return <IonLabel color="danger" className="submission-msg">Error</IonLabel>;
        } else if(submission === 1) {
            return <IonProgressBar type="indeterminate"></IonProgressBar>;
        } else if(submission === 2) {
            return <IonLabel color="success" className="submission-msg">Saved</IonLabel>;
        } else {
            return null;
        }
    }

	return (
		<IonPage className='edit-budget'>
            <IonContent fullscreen>
				<IonHeader className="modal-header">
					<IonToolbar className="top-bar">
						<IonTitle className="edit-budget-title">Edit Budget</IonTitle>
						<IonButtons slot="end">
							<IonButton onClick={() => {hide(false)}} className="btn-menu">
								<IonIcon slot="icon-only" icon={closeOutline} className="menu-icon"/>
							</IonButton>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
				<div className="modal-content">
					<form className="budget-budget-form" onSubmit={handleSubmit}>
						<IonGrid>
							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="text" 
									name="budgetName"
									className="form-input"
									placeholder="Description"
									value={values.budgetName}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.budgetName}</IonLabel>
							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="number"
									step=".01"
									name="totalLimit"
									className="form-input"
									placeholder="Target Amount"
									value={values.totalLimit}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.totalLimit}</IonLabel>
							<IonItem className="ion-no-padding ion-margin">
								<IonDatetime 
									displayFormat="DD-MMM-YYYY" 
									placeholder="Target Date" 
									className="form-input"
									name="endDate"
									value={values.endDate.toString()}
									max="2099-12-31"
									onIonChange={handleChange}
									>
								</IonDatetime> 
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.endDate}</IonLabel>

							<IonItem className="ion-no-padding ion-margin">
								<IonInput 
									type="number"
									step=".01"
									name="currentAmount"
									className="form-input"
									placeholder="Current Amount"
									value={values.currentAmount}
									onIonChange={handleChange}
									/>
							</IonItem>
							<IonLabel color="danger" className="ion-padding-start">{errors.currentAmount}</IonLabel>

							<IonItem className="ion-no-padding ion-margin">
								<IonSelect onIonChange={handleChange} name="accountId" placeholder="Select an account" value={values.accountId}>
									{accounts.map((acc:any, i) => (
										<IonSelectOption value={acc.accountId} key={i}>
											{acc.accountName}
										</IonSelectOption>
									))}
								</IonSelect>
							</IonItem>

							<IonButton expand="block" type="submit" color="primary" id="btn-save" className="ion-margin">Save</IonButton>
							{messages()}
						</IonGrid>
					</form>
				</div>
            </IonContent>
        </IonPage>
	);
};

export default EditBudget;
