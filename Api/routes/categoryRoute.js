const express = require('express');
const router = express.Router();
const winston = require('../core/winston');
const env = require('../scripts/env');
const {v4:uuidv4} = require('uuid');
const Category = require('../classes/Category');


router.post('/new', (req, res) => {

    const category = new Category(req.body);
    category.categoryName = req.sanitize(category.categoryName);
    category.parentCategoryId = req.sanitize(category.parentCategoryId);

    if (category.assignCategoryId().validate()) {
        category.insertCategory().then((results) => {
            if (results.success === true) {
                res.status(200);
                return res.json({success: true});
            } else {
                res.status(400);
                return res.json({error: "New category could not be inserted"});
            }
        }).catch((err) => {
            res.status(400);
            winston.error(JSON.stringify(err));
            return res.json(err);    
        });
    } else {
        res.status(400);
        return res.json({error: "New category could not be validated"});
    }
});

router.get("/:field/:code/getCategory", (req, res) => {

    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);
    const categoryData = {};

    if (field !== "categoryId") {
        res.status(400);
        return res.json({error: "unknown field identifier"});
    }

    categoryData.categoryId = code;
    const category = new Category(categoryData);

    category.populate().then(() => {
        res.status(200);
        return res.json({
            categoryId: category.categoryId,
            categoryName: category.categoryName,
            parentCategoryId: category.parentCategoryId
        });
    }).catch((err) => {
        winston.error(err);
        res.status(500);
        return res.json(err);
    });
});

router.post("/:field/:code/updateCategory", (req, res) => {

    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);
    
    const categoryData = {};
    categoryData.categoryId = code;
    const category = new Category(categoryData);

    category.populate().then(() => {

        //throw with no category name or parent id to update
        if ((Object.keys(req.body).length === 0) || !req.body.categoryName) {
            res.status(400);
            return res.json({error: "no category name detected in body"});
        }

        if (req.body.categoryName) {
            category.categoryName = req.body.categoryName;
        }

        if (req.body.parentCategoryId) {

            category.parentCategoryId = req.body.parentCategoryId;

            category.verifyParentID().then((parent) => {

            }).catch((err) => {
                res.status(400);
                return res.json({error: "parent id does not exist"});
            });
        }

        category.validate().update().then(() => {
            res.status(200);
            return res.json({success: true});
        }).catch((err) => {
            winston.error(err);
            res.status(500);
            return res.json(err);
        })

    }).catch((err) => {
        winston.error(err);
        res.status(500);
        return res.json(err);
    });
});

router.post("/:field/:code/deleteCategory", (req, res) => {

    const field = req.sanitize(req.params.field);
    const code = req.sanitize(req.params.code);

    const categoryData = {};
    categoryData.categoryId = code;
    const category = new Category(categoryData);

    if (!category.isParentCategory()) {
        category.deleteCategory().then(() => {
            res.status(200);
            return res.json({success: true});
        }).catch((err) => {
            winston.error(err)
            res.status(500)
            return res.json(err)
        })
    } else {
        res.status(400);
        return res.json({error: "cannot delete parent category"});
    }

});

router.get("/getAllDefault", (req, res) => {
    Category.getAll().then((categories) => {
        res.status(200)
        return res.json(categories)
    }).catch((err) => {
        res.status(500)
        return res.json(err)
    })
})

router.get("/:categoryId/getCategoryTransactions", (req, res) => {
    const categoryId = req.sanitize(req.params.categoryId)
    const from = req.sanitize(req.query.from)
    const to = req.sanitize(req.query.to)
    const userId = req.session.user.userId
    Category.getCategoryTransactions({categoryId, from, to, userId}).then((transactions) => {
        res.status(200)
        return res.json(transactions)
    }).catch((err) => {
        console.error(err)
        res.status(500)
        return res.json(err)
    })
})

module.exports = router;