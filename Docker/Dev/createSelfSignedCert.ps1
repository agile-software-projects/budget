﻿$date_now = Get-Date
$extended_date = $date_now.AddYears(3)

$cert = New-SelfSignedCertificate -certstorelocation cert:\localmachine\My -dnsname webprojects-dev.co.uk, webprojects-dev.co.uk, webprojects-dev.co.uk -notafter $extended_date -KeyLength 4096

$pwd = ConvertTo-SecureString -String 'certificate1234' -Force -AsPlainText

$path = 'Cert:\LocalMachine\My\' + $cert.Thumbprint

Export-PfxCertificate -cert $path -FilePath c:\ssl\certificate.pfx -Password $pwd