import React from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonButtons, IonIcon, IonCard, IonCardHeader, IonCardSubtitle, IonCardTitle } from '@ionic/react';
import { trashOutline } from 'ionicons/icons';
import './RecurringExpenses.css';
import Menu from '../menu/Menu';
import { useStore } from '../../store/store-actions';
import { priceFormat } from '../../helpers/price';
import moment from 'moment';
import { Account, Transaction } from '../../store/store-types';

const RecurringExpenses: React.FC = () => {

	const {allTransactions, deleteTransaction, accounts} = useStore()

	const getRecurringTransactions = () => allTransactions.filter(t => t.recurring)

	const directionIndicator = (direction: string, amount: number) => {
        if(direction === 'income'){
            return `+ ${priceFormat(amount)}`;
        }
        return `- ${priceFormat(amount)}`;
    }

	const getNextDate = (recurrence: string, date: string) => {
		const d = moment(date)
		const r1 = recurrence[0]
		const r2 = recurrence[1] === 'm' ? 'M' : recurrence[1]
		d.add(r1, r2 as any)

		return `${d.format('MMM DD')} - every ${r1} ${r2}`
	}

	const getAccount = (transaction: Transaction) => {
        const find = accounts.filter(a => a.accountId === transaction.accountId)
        return (find.length ? find[0] : null) as Account
    }

	return (
		<IonPage className="recurring-expenses">
			<IonContent fullscreen color="light">
				<IonHeader className="header">
					<IonToolbar color="light" className="top-bar">
						<IonTitle size="large" className="title">Recurring</IonTitle>
						<IonButtons slot="end">
							<Menu/>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
				<div className="recurring-list">
					{getRecurringTransactions().map((trans, i) => 
					<IonCard key={i}>
						<IonCardHeader>
							<div className="card-top">
								<div className="recurring-item-header">
									<div className="header-left">
										<div>{trans.transactionDescription}</div>
										{/* <div className="recurring-cat">Meals</div> */}
									</div>
									{/* <div className="header-right">
										<div className="item-actions">
											<IonToggle color="secondary" checked={true}/>
										</div>
									</div> */}
								</div>
							</div>
							<div className="card-bottom">
								<IonCardTitle>{directionIndicator(trans.transactionDirection, trans.amount)}</IonCardTitle>
								<div className="recurring-bottom">
									<IonCardSubtitle className="item-overview">Next date: {getNextDate(trans.recurrencePeriod, trans.transactionDate as any)}</IonCardSubtitle>
									<IonIcon slot="icon-only" icon={trashOutline} onClick={() => deleteTransaction(trans, getAccount(trans))} className="delete-icon" />
								</div>
							</div>
						</IonCardHeader>
					</IonCard>)}
				</div>
			</IonContent>
		</IonPage>
	);
};

export default RecurringExpenses;
