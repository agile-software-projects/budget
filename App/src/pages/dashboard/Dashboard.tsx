import React, {useEffect, useState} from 'react';
import { IonModal, IonContent, IonHeader, IonChip, IonTitle, IonToolbar, IonButtons, IonIcon, IonLabel, IonItem, IonButton, IonLoading } from '@ionic/react';
import { closeOutline } from 'ionicons/icons';
import './Dashboard.css';
import {getLastMonthsRange, getMonthRange} from '../../helpers/orderMonths'
import moment from 'moment'
import { useStore } from '../../store/store-actions';
import { priceFormat } from '../../helpers/price';
import { Transaction } from '../../store/store-types';
import { apiUrl } from "../../helpers/env";

const Dashboard = (props: any) => {
    const { isOpen, hide } = props
    const { categories, allTransactions, accounts} = useStore()
    const [categoriesTransactions, setCategoriesTransactions] = useState([])
    const [loading, setLoading] = useState(true)

    // set state - selected month to current month
    const [selectedMonth, setSelectedMonth] = useState(0)

    // create months modal from months array
    const months = getLastMonthsRange()
    const monthsModal = months.map((month, index) => {
        return (
            <IonChip className={index === selectedMonth ? 'active-chip' : ''} color={index === selectedMonth ? '' : 'light'} key={month} onClick={() => {
                    setSelectedMonth(index); 
                    setTimeout(() => getTotal(index), 100) 
                }}>
                <IonLabel>{moment(month).format('MMMM')}</IonLabel>
            </IonChip>
        )
    })

    const [total, setTotal] = useState(0)

    //get all categories
	const getTotal = async (m: number) => {
        setLoading(true)
        try{
            const {from, to} = getMonthRange(months[m])

            let grandTotal = 0
            const cats = await Promise.all(categories.map((category: any) => {
                let catTotal = 0
                // get category transactions
                return new Promise((resolve, reject) => {
                    fetch(`${apiUrl}/category/${category.categoryId}/getCategoryTransactions?from=${from}&to=${to}`, {
                        method: "get",
                        credentials: "include"
                    }).then((response) => {
                        return response.json()
                    }).then((transactions) => {
                        transactions.map((t: Transaction) => {
                            if(t.transactionDirection === 'expense'){
                                catTotal -= t.amount
                            }else{
                                catTotal += t.amount
                            }
                        })
                        grandTotal += catTotal
                        resolve({...category, total: catTotal, transactions})
                    }).catch((err) => {
                        console.error(err)
                    })
                })
            })) as any
            setTotal(grandTotal)
            setCategoriesTransactions(cats)
            setLoading(false)
        }catch(e){
            setLoading(false)
        }
    } 

	useEffect(() => {
        setTimeout(() => {
            getTotal(selectedMonth)
        }, 5000)
	}, [categories, allTransactions, accounts])

	return (
		<IonModal isOpen={isOpen} cssClass='dashboard'>
            <IonContent fullscreen color="primary">
				<IonHeader className="modal-header">
					<IonToolbar color="primary" className="top-bar">
						<IonTitle size="large" className="title">Dashboard</IonTitle>
						<IonButtons slot="end">
							<IonButton onClick={hide} className="btn-menu btn-menu-contrast">
								<IonIcon slot="icon-only" icon={closeOutline} className="menu-icon"/>
							</IonButton>
						</IonButtons>
					</IonToolbar>
				</IonHeader>
                <div className="modal-content">
                    <div className="months-filter">
                        {monthsModal}
                    </div>
                    <div className="dashboard-value">{priceFormat(total)}</div>
                    <div className="dashboard-list">
                        {categoriesTransactions.map((category: any, i) => 
                            <IonItem color="primary" className="dashboard-list-item" key={i}>
                                <span slot="start" className="cat-circle"></span>
                                <IonLabel slot="start">{category.categoryName}</IonLabel>
                                <IonLabel slot="end" className="cat-amount">{priceFormat(category.total)}</IonLabel>
                            </IonItem>
                        )}
                    </div>
                    {/* <IonButton expand="block" color="light" className="download-report-button">Download Report</IonButton> */}
                </div>
            </IonContent>
            <IonLoading cssClass='loading' isOpen={loading} message={'Loading...'} />
        </IonModal>
	);
};

export default Dashboard;
