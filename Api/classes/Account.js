const { v4: uuidv4 } = require('uuid');
const databaseFunctions = require('../core/databaseFunctions');
const Budget = require('./Budget');
const Category = require("./Category");
const winston = require("../core/winston");

class Account {
    constructor(account) {
        this.accountId = account.accountId;
        this.userId = account.userId;
        this.accountType = account.accountType;
        this.accountName = account.accountName;
        this.creationDate = account.creationDate;
        this.accountEnabled = account.accountEnabled;
        this.currentBalance = account.currentBalance;
        this.lastUpdated = account.lastUpdated;
    }

    wholeAccount() {
        return {
            accountId: this.accountId,
            userId: this.userId,
            accountType: this.accountType,
            accountName: this.accountName,
            creationDate: this.creationDate,
            accountEnabled: this.accountEnabled,
            currentBalance: this.currentBalance,
            lastUpdated: this.lastUpdated
        }
    }

    assignAccountId() {
        this.accountId = uuidv4();
        return this;
    }

    validate() {
        if(this.accountId.length != 36) { throw(new Error("accountId is invalid")); }
        if(this.userId.length != 36) { throw(new Error("userId is invalid")); }

        const stringRe = /^(?=[a-zA-Z0-9._\ \-]{1,20}$)/;
        if (!(stringRe.test(this.accountType))) { throw(new Error("account type is invalid")); }
        if (!(stringRe.test(this.accountName))) { throw(new Error("accountName is invalid")); }
        
        const isDate = (date) => {
            return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
        }
        if (!(isDate(this.creationDate))) { return(new Error("creationDate is invalid")); }
        if (!(isDate(this.lastUpdated))) { return(new Error("lastUpdated is invalid")); }

        if (!(typeof this.accountEnabled === "boolean" || this.accountEnabled === 1 || this.accountEnabled === 0)) { return(new Error("accountEnabled is invalid")); }
        
        if (isNaN(this.currentBalance)) { return(new Error("currentBalance is invalid")); }

        return this;
    }

    verifyUser() {
        return new Promise((resolve, reject) => {
            if (this.validate()) {
                databaseFunctions.selectTable("users", {userId: this.userId}).then((users) => {
                    if(users.length === 1) {
                        resolve(users[0]);
                    } else {
                        reject({error: "user not verified"});
                    }
                }).catch((err) => { 
                    reject({error: err});
                })
            } else {
                reject({error: "user not verified"});
            }
        });
    }

    insertAccount() {
        //check it doesn't already exist and insert if it doesn't
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("account", {accountId: this.accountId}).then((accounts) => {
                // if (accounts.length === 1) { reject({error: "an account with this id already exists"}) }
                this.creationDate = new Date();
                this.lastUpdated = new Date();
                databaseFunctions.insertTableRow("account", this.wholeAccount()).then((rowsAffected) => {
                    if (rowsAffected[0] === 1) {
                        //set defaults
                        const endDate = new Date();
                        endDate.setDate(endDate.getDate() + 30);                    
                        //budget needs default cats
                        databaseFunctions.selectTable("category", {isDefault: true}).then((categories) => {
                            const defaultLinks = [];
                            categories.forEach((cat) => {
                                const category = new Category(cat);

                                const budget = new Budget({
                                    budgetName: "default",
                                    totalLimit: 99900,
                                    accountId: this.accountId,
                                    active: true,
                                    resetPeriod: "30d",
                                    currentPeriod: uuidv4(),
                                    startDate: new Date(),
                                    endDate: endDate
                                });    
                                
                                budget.assignBudgetId().validate().insertBudget().then(() => {
                                    defaultLinks.push(category.linkToBudget(budget.budgetId));
                                }).catch((err) => {
                                    reject(err);
                                });
                            });
                            Promise.all(defaultLinks).then(() => {
                                resolve({success: true});
                            }).catch((err) => {
                                reject(err);
                            });
                        }).catch((err) => {
                            reject(err);
                        });
                    } else {
                        reject({error: "account not inserted"});
                    }
                }).catch((err) => {
                    reject({error: err});
                });
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    populate() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("account", {accountId: this.accountId}).then((accounts) => {
                if (accounts.length === 1) {
                    const account = accounts[0];
                    this.userId = account.userId;
                    this.accountType = account.accountType;
                    this.accountName = account.accountName;
                    this.creationDate = account.creationDate;
                    this.accountEnabled = account.accountEnabled;
                    this.currentBalance = account.currentBalance;
                    this.lastUpdated = account.lastUpdated;
                    resolve();
                } else {
                    reject({error: "account doesn't exist"});
                }
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    update() {
        //check account exists and update if it does
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("account", {accountId: this.accountId}).then((accounts) => {
                if (accounts.length === 1) {
                    this.lastUpdated = new Date();
                    databaseFunctions.updateTableRow("account", {accountId: this.accountId}, this.wholeAccount()).then((rowsAffected) => { 
                        if (rowsAffected === 1) { 
                            resolve({success: true});
                        } else {    
                            reject({error: "account not updated"});
                        }
                    }).catch((err) => {     
                        reject({error: err});
                    });
                } 
            }).catch((err) => {
                reject({error: err});
            });
        });
    }

    getAllAccountTransactions() {
        return new Promise((resolve, reject) => {
            databaseFunctions.selectTable("transactions", {accountId: this.accountId}).then((transactions) => {
                if (transactions.length > 0) {
                    resolve(transactions)
                } else {
                    resolve([])
                }
            }).catch((err) => {
                reject(err);
            });
        });
    }

    deleteAccount() {
        //delete the transaction
        return new Promise((resolve, reject) => {
            databaseFunctions.deleteTableRows("account", {accountId: this.accountId}).then((rowsAffected) => {
                if (rowsAffected.rowsAffected[0] === 1) {
                    resolve({success: true});
                } else {
                    reject({error: "could not be deleted"});
                }
            }).catch((err) => {
                console.log(err);
                reject({error: err});
            });
        });
    }
}

module.exports = Account;